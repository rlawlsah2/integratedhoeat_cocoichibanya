package com.cdef.orderappformanagermobilecocoichibanya.View.CustomView;

/**
 * Created by kimjinmo on 2018. 6. 8..
 */


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fxn769.TextGetListner;

import java.util.ArrayList;

public class CustomNumPad extends FrameLayout implements View.OnClickListener {
    private Context context;
    private ArrayList<TextView> num = new ArrayList<>();
    private ArrayList<ImageView> line = new ArrayList<>();
    private String digits = "";
    private int TextLengthLimit = 6;
    private float TextSize = 12;
    private int TextColor = Color.BLACK;
    private int BackgroundResource = com.fxn769.R.drawable.numpad_background;
    private int ImageResource = com.fxn769.R.drawable.ic_backspace;
    private boolean GridVisible = true;
    private int GridBackgroundColor = Color.GRAY;
    private int GridThickness = 3;
    private String FontFaceString = "";
    private Typeface typeface;
    private ImageView delete;
    private FrameLayout delete_layout;
    private TextGetListner textGetListner;

    public void setDigits(String input)
    {
        this.digits = input;
    }

    public CustomNumPad(@NonNull Context context) {
        super(context);
        initialise(context, null);
    }

    public CustomNumPad(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialise(context, attrs);
    }

    public CustomNumPad(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialise(context, attrs);
    }

    public CustomNumPad setOnTextChangeListner(TextGetListner textGetListner) {
        this.textGetListner = textGetListner;
        setup();
        return this;
    }

    public CustomNumPad setBackgroundRes(int BackgroundResource) {
        this.BackgroundResource = BackgroundResource;
        setup();
        return this;
    }

    public CustomNumPad setImageRes(int ImageResource) {
        this.ImageResource = ImageResource;
        setup();
        return this;
    }

    public CustomNumPad setFontFace(String FontFaceString) {
        this.FontFaceString = FontFaceString;
        typeface = Typeface.createFromAsset(context.getAssets(), this.FontFaceString);
        setup();
        return this;
    }

    private void initialise(Context context, AttributeSet attrs) {
        this.context = context;
        TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, com.fxn769.R.styleable.Numpad, 0, 0);
        digits = attributes.getString(com.fxn769.R.styleable.Numpad_numpad_digits);
        TextLengthLimit = attributes.getInt(com.fxn769.R.styleable.Numpad_numpad_text_limit, 5);
        TextSize = attributes.getDimension(com.fxn769.R.styleable.Numpad_numpad_text_size, 12.0f);
        TextColor = attributes.getColor(com.fxn769.R.styleable.Numpad_numpad_text_color, Color.BLACK);
        BackgroundResource = attributes.getResourceId(com.fxn769.R.styleable.Numpad_numpad_background_resource, com.fxn769.R.drawable.numpad_background);
        ImageResource = attributes.getResourceId(com.fxn769.R.styleable.Numpad_numpad_image_resource, com.fxn769.R.drawable.ic_backspace);
        GridVisible = attributes.getBoolean(com.fxn769.R.styleable.Numpad_numpad_grid_visible, false);
        GridBackgroundColor = attributes.getColor(com.fxn769.R.styleable.Numpad_numpad_grid_background_color, Color.GRAY);
        GridThickness = (int) attributes.getDimension(com.fxn769.R.styleable.Numpad_numpad_grid_line_thickness, 3);
        FontFaceString = attributes.getString(com.fxn769.R.styleable.Numpad_numpad_fontpath);
        if (digits == null) {
            digits = "";
        }

        final View v = LayoutInflater.from(context).inflate(com.fxn769.R.layout.numlock_view, this, false);
        num.add((TextView) v.findViewById(com.fxn769.R.id.one));
        num.add((TextView) v.findViewById(com.fxn769.R.id.two));
        num.add((TextView) v.findViewById(com.fxn769.R.id.three));
        num.add((TextView) v.findViewById(com.fxn769.R.id.four));
        num.add((TextView) v.findViewById(com.fxn769.R.id.five));
        num.add((TextView) v.findViewById(com.fxn769.R.id.six));
        num.add((TextView) v.findViewById(com.fxn769.R.id.seven));
        num.add((TextView) v.findViewById(com.fxn769.R.id.eight));
        num.add((TextView) v.findViewById(com.fxn769.R.id.nine));
        num.add((TextView) v.findViewById(com.fxn769.R.id.zero));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line1));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line2));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line3));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line4));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line5));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line6));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line7));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line8));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line9));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line10));
        line.add((ImageView) v.findViewById(com.fxn769.R.id.line11));
        delete = v.findViewById(com.fxn769.R.id.delete);
        delete_layout = v.findViewById(com.fxn769.R.id.delete_layout);
        if (FontFaceString == null) {
            typeface = Typeface.DEFAULT;
        } else {
            typeface = Typeface.createFromAsset(context.getAssets(), FontFaceString);
        }
        setup();
        addView(v);
    }

    private void setup() {

        for (TextView textView : num) {
            textView.setOnClickListener(this);
            textView.setTextSize(TextSize);
            textView.setTextColor(TextColor);
            textView.setBackgroundResource(BackgroundResource);
            textView.setTypeface(typeface);
        }

        if (GridVisible) {
            for (ImageView imageView : line) {
                imageView.setVisibility(VISIBLE);
                imageView.setBackgroundColor(GridBackgroundColor);
            }
            line.get(0).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
            line.get(1).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
            line.get(2).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
            line.get(3).setLayoutParams(new LinearLayout.LayoutParams(GridThickness, LayoutParams.MATCH_PARENT));
            line.get(4).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
            line.get(5).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
            line.get(6).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
            line.get(7).setLayoutParams(new LinearLayout.LayoutParams(GridThickness, LayoutParams.MATCH_PARENT));
            line.get(8).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
            line.get(9).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
            line.get(10).setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, GridThickness));
        }

        delete_layout.setOnClickListener(this);
        delete_layout.setBackgroundResource(BackgroundResource);
        delete.setImageResource(ImageResource);
    }

    public String getDigits() {
        return digits;
    }

    public int getTextLengthLimit() {
        return TextLengthLimit;
    }

    public CustomNumPad setTextLengthLimit(int TextLengthLimit) {
        this.TextLengthLimit = TextLengthLimit;
        setup();
        return this;
    }

    public float getTextSize() {
        return TextSize;
    }

    public CustomNumPad setTextSize(int TextSize) {
        this.TextSize = TextSize;
        setup();
        return this;
    }

    public int getTextColor() {
        return TextColor;
    }

    public CustomNumPad setTextColor(int TextColor) {
        this.TextColor = TextColor;
        setup();
        return this;
    }

    public int getBackgroundResource() {
        return BackgroundResource;
    }

    public int getImageResource() {
        return ImageResource;
    }

    public boolean isGridVisible() {
        return GridVisible;
    }

    public CustomNumPad setGridVisible(boolean GridVisible) {
        this.GridVisible = GridVisible;
        setup();
        return CustomNumPad.this;
    }

    public int getGridBackgroundColor() {
        return GridBackgroundColor;
    }

    public CustomNumPad setGridBackgroundColor(int GridBackgroundColor) {
        this.GridBackgroundColor = GridBackgroundColor;
        setup();
        return CustomNumPad.this;
    }

    public int getGridThickness() {
        return GridThickness;
    }

    public CustomNumPad setGridThickness(int GridThickness) {
        this.GridThickness = GridThickness;
        setup();
        return CustomNumPad.this;
    }

    public Typeface getTypeface() {
        return typeface;
    }

    public ImageView getImageResourceView() {
        return delete;
    }

    public TextGetListner getTextGetListner() {
        return textGetListner;
    }

    @Override
    public void onClick(final View view) {

        if (view instanceof TextView && digits.length() < TextLengthLimit) {
            digits += ((TextView) view).getText();
        } else if (view instanceof FrameLayout && digits.length() >= 1) {
            digits = digits.substring(0, digits.length() - 1);
        }
        textGetListner.onTextChange(digits, TextLengthLimit - digits.length());
    }


}
