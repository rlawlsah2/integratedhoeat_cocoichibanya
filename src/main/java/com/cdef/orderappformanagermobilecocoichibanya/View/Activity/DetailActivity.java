package com.cdef.orderappformanagermobilecocoichibanya.View.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.androidhuman.rxfirebase2.database.RxFirebaseDatabase;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.orderappformanagermobilecocoichibanya.Dialog.MessageDialogBuilder;
import com.cdef.orderappformanagermobilecocoichibanya.OrderAppApplication;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.DefaultExceptionHandler;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.NavigationUtils;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.InitPersonFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.SelectMenuFragment;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.ActivityDetailBinding;
import com.google.firebase.database.FirebaseDatabase;

public class DetailActivity extends AppCompatActivity {


    ActivityDetailBinding binding;

    @Override
    protected void onStart() {
        super.onStart();
        OrderAppApplication.mContext = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        this.binding.setActivity(this);
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String fragment = bundle.getString("fragment");
            if (fragment != null) {
                if (fragment.equals(InitPersonFragment.class.getSimpleName())) {
                    NavigationUtils.templateOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, InitPersonFragment.class.getSimpleName(), bundle);
                } else if (fragment.equals(SelectMenuFragment.class.getSimpleName())) {
                    NavigationUtils.templateOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, SelectMenuFragment.class.getSimpleName(), bundle);
                }

            }
        } else {

        }
    }

    public void editCartItem(Order order) {
        LogUtil.d("카트 선택한거 액티비티로 옴: " + order.foodName);
        LogUtil.d("카트 선택한거 액티비티로 옴 : " + getSupportFragmentManager().findFragmentById(R.id.layoutFragment));
        LogUtil.d("카트 선택한거 액티비티로 옴 : " + getSupportFragmentManager().findFragmentByTag(SelectMenuFragment.class.getSimpleName()));


        if (getSupportFragmentManager().findFragmentByTag(SelectMenuFragment.class.getSimpleName()) != null) {
            ((SelectMenuFragment) getSupportFragmentManager().findFragmentByTag(SelectMenuFragment.class.getSimpleName())).
                    editCartItem(order);
        }

//
//        if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof SelectMenuFragment) {
//
//            ((SelectMenuFragment) getSupportFragmentManager().findFragmentById(R.id.layoutFragment)).
//                    editCartItem(order);
//        }
    }

    @Override
    public void onBackPressed() {
        LogUtil.d("스택이 쌓였는데1 : " + getSupportFragmentManager().getBackStackEntryCount());

        ///메뉴쪽에선 뒤로가기가 다른방향으로 쓰일 수 있으니 별도로 처리하자
        if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof SelectMenuFragment) {
            if (((SelectMenuFragment) getSupportFragmentManager().findFragmentById(R.id.layoutFragment)).isShowOption()) {
                ((SelectMenuFragment) getSupportFragmentManager().findFragmentById(R.id.layoutFragment)).closeOption(null);
                return;
            } else {

                LogUtil.d("지울거 있나 ");

                ///지울지 말지 선택
                ///1. orderId null 체크
                ///2. 카트 빈거 확인
                RxFirebaseDatabase.data(
                        FirebaseDatabase.getInstance().getReference()
                                .child(LoginUtil.getInstance(null).getmBrandName())
                                .child("tables")
                                .child(LoginUtil.getInstance(this).branchInfo.branchId)
                                .child(LoginUtil.getInstance(this).getmSelectedTableNo())
                                .child("IFSA_ORDER_ID"))
                        .subscribe((snapshot) -> {
                            LogUtil.d("지울거 있나  : " + snapshot);
                            LogUtil.d("지울거 있나  : " + (snapshot != null && snapshot.getValue() == null));

                            if (snapshot != null && snapshot.getValue() == null) {

                                MessageDialogBuilder.getInstance(this)
                                        .setContent(LoginUtil.getInstance(this).getmSelectedTableNo() + "번 테이블의\n장바구니/인원을\n초기화하시겠습니까?")
                                        .setOkButton("초기화", view -> {

                                            ///삭제대상
                                            ///1. cart
                                            ///2. order
                                            if (LoginUtil.getInstance(this).getmSelectedTableNo() != null) {
                                                FirebaseDatabase.getInstance().getReference()
                                                        .child(LoginUtil.getInstance(this).getmBrandName())
                                                        .child("cart")
                                                        .child(LoginUtil.getInstance(this).branchInfo.branchId)
                                                        .child(LoginUtil.getInstance(this).getmSelectedTableNo())
                                                        .setValue(null);

                                                FirebaseDatabase.getInstance().getReference()
                                                        .child(LoginUtil.getInstance(this).getmBrandName())
                                                        .child("tables")
                                                        .child(LoginUtil.getInstance(this).branchInfo.branchId)
                                                        .child(LoginUtil.getInstance(this).getmSelectedTableNo())
                                                        .child("members")
                                                        .setValue(null, (databaseError, databaseReference) -> {
                                                            if (databaseError != null) {
                                                                Toast.makeText(this, "초기화에 실패했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                                                                return;
                                                            }

                                                            Toast.makeText(this, "초기화 완료", Toast.LENGTH_SHORT).show();
                                                            MessageDialogBuilder.getInstance(this).dismiss();
                                                            finish();
                                                        });
                                            }
                                            else
                                            {
                                                finish();
                                            }
                                        })
                                        .setCancelButton("아니오", view -> {
                                            MessageDialogBuilder.getInstance(this).dismiss();
                                            finish();
                                        })
                                        .complete();
                                return;
                            }
                            else
                            {
                                finish();
                            }


                        }, error -> {

                        });

            }
        } else {


            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            } else

                super.onBackPressed();
        }
    }

    //
//    @Override
//    public void onBackPressed() {
//
//        LogUtil.d("스택이 쌓였는데1 : " + getSupportFragmentManager().getFragments().size());
//
//        Boolean temp = false;
//        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
//            if (((BaseFragment) fragment).loadingDialog != null) {
//                if (((BaseFragment) fragment).loadingDialog.isShowing()) {
//                    temp = true;
//                }
//
//
//            }
//        }
//        LogUtil.d("스택이 쌓였는데2 : " + temp);
//
//        if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof BillFragment) {
//            finish();
//            LogUtil.d("스택이 쌓였는데3 : " + getSupportFragmentManager().findFragmentById(R.id.layoutFragment));
//
//        }
//
//        if (!temp) {
//            LogUtil.d("스택이 쌓였는데4 : " + (getSupportFragmentManager().getFragments().size() > 1));
//
//            if (getSupportFragmentManager().getFragments().size() > 1) {
//                super.onBackPressed();
//            } else {
//                finish();
//            }
//
//        }
//    }

}
