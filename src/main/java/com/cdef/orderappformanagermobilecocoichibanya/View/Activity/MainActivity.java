package com.cdef.orderappformanagermobilecocoichibanya.View.Activity;

import android.app.NotificationManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.cdef.orderappformanagermobilecocoichibanya.BuildConfig;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.orderappformanagermobilecocoichibanya.OrderAppApplication;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.DefaultExceptionHandler;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.NavigationUtils;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.BaseFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.CartListFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.LoginFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.SelectTableFragment;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.ActivityMainBinding;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {


    ActivityMainBinding binding;
    NotificationCompat.Builder notiBuilder;
    NotificationManager notificationManager;
    int notiID = 1199;
    long initDataSize = 0;
    int notReadCount = 0;


    @Override
    protected void onStart() {
        super.onStart();
        OrderAppApplication.mContext = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);



        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        this.binding.setActivity(this);
        this.binding.setNotReadCount(0);
        OrderAppApplication.mContext = this;
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, LoginFragment.class.getSimpleName());

        ///노티피케이션 기본 설정
        this.notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        this.notiBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("조리가 완료되었습니다.")
                .setDefaults(android.app.Notification.DEFAULT_SOUND)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true);


        if (BuildConfig.CookState) {
            getSupportFragmentManager().addOnBackStackChangedListener(() -> {
                if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof LoginFragment) {
                    this.binding.setGoCompleteFlag(false);
                } else if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof CartListFragment) {
                    this.binding.setGoCompleteFlag(false);
                } else {
                    this.binding.setGoCompleteFlag(true);
                }
            });
        }

        this.binding.setGoCompleteFlag(false);

    }

    /**
     * 테이블 선택창이 메인으로 갔음.
     */
    public void goMain() {
        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, SelectTableFragment.class.getSimpleName());
//        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, SelectMenuFragment.class.getSimpleName());

        if (BuildConfig.CookState) {
            this.binding.setGoCompleteFlag(true);
            initOrderCompleteListener();
        }

    }

    private void initOrderCompleteListener() {
        //포스에 넣을 수 있는 상태인지 체크
        //로그인 이후니까 주문에 대한 리스너를 달자
        FirebaseDatabase.getInstance().getReference()
                .child(LoginUtil.getInstance(this).getmBrandName())
                .child("orderForKitchenComplete")
                .child(LoginUtil.getInstance(this).branchInfo.branchId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null) {
                            initDataSize = dataSnapshot.getChildrenCount();
                        }

                        ////들어오는걸 알림주기위한 코드
                        FirebaseDatabase.getInstance().getReference()
                                .child(LoginUtil.getInstance(getApplicationContext()).getmBrandName())
                                .child("orderForKitchenComplete")
                                .child(LoginUtil.getInstance(getApplicationContext()).branchInfo.branchId)
                                .addChildEventListener(orderCompleteListener);


                        ///들어오는걸 카운팅하기 위한 코드
                        //조건이 추가됨
                        FirebaseDatabase.getInstance().getReference()
                                .child(LoginUtil.getInstance(getApplicationContext()).getmBrandName())
                                .child("orderForKitchenComplete")
                                .child(LoginUtil.getInstance(getApplicationContext()).branchInfo.branchId)
                                .orderByChild("read")
                                .equalTo(null)
                                .addChildEventListener(orderCompleteNotReadListener);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public ChildEventListener orderCompleteListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot != null) {

                if (initDataSize <= 0) {
                    if (dataSnapshot.getValue(Order.class).read == null) {
                        LogUtil.d("새로운 주문이 들어왔다 : " + dataSnapshot.getValue(Order.class).foodName);
                        notiBuilder.setContentTitle(dataSnapshot.getValue(Order.class).orderedTableNo + "테이블 조리완료");
                        notiBuilder.setContentText(dataSnapshot.getValue(Order.class).foodName + "가 준비되었습니다.");
                        notificationManager.notify(notiID, notiBuilder.build());
                    }
                }
                initDataSize--;


            }

        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    public ChildEventListener orderCompleteNotReadListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            notReadCount++;
            LogUtil.d("조리완료 내역 상태 : onChildAdded : " + dataSnapshot);
            binding.setNotReadCount(notReadCount);
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            LogUtil.d("조리완료 내역 상태 : onChildChanged : " + dataSnapshot);

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            LogUtil.d("조리완료 내역 상태 : onChildRemoved : " + dataSnapshot);
            notReadCount--;
            binding.setNotReadCount(notReadCount);

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            LogUtil.d("조리완료 내역 상태 : onChildMoved : " + dataSnapshot);

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            LogUtil.d("조리완료 내역 상태 : onCancelled : " + databaseError);

        }
    };

    public void goCookCompleteList(View view) {
//        NavigationUtils.templateOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, CookCompleteListFragment.class.getSimpleName(), null);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

        } else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
            if (this.binding.buttonGoCompleteList.getAlpha() == 1)
                this.binding.buttonGoCompleteList.setAlpha(0.2f);
        } else {
            if (this.binding.buttonGoCompleteList.getAlpha() != 1)
                this.binding.buttonGoCompleteList.setAlpha(1.0f);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {

        Boolean temp = false;
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            LogUtil.d("등록된 프래그먼트 보자 : " + fragment);
            if (((BaseFragment) fragment).loadingDialog != null) {
                if (((BaseFragment) fragment).loadingDialog.isShowing()) {
                    temp = true;
                }


            }
        }

        if (!temp) {
            if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof SelectTableFragment) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

}
