package com.cdef.orderappformanagermobilecocoichibanya.View.Fragment;


import android.app.NotificationManager;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Extra;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Notification;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.dataModel.firebaseDataSet.TableInfo;
import com.cdef.orderappformanagermobilecocoichibanya.Dialog.LoadingDialog.LoadingDialog;
import com.cdef.orderappformanagermobilecocoichibanya.Dialog.MessageDialogBuilder;
import com.cdef.orderappformanagermobilecocoichibanya.OrderAppApplication;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.commonmodule.Utils.FBPathBuilder;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.MSSQLUtils;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.NavigationUtils;
import com.cdef.commonmodule.Utils.Utils;
import com.cdef.orderappformanagermobilecocoichibanya.View.Activity.DetailActivity;
import com.cdef.orderappformanagermobilecocoichibanya.View.Adapter.CartListAdapter;
import com.cdef.orderappformanagermobilecocoichibanya.View.ViewHolder.CartListViewHolder;
import com.cdef.orderappformanagermobilecocoichibanya.ViewModel.CartViewModel;
import com.cdef.orderappformanagermobilecocoichibanya.ViewModel.OrderViewModel;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.FragmentCartListBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;


/**
 * Created by kimjinmo on 2018. 4. 18
 * <p>
 * 장바구니 내역 확인하는 화면
 */

public class CartListFragment extends BaseFragment {

    FragmentCartListBinding binding;
    CartListAdapter mCartListAdapter = null;
    CartViewModel cartViewModel;
    OrderViewModel orderViewModel;
    private boolean isOrdering = false;
    private int totalPrice_textview = 0;
    private int totalPrice_pos = 0;
    private boolean isFirstOrder = false;

    PosChecker posChecker;
    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    NotificationManager notificationManager;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_loading));
        this.orderViewModel = new OrderViewModel();
        this.orderViewModel.initRepository(getContext());

        this.cartViewModel = new CartViewModel();
        this.cartViewModel.initSetting.observe(this, initsetting -> {

            switch (initsetting) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();

                    break;
                case ERROR:
                    this.loadingDialog.dismiss();

                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    break;
            }

        });
        ///cart 리스트 처리
        mCartListAdapter = new CartListAdapter(Order.class, R.layout.adapter_cart_list, CartListViewHolder.class,
                FirebaseDatabase.getInstance().getReference()
                        .child(LoginUtil.getInstance(getContext()).getmBrandName())
                        .child("cart")
                        .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                        .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                        .orderByChild("time")
        );

        mCartListAdapter.registerAdapterDataObserver(adapterDataObserver);
        mCartListAdapter.setListener(new CartListAdapter.OnItemClickListener() {
            @Override
            public void selectedItem(View view, Order order) {
                ////액티비티로 보낸다
                if (getActivity() instanceof DetailActivity) {
                    ((DetailActivity) getActivity()).editCartItem(order);
                    getActivity().onBackPressed();
                }
            }

            @Override
            public void changeQuantity(View view, Order order, Boolean isPlus) {
                if (isPlus) {
                    order.quantity += 1;
                    loadingDialog.show();
                    FirebaseDatabase.getInstance().getReference()
                            .child(LoginUtil.getInstance(getContext()).getmBrandName())
                            .child("cart")
                            .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                            .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                            .child(order.key)
                            .child("quantity")
                            .setValue(order.quantity, (databaseError, databaseReference) -> {
                                if (databaseError == null) {
                                } else
                                    Toast.makeText(getContext(), "잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();

                                loadingDialog.dismiss();
                            });
                } else {
                    if (order.quantity > 1) {
                        order.quantity -= 1;
                        loadingDialog.show();
                        FirebaseDatabase.getInstance().getReference()
                                .child(LoginUtil.getInstance(getContext()).getmBrandName())
                                .child("cart")
                                .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                                .child(order.key)
                                .child("quantity")
                                .setValue(order.quantity, (databaseError, databaseReference) -> {
                                    if (databaseError == null) {
                                    } else
                                        Toast.makeText(getContext(), "잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();

                                    loadingDialog.dismiss();
                                });
                    }
                }
            }

            @Override
            public void deleteItem(View view, Order order) {
                loadingDialog.show();
                FirebaseDatabase.getInstance().getReference()
                        .child(LoginUtil.getInstance(getContext()).getmBrandName())
                        .child("cart")
                        .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                        .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                        .child(order.key)
                        .setValue(null, (databaseError, databaseReference) -> {
                            if (databaseError == null) {
                                Toast.makeText(getContext(), "삭제되었습니다.", Toast.LENGTH_SHORT).show();
                                mCartListAdapter.notifyDataSetChanged();
                            } else
                                Toast.makeText(getContext(), "잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();

                            loadingDialog.dismiss();
                        });

            }
        });

        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(mCartListAdapter);
        alphaAdapter.setInterpolator(new OvershootInterpolator());
        this.binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        this.binding.recyclerView.setAdapter(alphaAdapter);

        LogUtil.d("장바구니 테이블 번호 : " + LoginUtil.getInstance(getContext()).getmSelectedTableNo());

        this.cartViewModel.getTableMembers(LoginUtil.getInstance(getContext()).branchInfo.branchId, LoginUtil.getInstance(getContext()).getmSelectedTableNo());

    }

    public void close(View view) {
        getActivity().onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // TODO: inflate a fragment view
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_cart_list, container, false);
        this.binding.setFragment(this);
        return binding.getRoot();
    }

    public void goSelectTable(View view) {
        LogUtil.d("테이블 선택하러 가자 : " + view);
        NavigationUtils.templateOpenFragment(getFragmentManager(), R.id.layoutFragment, SelectTableFragment.class.getSimpleName(), null);
    }

    private RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            setCartTotalPrice();
            if (mCartListAdapter != null)
                binding.setItemCount(mCartListAdapter.getItemCount());

        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            setCartTotalPrice();
            if (mCartListAdapter != null)
                binding.setItemCount(mCartListAdapter.getItemCount());
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            setCartTotalPrice();
            if (mCartListAdapter != null)
                binding.setItemCount(mCartListAdapter.getItemCount());
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            setCartTotalPrice();
            if (mCartListAdapter != null)
                binding.setItemCount(mCartListAdapter.getItemCount());
        }
    };

    public void goOrder(View view) {
        if (this.mCartListAdapter.getItemCount() > 0) {
            goOrder();
//            NavigationUtils.templateOpenFragment(getFragmentManager(), R.id.layoutFragment, BillFragment.class.getSimpleName(), null);
        } else {
            Toast.makeText(getContext(), "장바구니에 담긴 내역이 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    public void finish(View view) {
        getActivity().onBackPressed();

    }

    public void removeAllCart(View view) {


        MessageDialogBuilder.getInstance(getContext())
                .setTitle("알림")
                .setContent("담아둔 내역을 전부 삭제합니다. \n계속하시겠습니까?")
                .setDefaultButtons(view1 -> {
                    this.cartViewModel.removeCartALLItem(
                            LoginUtil.getInstance(getContext()).branchInfo.branchId,
                            LoginUtil.getInstance(getContext()).getmSelectedTableNo()

                    );
                    MessageDialogBuilder.getInstance(getContext()).dismiss();
                })
                .complete();


    }

    /**
     * 큐를 활용한 주문 방법
     **/
    public void queueOrder() {

        if (loadingDialog != null)
            loadingDialog.setTitleNew("주문중..");

//            loadingDialog.show();

        if (MSSQLUtils.getInstance().checkConnection()) {

            FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                    .child("cart")
                    .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                    .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot cartResult) {

                            if (cartResult != null && cartResult.getChildrenCount() > 0) {


                                ///카트에 담긴 아이템들을 불러온다
                                JSONObject orderObject = new JSONObject();
                                try {


                                    orderObject.put("userId", LoginUtil.getInstance(getContext()).branchInfo.branchId);
                                    orderObject.put("brandName", LoginUtil.getInstance(getContext()).getmBrandName());
                                    orderObject.put("branchId", LoginUtil.getInstance(getContext()).branchInfo.branchId);
                                    orderObject.put("tableNo", LoginUtil.getInstance(getContext()).getmSelectedTableNo());
                                    orderObject.put("orderType", LoginUtil.getInstance(getContext()).getmSelectedTableNo().startsWith("D") ? "H" :
                                            (LoginUtil.getInstance(getContext()).getmSelectedTableNo().startsWith("A") ? "P" :
                                                    (LoginUtil.getInstance(getContext()).getmSelectedTableNo().startsWith("B") ? "D" :
                                                            (LoginUtil.getInstance(getContext()).getmSelectedTableNo().startsWith("F") ? "D" : "H"))));


                                    totalPrice_pos = 0;

                                    JSONArray orders = new JSONArray();
                                    for (DataSnapshot snapshot : cartResult.getChildren()) {
                                        Order item = snapshot.getValue(Order.class);
                                        JSONObject ordersItem = new JSONObject();   ///orders에 들어갈 각 항목
                                        JSONArray ordersItemSideOptions = new JSONArray();
                                        ordersItem.put("foodUid", item.foodUid);
                                        ordersItem.put("foodName", item.foodName);
                                        ordersItem.put("quantity", item.quantity);
                                        ordersItem.put("price", item.price);
                                        ordersItem.put("CMDT_CD", item.CMDTCD);
                                        ordersItem.put("categoryName", item.categoryName);


                                        for (String optionKey : item.options.keySet()) {
                                            JSONObject ordersItemSideOptionItem = new JSONObject();   ///orders에 들어갈 각 항목의 옵션
                                            ordersItemSideOptionItem.put("foodUid", item.options.get(optionKey).foodUid);
                                            ordersItemSideOptionItem.put("foodName", item.options.get(optionKey).foodName);
                                            ordersItemSideOptionItem.put("price", item.options.get(optionKey).price);
                                            ordersItemSideOptionItem.put("quantity", item.options.get(optionKey).quantity);
                                            ordersItemSideOptionItem.put("CMDT_CD", item.options.get(optionKey).CMDTCD);

                                            ordersItemSideOptions.put(ordersItemSideOptionItem);
                                        }

                                        if (ordersItemSideOptions.length() > 0) {
                                            ordersItem.put("options", ordersItemSideOptions);
                                        }

                                        orders.put(ordersItem);
                                        totalPrice_pos += (item.price * item.quantity);

                                    }

                                    orderObject.put("orders", orders);
                                    orderObject.put("totalPrice", totalPrice_pos);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                MessageDialogBuilder.getInstance(getContext())
                                        .setContent("주문내역을 확인해주세요.\n주문후 취소할 수 없습니다.\n주문하시겠습니까?")
                                        .setOnDismissListener_(dialogInterface -> {
                                            isOrdering = false;
                                        })
                                        .setCancelButton("취소", view -> {
                                            MessageDialogBuilder.getInstance(getContext())
                                                    .dismiss();
                                            loadingDialog.dismiss();
                                            isOrdering = false;
                                        })
                                        .setOkButton("확인", view -> {
                                            LogUtil.d("확인 버튼의 selected 상태값 : " + view.isSelected());

                                            if (!view.isSelected()) {
                                                view.setSelected(true);
                                                loadingDialog.show();
                                                orderViewModel.goOrder("http://" + LoginUtil.getInstance(getContext()).getsPosIp() + ":9626/api/remote/order", orderObject.toString())
                                                        .subscribe(
                                                                response -> {
                                                                    LogUtil.d("주문 goOrder response : " + response);
                                                                    loadingDialog.dismiss();
                                                                    isOrdering = false;
//                                                                MessageDialogBuilder.getInstance(getContext()).dismiss();

                                                                    FirebaseDatabase.getInstance().getReference()
                                                                            .child(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                            .child("cart")
                                                                            .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                            .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                                                                            .setValue(null);

                                                                    MessageDialogBuilder.getInstance(getContext()).dismiss();
                                                                    Toast.makeText(getContext(), "주문이 완료되었습니다", Toast.LENGTH_LONG).show();
                                                                    getActivity().finish();

                                                                },
                                                                error -> {
                                                                    LogUtil.d("주문 modelView error : " + error);
                                                                    loadingDialog.dismiss();
                                                                    isOrdering = false;
                                                                    MessageDialogBuilder.getInstance(getContext()).dismiss();
                                                                    Toast.makeText(getContext(), "포스기 주문 프로그램을 실행해주세요.", Toast.LENGTH_SHORT).show();

                                                                },
                                                                () -> {

                                                                    LogUtil.d("주문 modelView complete : ");
                                                                    loadingDialog.dismiss();
                                                                    isOrdering = false;
                                                                    MessageDialogBuilder.getInstance(getContext()).dismiss();

                                                                });


                                            } else {
                                                Toast.makeText(getContext(), "주문중입니다.", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .complete();
                            } else {
                                Toast.makeText(getContext(), "주문할 메뉴를 선택해주세요.", Toast.LENGTH_SHORT).show();
                                isOrdering = false;
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(getContext(), "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                            isOrdering = false;
                        }
                    });
        } else {
            Toast.makeText(getContext(), "포스 프로그램이 켜져있는지 확인해주세요.", Toast.LENGTH_SHORT).show();
        }


    }


    public void goOrder() {
//        posChecker = new PosChecker();
//        posChecker.execute();
//        queueOrder();
        if (OrderAppApplication.getUseQueue(getContext()) == null) {
//            기존방식
            posChecker = new PosChecker();
            posChecker.execute();
//            Toast.makeText(getContext(), "이전방식 주문", Toast.LENGTH_SHORT).show();

        } else {
            PosCheckerForOrderQueue posCheckerForOrderQueue = new PosCheckerForOrderQueue();
            posCheckerForOrderQueue.execute();
//            Toast.makeText(getContext(), "큐 주문", Toast.LENGTH_SHORT).show();

        }


    }


    public class PosCheckerForOrderQueue extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LogUtil.d("포스 체커 onPreExecute loadingDialog : " + loadingDialog);
            if (loadingDialog != null)
                loadingDialog.show();
        }

        /**
         * 백그라운드에서 포스를 먼저 체크한다.
         */
        @Override
        protected Boolean doInBackground(Void... voids) {
            return MSSQLUtils.getInstance().checkConnection();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            LogUtil.d("포스 체커 onProgressUpdate loadingDialog : " + loadingDialog);
            if (loadingDialog != null)
                loadingDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean isConnected) {
            super.onPostExecute(isConnected);
            if (isConnected) {
                FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                        .child("cart")
                        .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                        .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot cartResult) {

                                if (cartResult != null && cartResult.getChildrenCount() > 0) {


                                    ///카트에 담긴 아이템들을 불러온다
                                    JSONObject orderObject = new JSONObject();
                                    JSONObject member = new JSONObject();

                                    try {


                                        orderObject.put("userId", LoginUtil.getInstance(getContext()).branchInfo.branchId);
                                        orderObject.put("brandName", LoginUtil.getInstance(getContext()).getmBrandName());
                                        orderObject.put("branchId", LoginUtil.getInstance(getContext()).branchInfo.branchId);
                                        orderObject.put("tableNo", LoginUtil.getInstance(getContext()).getmSelectedTableNo());
                                        orderObject.put("orderType", LoginUtil.getInstance(getContext()).getmSelectedTableNo().startsWith("D") ? "H" :
                                                (LoginUtil.getInstance(getContext()).getmSelectedTableNo().startsWith("A") ? "P" :
                                                        (LoginUtil.getInstance(getContext()).getmSelectedTableNo().startsWith("B") ? "D" :
                                                                (LoginUtil.getInstance(getContext()).getmSelectedTableNo().startsWith("F") ? "D" : "H"))));


                                        member.put("men", cartViewModel.mTableMember.getValue().men);
                                        member.put("women", cartViewModel.mTableMember.getValue().women);


                                        orderObject.put("members", member);


                                        totalPrice_pos = 0;

                                        JSONArray orders = new JSONArray();
                                        for (DataSnapshot snapshot : cartResult.getChildren()) {
                                            Order item = snapshot.getValue(Order.class);
                                            JSONObject ordersItem = new JSONObject();   ///orders에 들어갈 각 항목
                                            JSONArray ordersItemSideOptions = new JSONArray();
                                            ordersItem.put("foodUid", item.foodUid);
                                            ordersItem.put("foodName", item.foodName);
                                            ordersItem.put("quantity", item.quantity);
                                            ordersItem.put("price", item.price);
                                            ordersItem.put("CMDT_CD", item.CMDTCD);
                                            ordersItem.put("categoryName", item.categoryName);


                                            for (String optionKey : item.options.keySet()) {
                                                JSONObject ordersItemSideOptionItem = new JSONObject();   ///orders에 들어갈 각 항목의 옵션
                                                ordersItemSideOptionItem.put("foodUid", item.options.get(optionKey).foodUid);
                                                ordersItemSideOptionItem.put("foodName", item.options.get(optionKey).foodName);
                                                ordersItemSideOptionItem.put("price", item.options.get(optionKey).price);
                                                ordersItemSideOptionItem.put("quantity", item.options.get(optionKey).quantity);
                                                ordersItemSideOptionItem.put("CMDT_CD", item.options.get(optionKey).CMDTCD);

                                                ordersItemSideOptions.put(ordersItemSideOptionItem);
                                            }

                                            if (ordersItemSideOptions.length() > 0) {
                                                ordersItem.put("options", ordersItemSideOptions);
                                            }

                                            orders.put(ordersItem);
                                            totalPrice_pos += (item.price * item.quantity);

                                        }

                                        orderObject.put("orders", orders);
                                        orderObject.put("totalPrice", totalPrice_pos);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    MessageDialogBuilder.getInstance(getContext())
                                            .setContent("주문내역을 확인해주세요.\n주문후 취소할 수 없습니다.\n주문하시겠습니까?")
                                            .setOnDismissListener_(dialogInterface -> {
                                                isOrdering = false;
                                            })
                                            .setCancelButton("취소", view -> {
                                                MessageDialogBuilder.getInstance(getContext())
                                                        .dismiss();
                                                loadingDialog.dismiss();
                                                isOrdering = false;
                                            })
                                            .setOkButton("확인", view -> {
                                                LogUtil.d("확인 버튼의 selected 상태값 : " + view.isSelected());

                                                if (!view.isSelected()) {
                                                    view.setSelected(true);
                                                    loadingDialog.show();
                                                    orderViewModel.goOrder("http://" + LoginUtil.getInstance(getContext()).getsPosIp() + ":9626/api/remote/order", orderObject.toString())
                                                            .subscribe(
                                                                    response -> {
                                                                        LogUtil.d("주문 goOrder response : " + response);
                                                                        loadingDialog.dismiss();
                                                                        isOrdering = false;
//                                                                MessageDialogBuilder.getInstance(getContext()).dismiss();

                                                                        FirebaseDatabase.getInstance().getReference()
                                                                                .child(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                .child("cart")
                                                                                .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                                                                                .setValue(null);

                                                                        MessageDialogBuilder.getInstance(getContext()).dismiss();
                                                                        Toast.makeText(getContext(), "주문이 완료되었습니다", Toast.LENGTH_LONG).show();
                                                                        getActivity().finish();

                                                                    },
                                                                    error -> {
                                                                        LogUtil.d("주문 modelView error : " + error);
                                                                        loadingDialog.dismiss();
                                                                        isOrdering = false;
                                                                        MessageDialogBuilder.getInstance(getContext()).dismiss();
                                                                        Toast.makeText(getContext(), "포스기 주문 프로그램을 실행해주세요.", Toast.LENGTH_SHORT).show();

                                                                    },
                                                                    () -> {

                                                                        LogUtil.d("주문 modelView complete : ");
                                                                        loadingDialog.dismiss();
                                                                        isOrdering = false;
                                                                        MessageDialogBuilder.getInstance(getContext()).dismiss();

                                                                    });


                                                } else {
                                                    Toast.makeText(getContext(), "주문중입니다.", Toast.LENGTH_SHORT).show();
                                                }
                                            })
                                            .complete();
                                } else {
                                    Toast.makeText(getContext(), "주문할 메뉴를 선택해주세요.", Toast.LENGTH_SHORT).show();
                                    isOrdering = false;
                                }


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Toast.makeText(getContext(), "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                                isOrdering = false;
                                loadingDialog.dismiss();

                            }
                        });
            } else {
                Toast.makeText(getContext(), "포스 프로그램이 켜져있는지 확인해주세요.", Toast.LENGTH_SHORT).show();
                loadingDialog.dismiss();
            }

        }
    }


    public class PosChecker extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LogUtil.d("포스 체커 onPreExecute loadingDialog : " + loadingDialog);
            if (loadingDialog != null)
                loadingDialog.show();
        }

        /**
         * 백그라운드에서 포스를 먼저 체크한다.
         */
        @Override
        protected Boolean doInBackground(Void... voids) {
            return MSSQLUtils.getInstance().checkConnection();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            LogUtil.d("포스 체커 onProgressUpdate loadingDialog : " + loadingDialog);
            if (loadingDialog != null)
                loadingDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean isConnected) {
            super.onPostExecute(isConnected);

            if (loadingDialog != null)
                loadingDialog.dismiss();
            ///이후 주문 처리
            if (isConnected) {
                isFirstOrder = true;
                MSSQLUtils.getInstance().log("주문시작");
                if (isOrdering != true) {
                    isOrdering = true;
                    FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                            .child("cart")
                            .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                            .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot cartResult) {

                                    if (cartResult != null && cartResult.getChildrenCount() > 0) {
                                        MessageDialogBuilder.getInstance(getContext())
                                                .setContent("주문내역을 확인해주세요.\n주문후 취소할 수 없습니다.\n주문하시겠습니까?")
                                                .setOnDismissListener_(dialogInterface -> {
                                                    isOrdering = false;
                                                })
                                                .setCancelButton("취소", view -> {
                                                    MessageDialogBuilder.getInstance(getContext())
                                                            .dismiss();
                                                    loadingDialog.dismiss();
                                                    isOrdering = false;
                                                })
                                                .setOkButton("확인", view -> {
                                                    LogUtil.d("확인 버튼의 selected 상태값 : " + view.isSelected());

                                                    if (!view.isSelected()) {
                                                        view.setSelected(true);
                                                        isOrdering = true;
                                                        loadingDialog.setTitleNew("주문중..");
                                                        loadingDialog.show();
                                                        ////IFSA_ORDER_ID 확인
                                                        FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                .child("tables")
                                                                .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
//                                                                .child("IFSA_ORDER_ID")
                                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(DataSnapshot table) {
                                                                        ///IFSA_ORDER_ID 을 확인해보자.
                                                                        String IFSA_ORDER_ID = null;
                                                                        String tableType = "H";
                                                                        TableInfo tableInfo;
                                                                        if (table != null) {
                                                                            tableInfo = table.getValue(TableInfo.class);
                                                                            IFSA_ORDER_ID = tableInfo.IFSA_ORDER_ID;
                                                                            ///작업전 백업
//                                                                            tableType = tableInfo.tableType == null ? "H" : (tableInfo.tableType.length() > 0 ? tableInfo.tableType : "H");
                                                                            tableType = tableInfo.tableType == null ? null : (tableInfo.tableType.length() > 0 ? tableInfo.tableType : null);//파베에 자료가 있으면 그걸로 써라
                                                                            if (tableType == null) { ///파베에 없었으면 이거 써
                                                                                try {
                                                                                    Integer.parseInt(LoginUtil.getInstance(getContext()).getmSelectedTableNo());
                                                                                    tableType = "H";
                                                                                } catch (Exception e) {
                                                                                    switch (LoginUtil.getInstance(getContext()).getmSelectedTableNo().substring(0, 1)) {
                                                                                        case "D":
                                                                                            tableType = "H";
                                                                                            break;
                                                                                        case "A":
                                                                                            tableType = "P";
                                                                                            break;
                                                                                        case "B":
                                                                                            tableType = "D";
                                                                                            break;
                                                                                        case "F":
                                                                                            tableType = "D";
                                                                                            break;
                                                                                    }
                                                                                }
                                                                            }

                                                                            LogUtil.d("주문 넣기 직전 타입 : " + tableType);


                                                                            if (IFSA_ORDER_ID != null) {
                                                                                isFirstOrder = false;
                                                                                LoginUtil.getInstance(getContext()).setmOrderID(IFSA_ORDER_ID);
                                                                            } else //비워져 있다는건 어찌되었든 파베 데이터에 없다는 뜻. 새로운 값을 넣어라
                                                                            {

                                                                                isFirstOrder = true;

                                                                                LoginUtil.getInstance(getContext()).setmOrderID(
                                                                                        Utils.initOrderID(
                                                                                                LoginUtil.getInstance(getContext()).getsBranchUid()
                                                                                                , LoginUtil.getInstance(getContext()).getmSelectedTableNo()
                                                                                        )
                                                                                );
                                                                            }
                                                                        } else    //비워져 있다는건 테이블이 초기화 되었다는 뜻. 새로운 값을 넣어라
                                                                        {
//                                                            LoginUtil.getInstance(getContext()).setmOrderID(null);

                                                                            isFirstOrder = true;

                                                                            LoginUtil.getInstance(getContext()).setmOrderID(
                                                                                    Utils.initOrderID(
                                                                                            LoginUtil.getInstance(getContext()).getsBranchUid()
                                                                                            , LoginUtil.getInstance(getContext()).getmSelectedTableNo()
                                                                                    )
                                                                            );
                                                                        }
                                                                        IFSA_ORDER_ID = LoginUtil.getInstance(getContext()).getmOrderID();

                                                                        Map<String, Object> order = new HashMap<>();    //주문을 담을 객체
                                                                        ///1. IFSA_ORDER_ID를 table 밑에 저장
                                                                        order.put(

                                                                                FBPathBuilder.getInstance()
                                                                                        .init()
                                                                                        .set(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                        .set("tables")
                                                                                        .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                        .set(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                                                                                        .set("IFSA_ORDER_ID")
                                                                                        .complete()
                                                                                , IFSA_ORDER_ID);

                                                                        ///2. 주문내역 집어넣기
//                                                        Map<String, Object> orderList = new HashMap<>();
                                                                        Order tmpOrder;
                                                                        String key;
                                                                        String timeStamp = Utils.getDate("yy-MM-dd HH:mm:ss");
                                                                        for (DataSnapshot item : cartResult.getChildren()) {
                                                                            tmpOrder = item.getValue(Order.class);
                                                                            tmpOrder.time = timeStamp;
                                                                            key = FirebaseDatabase.getInstance().getReference()
                                                                                    .child(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                    .child("orderData")
                                                                                    .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                    .child(IFSA_ORDER_ID)
                                                                                    .child("orders")
                                                                                    .push().getKey();
                                                                            tmpOrder.key = key;
                                                                            ////1. 기본 주문 넣기
                                                                            order.put(
                                                                                    FBPathBuilder.getInstance()
                                                                                            .init()
                                                                                            .set(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                            .set("orderData")
                                                                                            .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                            .set(IFSA_ORDER_ID)
                                                                                            .set("orders")
                                                                                            .set(key)
                                                                                            .complete()

                                                                                    , tmpOrder
                                                                            );

                                                                            ///2. orderForKitchen 주문넣기
                                                                            if (tmpOrder.isKitchen == 1) {
                                                                                tmpOrder.orderId = IFSA_ORDER_ID;
                                                                                order.put(
                                                                                        FBPathBuilder.getInstance()
                                                                                                .init()
                                                                                                .set(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                                .set("orderForKitchen")
                                                                                                .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                                .set(key)
                                                                                                .complete()

                                                                                        , tmpOrder
                                                                                );
                                                                            }
                                                                        }

                                                                        ///(2) 주문코드
                                                                        order.put(
                                                                                FBPathBuilder.getInstance()
                                                                                        .init()
                                                                                        .set(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                        .set("orderData")
                                                                                        .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                        .set(IFSA_ORDER_ID)
                                                                                        .set("tableNo")
                                                                                        .complete()
                                                                                ,
                                                                                LoginUtil.getInstance(getContext()).getmSelectedTableNo()
                                                                        );

                                                                        ///3. 노티피케이션 데이터
                                                                        totalPrice_pos = 0;
                                                                        HashMap<String, Order> map = new HashMap<>();
                                                                        Notification notification = new Notification();
                                                                        notification.content = "/--";
                                                                        notification.read = false;
                                                                        notification.tableNo = LoginUtil.getInstance(getContext()).getmSelectedTableNo();
                                                                        notification.title = getString(R.string.notificationTitleOrder);
                                                                        notification.type = getString(R.string.notificationTypeOrder);
                                                                        notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");
                                                                        String contentItem = "";
                                                                        for (DataSnapshot item : cartResult.getChildren()) {
                                                                            Order cart = item.getValue(Order.class);
                                                                            contentItem = "";

                                                                            if (cart.options != null && cart.options.size() > 0) {
                                                                                contentItem = cart.foodName;
                                                                                StringBuilder sb = new StringBuilder();
                                                                                for (String optionKey : cart.options.keySet()) {
                                                                                    sb.append((cart.options.get(optionKey).foodName));
                                                                                    sb.append("(").append(cart.options.get(optionKey).quantity).append("ea)");
                                                                                    sb.append(", ");
                                                                                }

                                                                                try {
                                                                                    int lastIndex = sb.toString().lastIndexOf(", ");
                                                                                    contentItem += "(" + (sb.toString().substring(0, lastIndex)) + ")";//선택한 메뉴 옵션들 나열 지정

                                                                                } catch (Exception e) {
                                                                                    contentItem += "(" + (sb.toString()) + ")"; //선택한 메뉴 옵션들 나열 지정

                                                                                }

                                                                            } else {
                                                                                contentItem = cart.foodName;
                                                                            }
                                                                            notification.content = notification.content + "/ " + contentItem + ": " + cart.quantity + "개  ";
                                                                            totalPrice_pos += (cart.price * cart.quantity);
                                                                            map.put(item.getKey(), cart);
                                                                        }
                                                                        if (notification.content != null) {
                                                                            notification.content = notification.content.replace("/--/", "");
                                                                        }
                                                                        notification.content += "(담당자" + LoginUtil.getInstance(getContext()).branchInfo.branchId + ")";
                                                                        order.put(
                                                                                FBPathBuilder.getInstance()
                                                                                        .init()
                                                                                        .set(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                        .set("notification")
                                                                                        .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                        .set(FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                                .child("notification")
                                                                                                .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                                .push().getKey())
                                                                                        .complete()
                                                                                , notification
                                                                        );

                                                                        ///이후는 주문 성공시 돌아갈 항목들
                                                                        //4.카트삭제
                                                                        order.put(
                                                                                FBPathBuilder.getInstance().init()
                                                                                        .set(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                        .set("cart")
                                                                                        .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                        .set(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                                                                                        .complete()
                                                                                ,
                                                                                null
                                                                        );
                                                                        String finalTableType = tableType;
                                                                        FirebaseDatabase.getInstance().getReference()
                                                                                .updateChildren(order, (databaseError, databaseReference) -> {
                                                                                    LogUtil.d("주문이 성공했는가 : " + databaseError);
                                                                                    if (databaseError == null) {

                                                                                        String tableNo = LoginUtil.getInstance(getContext()).getmOrderSTBL_CD();
                                                                                        LogUtil.d("포스 테이블 번호 조회 전  : " + LoginUtil.getInstance(getContext()).getmSelectedTableNo());


                                                                                        if (tableNo == null) {
                                                                                            tableNo = MSSQLUtils.getInstance().getUserID(LoginUtil.getInstance(getContext()).getmSelectedTableNo());
                                                                                            if (tableNo != null) {
                                                                                                LoginUtil.getInstance(getContext()).setmOrderSTBL_CD(tableNo);
                                                                                            }
                                                                                        }
                                                                                        LogUtil.d("포스 테이블 번호 조회 후 : " + tableNo);

                                                                                        if (tableNo != null && !tableNo.startsWith("[error]")) {
                                                                                            LogUtil.d("주문 가격 확인 setCartTotalPrice 주문직전 : " + totalPrice_pos);


                                                                                            if (MSSQLUtils.getInstance().insertOrder_(totalPrice_pos, tableNo, finalTableType, isFirstOrder, map, cartViewModel.mTableMember.getValue().men + "", cartViewModel.mTableMember.getValue().women + "") == 1)  //포스넣기 성공
                                                                                            {
                                                                                                loadingDialog.dismiss();
                                                                                                isOrdering = false;

                                                                                                ///
                                                                                                MessageDialogBuilder.getInstance(getContext()).dismiss();
                                                                                                Toast.makeText(getContext(), "주문이 완료되었습니다", Toast.LENGTH_LONG).show();
                                                                                                getActivity().finish();
                                                                                            } else {
                                                                                                ///노티 띄워라.
                                                                                                notification.title = "[수기 입력] 메뉴 추가";
                                                                                                Extra extra = new Extra();
                                                                                                extra.ref = "androidApp";
                                                                                                extra.orderId = LoginUtil.getInstance(getContext()).getmOrderID();
                                                                                                notification.extra = extra;
                                                                                                notification.type = "order_danger";

                                                                                                FirebaseDatabase.getInstance()
                                                                                                        .getReference()
                                                                                                        .child(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                                        .child("notification")
                                                                                                        .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                                        .push()
                                                                                                        .setValue(notification);


//                                                                                                MessageDialogBuilder.getInstance(getContext())
//                                                                                                        .setContent("주문이 완료되었습니다.(E1)")
//                                                                                                        .setDefaultButton()
//                                                                                                        .setOnDismissListener_(dialogInterface -> getActivity().onBackPressed())
//                                                                                                        .complete();
                                                                                                loadingDialog.dismiss();
                                                                                                isOrdering = false;
                                                                                                showNotification(LoginUtil.getInstance(getContext()).getmSelectedTableNo(), notification.time);
                                                                                                MessageDialogBuilder.getInstance(getContext()).dismiss();

                                                                                                Toast.makeText(getContext(), "주문이 완료되었습니다(E1)", Toast.LENGTH_LONG).show();
                                                                                                getActivity().finish();


                                                                                            }
                                                                                        } else {
                                                                                            ///노티 띄워라.
                                                                                            notification.title = "[수기 입력] 메뉴 추가";
                                                                                            Extra extra = new Extra();
                                                                                            extra.ref = "androidApp";
                                                                                            extra.orderId = LoginUtil.getInstance(getContext()).getmOrderID();
                                                                                            notification.extra = extra;
                                                                                            notification.type = "order_danger";

                                                                                            FirebaseDatabase.getInstance()
                                                                                                    .getReference()
                                                                                                    .child(LoginUtil.getInstance(getContext()).getmBrandName())
                                                                                                    .child("notification")
                                                                                                    .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                                                                                    .push()
                                                                                                    .setValue(notification);

//                                                                                            MessageDialogBuilder.getInstance(getContext())
//                                                                                                    .setContent("주문이 완료되었습니다.(E2)")
//                                                                                                    .setDefaultButton()
//                                                                                                    .setOnDismissListener_(dialogInterface -> getActivity().onBackPressed())
//                                                                                                    .complete();
                                                                                            loadingDialog.dismiss();
                                                                                            isOrdering = false;
                                                                                            showNotification(LoginUtil.getInstance(getContext()).getmSelectedTableNo(), notification.time);
                                                                                            MessageDialogBuilder.getInstance(getContext()).dismiss();

                                                                                            Toast.makeText(getContext(), "주문이 완료되었습니다(E2)", Toast.LENGTH_LONG).show();
                                                                                            getActivity().finish();


                                                                                        }


                                                                                        LoginUtil.getInstance(getContext()).setmOrderSTBL_CD(null);
                                                                                        LoginUtil.getInstance(getContext()).setmOrderID(null);
//
//                                                                                            MessageDialogBuilder.getInstance(getContext())
//                                                                                                    .setContent("주문이 완료되었습니다.(E3)")
//                                                                                                    .setDefaultButton()
//                                                                                                    .setOnDismissListener_(dialogInterface -> getActivity().onBackPressed())
//                                                                                                    .complete();

                                                                                    } else {
                                                                                        LoginUtil.getInstance(getContext()).setmOrderID(null);
                                                                                        LoginUtil.getInstance(getContext()).setmOrderSTBL_CD(null);
                                                                                        Toast.makeText(getContext(), "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                                                                                        isOrdering = false;
                                                                                    }
                                                                                });


                                                                    }

                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {
                                                                        Toast.makeText(getContext(), "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                                                                        isOrdering = false;

                                                                        LoginUtil.getInstance(getContext()).setmOrderSTBL_CD(null);
                                                                        LoginUtil.getInstance(getContext()).setmOrderID(null);
                                                                    }
                                                                });

                                                    } else {
                                                        Toast.makeText(getContext(), "주문중입니다.", Toast.LENGTH_SHORT).show();
                                                    }
                                                })
                                                .complete();
                                    } else {
                                        Toast.makeText(getContext(), "주문할 메뉴를 선택해주세요.", Toast.LENGTH_SHORT).show();
                                        isOrdering = false;
                                    }


                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Toast.makeText(getContext(), "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                                    isOrdering = false;
                                    LoginUtil.getInstance(getContext()).setmOrderSTBL_CD(null);
                                    LoginUtil.getInstance(getContext()).setmOrderID(null);
                                }
                            });
                } else {
                    Toast.makeText(getContext(), "주문중입니다.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), "pos연동안됨. 와이파이를 확인해주세요.", Toast.LENGTH_SHORT).show();
            }

        }
    }


    private void setError(String error) {
        FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(getContext()).getmBrandName())
                .child("error")
                .child(Utils.getDate("yyyy-MM-dd"))
                .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                .child(LoginUtil.getInstance(getContext()).getmSelectedTableNo())
                .child(error)
                .push()
                .setValue(Utils.getDate("yyyy-MM-dd HH:mm:ss"));
    }


    private void setCartTotalPrice() {
        totalPrice_textview = mCartListAdapter.getTotalPrice();
        this.binding.setTotalPrice(totalPrice_textview);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void showNotification(String tableNo, String regTime) {
        String str = regTime + " 테이블번호[" + tableNo + "]에서 주문한 음식이 POS에 등록되지않았습니다. 관리자페이지를 확인하시고 POS에 입력해주세요.";

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("주문 수기입력 알림")
                .setContentText(str)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setSound(defaultSoundUri)
                .setDefaults(android.app.Notification.DEFAULT_ALL)
                .setPriority(android.app.Notification.PRIORITY_MAX); //** MAX 나 HIGH로 줘야 가능함

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle(notificationBuilder);
        inboxStyle.addLine(regTime + " 테이블번호[" + tableNo + "]에서");
        inboxStyle.addLine("주문한 음식이 POS에 등록되지않았습니다.");
        inboxStyle.addLine("관리자페이지를 확인하시고 POS에 입력해주세요.");
        notificationBuilder.setStyle(inboxStyle);


        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}
