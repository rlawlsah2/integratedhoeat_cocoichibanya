package com.cdef.orderappformanagermobilecocoichibanya.View.Fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.orderappformanagermobilecocoichibanya.Dialog.LoadingDialog.LoadingDialog;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.orderappformanagermobilecocoichibanya.View.Adapter.BillListAdapter;
import com.cdef.orderappformanagermobilecocoichibanya.View.ViewHolder.BillListViewHolder;
import com.cdef.orderappformanagermobilecocoichibanya.ViewModel.CartViewModel;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.FragmentBillBinding;
import com.google.firebase.database.FirebaseDatabase;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;


/**
 * Created by kimjinmo on 2018. 4. 18
 * <p>
 * 주문 내역 확인하는 화면
 */

public class BillFragment extends BaseFragment {

    FragmentBillBinding binding;
    BillListAdapter mBillListAdapter = null;
    CartViewModel cartViewModel;
    private int totalPrice_textview = 0;



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_loading));
        this.cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);//new CartViewModel();
        this.cartViewModel.initSetting.observe(this, initsetting -> {

            switch (initsetting) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();

                    break;
                case ERROR:
                    this.loadingDialog.dismiss();

                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    break;
            }

        });

        ///주문내역을 가져올 수 있는 orderID부터 조회하자
        cartViewModel.mOrderId.observe(this, orderId -> {

            if (orderId != null) { ///cart 리스트 처리
                ///cart 리스트 처리
                mBillListAdapter = new BillListAdapter(Order.class, R.layout.adapter_bill_list, BillListViewHolder.class,
                        FirebaseDatabase.getInstance().getReference()
                                .child(LoginUtil.getInstance(getContext()).getmBrandName())
                                .child("orderData")
                                .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                                .child(orderId)
                                .child("orders")
                                .orderByChild("time")
                );
                mBillListAdapter.registerAdapterDataObserver(adapterDataObserver);

                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(mBillListAdapter);
                alphaAdapter.setInterpolator(new OvershootInterpolator());
                this.binding.recyclerView.setAdapter(alphaAdapter);

            } else {
                Toast.makeText(getContext(), "테이블 주문정보를 가져올 수 없습니다.", Toast.LENGTH_SHORT).show();
            }
        });

        this.binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        cartViewModel.getOrderId(
                (LoginUtil.getInstance(getContext()).branchInfo.branchId)
                , (LoginUtil.getInstance(getContext()).getmSelectedTableNo())
        );


    }

    public void close(View view) {
        getActivity().onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // TODO: inflate a fragment view
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_bill, container, false);
        this.binding.setFragment(this);
        return binding.getRoot();
    }

    private RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            setCartTotalPrice();
            if (mBillListAdapter != null)
                binding.setItemCount(mBillListAdapter.getItemCount());

        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            setCartTotalPrice();
            if (mBillListAdapter != null)
                binding.setItemCount(mBillListAdapter.getItemCount());
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            setCartTotalPrice();
            if (mBillListAdapter != null)
                binding.setItemCount(mBillListAdapter.getItemCount());
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            setCartTotalPrice();
            if (mBillListAdapter != null)
                binding.setItemCount(mBillListAdapter.getItemCount());
        }
    };


    private void setCartTotalPrice() {
        totalPrice_textview = mBillListAdapter.getTotalPrice();
        this.binding.setTotalPrice(totalPrice_textview);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
