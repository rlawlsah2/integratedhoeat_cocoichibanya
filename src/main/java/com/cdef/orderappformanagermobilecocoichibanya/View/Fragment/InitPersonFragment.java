package com.cdef.orderappformanagermobilecocoichibanya.View.Fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.cdef.orderappformanagermobilecocoichibanya.Dialog.LoadingDialog.LoadingDialog;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.commonmodule.Utils.FBPathBuilder;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.NavigationUtils;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.FragmentInitPersonBinding;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class InitPersonFragment extends BaseFragment {

    FragmentInitPersonBinding binding = null;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_loading));

    }

    private void calPersonCount()
    {

        int domestic = 0;
        int foreign = 0;


        try {
            domestic = Integer.parseInt(binding.editPersonCount.getText().toString());
        } catch (Exception e) {
        }
        try {
            foreign = Integer.parseInt(binding.editPersonCountForeign.getText().toString());
        } catch (Exception e) {
        }

        binding.setPersonCount((domestic + foreign) + "");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        this.binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_init_person, container, false);
        this.binding.setFragment(this);
        this.binding.setPersonCount("");
        this.binding.num.setOnTextChangeListner((String text, int digits_remaining) -> {
            if (this.isLayoutTableNo)
            {
                this.binding.editPersonCount.setText(text);
            }
            else
            {
                this.binding.editPersonCountForeign.setText(text);
            }
            calPersonCount();
        });
        this.binding.editPersonCount.setOnFocusChangeListener((view, b) -> {
            this.binding.setFocusTableNo(b);
        });

        this.binding.editPersonCountForeign.setOnFocusChangeListener((view, b) -> {
            this.binding.setFocusTableNoForeign(b);
        });
        this.binding.editPersonCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                int domestic = 0;
                int foreign = 0;


                try {
                    domestic = Integer.parseInt(binding.editPersonCount.getText().toString());
                } catch (Exception e) {
                }
                try {
                    foreign = Integer.parseInt(binding.editPersonCountForeign.getText().toString());
                } catch (Exception e) {
                }

                binding.setPersonCount((domestic + foreign) + "");
            }
        });
        this.binding.editPersonCountForeign.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int domestic = 0;
                int foreign = 0;


                try {
                    domestic = Integer.parseInt(binding.editPersonCount.getText().toString());
                } catch (Exception e) {
                }
                try {
                    foreign = Integer.parseInt(binding.editPersonCountForeign.getText().toString());
                } catch (Exception e) {
                }

                binding.setPersonCount((domestic + foreign) + "");
            }
        });


        ////테이블 번호에서 완료 누르면 바로 진행되게끔
        this.binding.editPersonCountForeign.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        this.binding.buttonGoOrder.performClick();
                        return true;
                    default:
                        break;
                }
            }
            return false;
        });
        String tableNo = getArguments().getString("tableNo");

        if (tableNo == null)
            getActivity().onBackPressed();
        else {
            this.binding.setSelectedTableNo(tableNo);
        }
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    /**
     * 바탕화면 눌렀을때 키 입력창 사라지게
     **/
    public void clickOUT() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) (getActivity().getSystemService(Context.INPUT_METHOD_SERVICE));
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void goMenuSelect(View view) {

        clickOUT();

        long member = 0;

        int domestic = 0;
        int foreign = 0;


        try {
            domestic = Integer.parseInt(binding.editPersonCount.getText().toString());
        } catch (Exception e) {
        }
        try {
            foreign = Integer.parseInt(binding.editPersonCountForeign.getText().toString());
        } catch (Exception e) {
        }

//        try {
//            member = Long.parseLong(this.binding.editPersonCount.getText().toString());
//        } catch (Exception e) {
//
//        }
        member = domestic + foreign;
        final long fMember = member;

        Map<String, Object> map = new HashMap<>();

        map.put(FBPathBuilder.getInstance().init()
                .set(LoginUtil.getInstance(getContext()).getmBrandName())
                .set("tables")
                .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                .set(this.binding.getSelectedTableNo())
                .set("members")
                .set("men")
                .complete(), domestic);
        map.put(FBPathBuilder.getInstance().init()
                .set(LoginUtil.getInstance(getContext()).getmBrandName())
                .set("tables")
                .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                .set(this.binding.getSelectedTableNo())
                .set("members")
                .set("women")
                .complete(), foreign);

        FirebaseDatabase.getInstance().getReference().updateChildren(map, (databaseError, databaseReference) -> {
            if (databaseError == null) {
                Bundle bundle = new Bundle();
                bundle.putString("tableNo", this.binding.getSelectedTableNo());
                bundle.putLong("member", fMember);
                NavigationUtils.templateOpenFragment(getFragmentManager(), R.id.layoutFragment, SelectMenuFragment.class.getSimpleName(), bundle);
            } else {
                Toast.makeText(getContext(), "잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    Boolean isLayoutTableNo = true;

    public void clickInput(View view) {
        this.isLayoutTableNo = view.equals(this.binding.layoutTableNo);
        if (this.isLayoutTableNo) {
            this.binding.num.setDigits(this.binding.editPersonCount.getText().toString());
            Toast.makeText(getContext(), "내국인 입력 모드", Toast.LENGTH_SHORT).show();
        } else {
            this.binding.num.setDigits(this.binding.editPersonCountForeign.getText().toString());
            Toast.makeText(getContext(), "외국인 입력 모드", Toast.LENGTH_SHORT).show();


        }
    }

    public void goBack(View view) {
        getActivity().onBackPressed();
    }
}
