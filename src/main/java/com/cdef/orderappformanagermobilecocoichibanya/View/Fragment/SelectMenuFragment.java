package com.cdef.orderappformanagermobilecocoichibanya.View.Fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.dataModel.MenuSideGroupItemData;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.commonmodule.dataModel.OrderCategoryData;
import com.cdef.commonmodule.Utils.TabletSetting;
import com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable.MenuCategory;
import com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable.MenuCategorySub;
import com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable.MenuOption;
import com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable.MenuOptionSub;
import com.cdef.orderappformanagermobilecocoichibanya.Dialog.LoadingDialog.LoadingDialog;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.commonmodule.Utils.FBPathBuilder;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.NavigationUtils;
import com.cdef.commonmodule.Utils.Utils;
import com.cdef.orderappformanagermobilecocoichibanya.View.Adapter.MenuListAdapter;
import com.cdef.orderappformanagermobilecocoichibanya.View.CustomView.NoAnimationItemAnimator;
import com.cdef.orderappformanagermobilecocoichibanya.View.Expandable.MenuCategoryItem;
import com.cdef.orderappformanagermobilecocoichibanya.View.Expandable.MenuCategorySubItem;
import com.cdef.orderappformanagermobilecocoichibanya.View.Expandable.MenuOptionItem;
import com.cdef.orderappformanagermobilecocoichibanya.View.Expandable.MenuOptionSubItem;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.FragmentMenuSelectBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.zaihuishou.expandablerecycleradapter.viewholder.AbstractAdapterItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by kimjinmo on 2018. 4.23
 * 메뉴 선택에 쓰이는 부분
 */

public class SelectMenuFragment extends BaseFragment {

    //    public String mSelecteTableNo;
    private final int ITEM_TYPE_MENU = 1;
    private final int ITEM_TYPE_MENU_OPTION = 2;
    private FragmentMenuSelectBinding binding = null;
    private MenuListAdapter mMenuListAdapter, mMenuOptionListAdapter;
    private List<Object> mMenuList;
    private List<Object> mMenuOptionList;
    private MenuItemData mSelectedMenuItemData;
    private HashMap<String, HashMap<String, MenuOptionSub>> mSelectedOptionMap = new HashMap<>();
    private String editCartKey;
    private Order editCartOrder;


    ///옵션 선택에 대해 처리하기 위한 데이터 구조와 메소드

    /**
     * key : 그룹명
     **/
    private void addSelectedOption(String key, MenuOptionSub menuOptionSub) {
        LogUtil.d("선택 옵션 key: " + key);
        if (!mSelectedOptionMap.containsKey(key) || mSelectedOptionMap.get(key) == null) {
            ///데이터가 제대로 없는 케이스
            this.mSelectedOptionMap.put(key, new HashMap<>());
        }
        this.mSelectedOptionMap.get(key).put(menuOptionSub.mMenuSideOptionItemData.CMDTCD, menuOptionSub);
    }


    private MenuOptionSub getSelectedOption(String key, MenuOptionSub menuOptionSub) {
        if (!mSelectedOptionMap.containsKey(key) || mSelectedOptionMap.get(key) == null) {
            ///데이터가 제대로 없는 케이스
            this.mSelectedOptionMap.put(key, new HashMap<>());
            return null;
        }
        return this.mSelectedOptionMap.get(key).get(menuOptionSub.mMenuSideOptionItemData.CMDTCD);
    }

    private void clearSelectedOption(String key) {
        if (this.mSelectedOptionMap != null)
            this.mSelectedOptionMap.remove(key);
    }

    private MenuOptionSub deleteSelectedOption(String key, MenuOptionSub menuOptionSubn) {
        if (!mSelectedOptionMap.containsKey(key) || mSelectedOptionMap.get(key) == null) {
            ///데이터가 제대로 없는 케이스
            this.mSelectedOptionMap.put(key, new HashMap<>());
            return null;
        }
        LogUtil.d("선택한거 빼기 : " + key);
        LogUtil.d("선택한거 빼기 : " + menuOptionSubn.mMenuSideOptionItemData.CMDTCD);
        return this.mSelectedOptionMap.get(key).remove(menuOptionSubn.mMenuSideOptionItemData.CMDTCD);
    }

    private int getSelectedOptionCounts(String key) {
        try {
            return this.mSelectedOptionMap.get(key).size();
        } catch (Exception e) {
            return 0;
        }
    }

    private void clearSelectedOption() {
        this.mSelectedOptionMap.clear();
        LogUtil.d("옵션 선택 비우기 : " + this.mSelectedOptionMap.size());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_loading));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        this.binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_menu_select, container, false);
        this.binding.setFragment(this);
        this.binding.setCartListCount(0);
        this.binding.setSelectedMenuQuantity(1);

        this.binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        this.binding.recyclerViewOption.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        layoutManager.postOnAnimation(null);
        layoutManager.setItemPrefetchEnabled(true);
        this.binding.recyclerViewOption.setLayoutManager(layoutManager);
        this.binding.recyclerViewOption.setItemAnimator(new NoAnimationItemAnimator());
        initMenuList();
        mMenuListAdapter = new MenuListAdapter(mMenuList) {
            @NonNull
            @Override
            public AbstractAdapterItem<Object> getItemView(Object type) {
                int itemType = (int) type;
                switch (itemType) {
                    case ITEM_TYPE_MENU:
                        return new MenuCategoryItem((view, item) -> {
                            mMenuListAdapter.collapseAllParentsWithoutSelected(item);

                        });
                    case ITEM_TYPE_MENU_OPTION:
                        return new MenuCategorySubItem((view, item) -> {

//                            if (item.mMenuItem.isSet == 0) {
//                                //단품 메뉴
//                                Toast.makeText(getContext(), "선택한 단품 메뉴 : " + item.mMenuItem.foodName, Toast.LENGTH_SHORT).show();
//
//                            } else {
//                            Toast.makeText(getContext(), "선택한 메뉴 : " + item.mMenuItem.foodName, Toast.LENGTH_SHORT).show();
                            clearSelectedOption();
                            mSelectedMenuItemData = item.mMenuItem;
                            if (item.mMenuItem.isSet > 0)    //0보다 크면 세트메뉴임.
                            {
                                binding.setSelectedMenuIsSet(item.mMenuItem.isSet);
                                initOptionList(item.mMenuItem);
                                binding.setSelectedMenuQuantity(1);
                                binding.layoutOption.setVisibility(View.VISIBLE);   //옵션화면을 연다.
                            } else {
                                addCartSingleItem();
                            }
                            binding.recyclerViewOption.scrollToPosition(0);

//                            }

                        });
                }
                return null;
            }

            @Override
            public Object getItemViewType(Object t) {
                if (t instanceof MenuCategory) {
                    return ITEM_TYPE_MENU;
                } else if (t instanceof MenuCategorySub)
                    return ITEM_TYPE_MENU_OPTION;
                return -1;
            }
        };
        mMenuOptionListAdapter = new MenuListAdapter(mMenuOptionList) {
            @NonNull
            @Override
            public AbstractAdapterItem<Object> getItemView(Object type) {
                int itemType = (int) type;
                switch (itemType) {
                    case ITEM_TYPE_MENU:
                        return new MenuOptionItem(menuOptionItem -> {
                            mMenuOptionListAdapter.collapseAllParentsWithoutSelected(menuOptionItem);
                            try {
                                binding.setSelectedOption(((MenuOption) menuOptionItem).mMenuSideGroupItemData.groupName);
                            } catch (Exception e) {

                            }
                        });
                    case ITEM_TYPE_MENU_OPTION:
                        return new MenuOptionSubItem((view, item, menuOptionSubItem) -> {
                            String groupName = item.groupName;
                            for (int i = 0; i < mMenuOptionListAdapter.getItemCount(); i++) {
                                if (mMenuOptionListAdapter.getDataList().get(i) instanceof MenuOptionSub) {
                                    if (((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).groupName.equals(groupName)) {
                                        MenuOptionSub tmp = getSelectedOption(groupName, ((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)));

                                        if (tmp == null)
                                            tmp = ((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i));
                                        ////해당 옵션이 1개만 선택할 수 있는애라면 전부다 지우고 아래에서 선택화면으로 바꺼야 한다.
//                                        if(tmp.maxGroupItem == 1 && i != menuOptionSubItem.getItemIndex())  ///1개일 경우
//                                        {
//                                            tmp.quantity = 0;
//                                            tmp.mSelected = false;
//                                            deleteSelectedOption(groupName, i);
//                                        }

                                        ///
                                        LogUtil.d("mMenuOptionListAdapter 의 item index : " + i);
                                        LogUtil.d("mMenuOptionListAdapter 의 item getItemIndex : " + menuOptionSubItem.getItemIndex());
                                        if (i == menuOptionSubItem.getItemIndex()) {   ///내가 선택한 아이템
                                            LogUtil.d("mMenuOptionListAdapter 의 item CMDTCD : " + tmp.mMenuSideOptionItemData.CMDTCD);
                                            LogUtil.d("mMenuOptionListAdapter 의 item maxGroupItem : " + tmp.maxGroupItem);
                                            LogUtil.d("mMenuOptionListAdapter 의 item quantity : " + tmp.quantity);

                                            tmp.mSelected = !tmp.mSelected;
                                            if (tmp.mSelected) {
                                                if (tmp.maxGroupItem == 0 || tmp.maxGroupItem == 1 || (tmp.maxGroupItem > getSelectedOptionCounts(groupName))) {
                                                    tmp.quantity = 1;
                                                    addSelectedOption(groupName, tmp);
//                                                    addSelectedOption(groupName, i);
                                                } else {
                                                    tmp.mSelected = !tmp.mSelected;
                                                    Toast.makeText(getContext(), "해당 옵션은 " + tmp.maxGroupItem + "개 이상 선택할 수 없습니다. 다른 옵션을 해제후 선택해주세요.", Toast.LENGTH_SHORT).show();
                                                    break;
                                                }
                                            } else {
                                                tmp.quantity = 0;
                                                deleteSelectedOption(groupName, tmp);

//                                                deleteSelectedOption(groupName, i);
                                            }

                                        } else {
                                            if (!tmp.mIsAbleToMultiCheck)   //멀티체크가 불가능할때는 다른걸 다 false 처리
                                            {
                                                tmp.mSelected = false;
                                                tmp.quantity = 0;
                                                deleteSelectedOption(groupName, tmp);
                                            }
                                        }
                                        mMenuOptionListAdapter.modifyItem(i, tmp);
                                        LogUtil.d("mMenuOptionListAdapter 의 item 수정 후 : " + ((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).quantity);
                                    }
                                }
                            }

                            binding.optionSelectedOptions.setText(Html.fromHtml("선택: " + getSelectedOptionList())); //선택한 메뉴 옵션들 나열 지정
                            getSelectedOptionsPrice();
                        }, (view, item, menuOptionSubItem, isPlus) -> {
                            if (isPlus) {
                                if (item.quantity < item.mMenuSideOptionItemData.maxQuantity) {
                                    item.quantity += 1;
                                    item.mSelected = true;
                                } else {
                                    Toast.makeText(getContext(), "해당 옵션은 " + item.mMenuSideOptionItemData.maxQuantity + "개를 초과할 수 없습니다", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                if (item.quantity > 0) {
                                    item.quantity -= 1;
                                    if (item.quantity == 0)
                                        item.mSelected = false;
                                }

                            }
                            LogUtil.d("수량 조절 시작 quantity 후  : " + item.quantity);

                            if (item.quantity == 0)
                                deleteSelectedOption(item.groupName, item);
                            else
                                addSelectedOption(item.groupName, item);

                            mMenuOptionListAdapter.modifyItem(menuOptionSubItem.getItemIndex(), item);
                            LogUtil.d("수량 조절 시작 quantity 업데이트 후  : " + ((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(menuOptionSubItem.getItemIndex())).quantity);

                            binding.optionSelectedOptions.setText(Html.fromHtml("선택: " + getSelectedOptionList())); //선택한 메뉴 옵션들 나열 지정
                            getSelectedOptionsPrice();


                        });
                }
                return null;
            }

            @Override
            public Object getItemViewType(Object t) {
                if (t instanceof MenuOption) {
                    return ITEM_TYPE_MENU;
                } else if (t instanceof MenuOptionSub)
                    return ITEM_TYPE_MENU_OPTION;
                return -1;
            }
        };

        this.binding.recyclerView.setAdapter(mMenuListAdapter);
        this.binding.recyclerView.setItemAnimator(null);
        this.binding.recyclerViewOption.setAdapter(mMenuOptionListAdapter);
        this.binding.recyclerViewOption.setItemAnimator(null);


        editCartItemViewSetting();
        return binding.getRoot();
    }


    private void setCartListCount() {
        FirebaseDatabase.getInstance().getReference()
                .child(LoginUtil.getInstance(getContext()).getmBrandName())
                .child("cart")
                .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                .child(this.binding.getSelectedTableNo())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot == null) {
                            binding.setCartListCount(0);
                        } else {
                            binding.setCartListCount((int) dataSnapshot.getChildrenCount());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();

        Bundle bundle = getArguments();
        if (bundle != null) {
            ///
//            mSelecteTableNo = bundle.getString("tableNo");
            this.binding.setSelectedTableNo(bundle.getString("tableNo"));
            LoginUtil.getInstance(getContext()).setmSelectedTableNo(this.binding.getSelectedTableNo());
            Long memberCount = bundle.getLong("member");
            LogUtil.d("onResume : " + memberCount);
            this.binding.setPersonCount(memberCount + "");
            setCartListCount();

        }


    }

    ///각 메뉴 리스트를 생성
    private void initMenuList() {
        mMenuList = new ArrayList<>();
        mMenuOptionList = new ArrayList();

        for (OrderCategoryData.CategoryItem categoryItem : TabletSetting.getInstance().getmOrderSubCategoryAllList()) {
            if (TabletSetting.getInstance().getmAllMenusHashMap().get(categoryItem.categoryCode) != null)
                mMenuList.add(createMenu(categoryItem, false));
        }
    }


    ///선택된 메뉴의 옵션 뷰를 초기화한다.
    ///먼저 hashmap에서 데이터를 찾고
    ///있으면 그 목록을 불러와 뿌려주고
    ///없으면 그 목록을 생성해서 넣어주고나서 뿌려준다.
    private void initOptionList(MenuItemData selectedMenuItem) {

        this.binding.optionSelectedMainMenu.setText(Utils.removeFirstSpace(selectedMenuItem.foodName)); //선택한 메뉴 이름 지정
        this.binding.setSelectedMenuPrice(selectedMenuItem.price);


        LogUtil.d("옵션 리스트 정리 : " + selectedMenuItem.sideGroup);
        LogUtil.d("옵션 리스트 정리 : " + selectedMenuItem.sideGroup.size());
        mMenuOptionList.clear();
        for (MenuSideGroupItemData groupItemData : selectedMenuItem.sideGroup) {
//            mMenuOptionList.add(createOption(groupItemData, true, true));
            mMenuOptionList.add(createOption(groupItemData, false, groupItemData.maxGroupItem != 1));
        }

        mMenuOptionListAdapter.updateData(mMenuOptionList);

        ///다 만들었으니 디폴트 항목만 체크로 바꾸자
        for (int i = 0; i < mMenuOptionListAdapter.getDataList().size(); i++) {
            if (mMenuOptionListAdapter.getDataList().get(i) instanceof MenuOptionSub) {
                LogUtil.d(i + "번째 옵션 초기화중 defaultItemprice: " + ((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).mMenuSideOptionItemData.defaultItem);

                if (((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).mMenuSideOptionItemData.defaultItem == 1 && editCartKey == null) {
                    MenuOptionSub tmp = ((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i));
                    tmp.mSelected = true;
                    tmp.quantity = 1;
                    LogUtil.d(i + "번째 옵션 초기화중 CMDTCD: " + tmp.mMenuSideOptionItemData.CMDTCD);
                    addSelectedOption(tmp.groupName, tmp);

//                    addSelectedOption(tmp.groupName, i);
                    mMenuOptionListAdapter.modifyItem(i, tmp);


                }
            }
        }


        binding.optionSelectedOptions.setText(Html.fromHtml("선택: " + getSelectedOptionList())); //선택한 메뉴 옵션들 나열 지정
        this.getSelectedOptionsPrice();


    }

    ///메뉴 목록의 아이템 생성
    @NonNull
    private MenuCategory createMenu(OrderCategoryData.CategoryItem categoryItem, boolean isExpandDefault) {
        MenuCategory menuCategory = new MenuCategory();
        menuCategory.mCategoryItem = categoryItem;

        List<MenuCategorySub> menuCategorySubList = new ArrayList<>();

        LogUtil.d("메뉴 아이템 화면 생성1 : " + TabletSetting.getInstance().getmAllMenusHashMap().size());
        LogUtil.d("메뉴 아이템 화면 생성2 : " + categoryItem.categoryCode);
        LogUtil.d("메뉴 아이템 화면 생성3 : " + TabletSetting.getInstance().getmAllMenusHashMap().get(categoryItem.categoryCode));
        LogUtil.d("메뉴 아이템 화면 생성4 : " + TabletSetting.getInstance().getmAllMenusHashMap().get(categoryItem.categoryCode).size());

        for (MenuItemData item : TabletSetting.getInstance().getmAllMenusHashMap().get(categoryItem.categoryCode)) {
            MenuCategorySub sub = new MenuCategorySub();
            sub.mMenuItem = item;
            menuCategorySubList.add(sub);
        }

        menuCategory.mSubList.addAll(menuCategorySubList);
        menuCategory.mExpanded = isExpandDefault;
        return menuCategory;
    }

    ///메뉴 옵션 목록의 아이템 생성
    private MenuOption createOption(MenuSideGroupItemData menuSideGroupItemData, boolean isExpandDefault, boolean isAbleToMultiCheck) {
        MenuOption menuOption = new MenuOption();
        menuOption.mMenuSideGroupItemData = menuSideGroupItemData;

        List<MenuOptionSub> menuOptionSubList = new ArrayList<>();
        for (MenuSideOptionItemData item : menuSideGroupItemData.sideOptions) {
            MenuOptionSub sub = new MenuOptionSub();
            sub.mMenuSideOptionItemData = item;
            sub.mIsAbleToMultiCheck = isAbleToMultiCheck;
            sub.groupName = menuSideGroupItemData.groupName;
            sub.maxGroupItem = menuSideGroupItemData.maxGroupItem;

            menuOptionSubList.add(sub);
//            (((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).mMenuSideOptionItemData.defaultItem == 1)
            if (sub.mMenuSideOptionItemData.defaultItem == 1 && editCartKey == null) {
                sub.mSelected = true;
                sub.quantity = 1;
                ///초기화 할때 기본값들을 넣어줘야한다
                addSelectedOption(sub.groupName, sub);
            }
        }

        menuOption.mSubList.addAll(menuOptionSubList);
        menuOption.mExpanded = isExpandDefault;
        return menuOption;
    }

    private String getSelectedOptionList() {
        StringBuilder sb = new StringBuilder();


        ///밥 맵기는 뒤로
        ArrayList<MenuOptionSub> etc = new ArrayList<>();

        for (String key : this.mSelectedOptionMap.keySet()) {
            for (String subKey : this.mSelectedOptionMap.get(key).keySet()) {

                if (key.trim().equals("밥") || key.trim().equals("맵기")) {
                    etc.add(this.mSelectedOptionMap.get(key).get(subKey));
                } else {
                    sb.append("<b><u><font color='#000000'>");
                    sb.append(this.mSelectedOptionMap.get(key).get(subKey).mMenuSideOptionItemData.foodName);
                    sb.append("(");
                    sb.append((this.mSelectedOptionMap.get(key).get(subKey)).quantity);
                    sb.append("ea)");
                    sb.append("</font></u></b>");
                    sb.append(", ");
                }
            }
        }


        //밥 맵기는 뒤로 뺀다
        if (etc != null && etc.size() > 0) {
            for (MenuOptionSub item : etc) {
                sb.append(item.mMenuSideOptionItemData.foodName);
                sb.append("(");
                sb.append((item).quantity);
                sb.append("ea)");
                sb.append(", ");
            }
        }

        try {
            int lastIndex = sb.toString().lastIndexOf(", ");
            return sb.toString().substring(0, lastIndex);
        } catch (Exception e) {
            return sb.toString();
        }
    }

    private void getSelectedOptionsPrice() {
        int total = this.binding.getSelectedMenuPrice();

        for (String key : this.mSelectedOptionMap.keySet()) {
            for (String subKey : this.mSelectedOptionMap.get(key).keySet()) {
                total += (this.mSelectedOptionMap.get(key).get(subKey).mMenuSideOptionItemData.price *
                        this.mSelectedOptionMap.get(key).get(subKey).quantity);
            }
        }
//        for (int i = 0; i < mMenuOptionListAdapter.getItemCount(); i++) {
//
//
//            if (mMenuOptionListAdapter.getDataList().get(i) instanceof MenuOptionSub) {
//                if (((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).mSelected) //눌린것만
//                {
//
//
//                    total += (((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).mMenuSideOptionItemData.price *
//                            ((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).quantity);
//                }
//            }
//        }

        binding.optionSelectedTotalPrice.setText(Utils.setComma(total * this.binding.getSelectedMenuQuantity()) + "원"); //선택한 메뉴 옵션들 나열 지정
    }


    private void printSelectedOptionsPrice() {
        int total = this.binding.getSelectedMenuPrice();
        for (int i = 0; i < mMenuOptionListAdapter.getItemCount(); i++) {
            if (mMenuOptionListAdapter.getDataList().get(i) instanceof MenuOptionSub) {
                total += (((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).mMenuSideOptionItemData.price *
                        ((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).quantity);
            }
        }

        binding.optionSelectedTotalPrice.setText(Utils.setComma(total * this.binding.getSelectedMenuQuantity()) + "원"); //선택한 메뉴 옵션들 나열 지정
    }

    public void goChangePersonCounts(View view) {

        NavigationUtils.removeAllFragment(getFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("tableNo", this.binding.getSelectedTableNo());
        NavigationUtils.templateOpenFragment(getFragmentManager(), R.id.layoutFragment, InitPersonFragment.class.getSimpleName(), bundle);
    }

    public void closeOption(View view) {
        binding.layoutOption.setVisibility(View.GONE);
    }

    public void goCart(View view) {
        LoginUtil.getInstance(getContext()).setmSelectedTableNo(this.binding.getSelectedTableNo());
        NavigationUtils.templateOpenFragment(getFragmentManager(), R.id.layoutFragment, CartListFragment.class.getSimpleName(), null);
    }

    public void goBill(View view) {
        LoginUtil.getInstance(getContext()).setmSelectedTableNo(this.binding.getSelectedTableNo());
        NavigationUtils.templateOpenFragment(getFragmentManager(), R.id.layoutFragment, BillFragment.class.getSimpleName(), null);
    }


    public void changeQuantity(View view) {
        if (view.equals(this.binding.layoutButtonMinus)) {
            if (this.binding.getSelectedMenuQuantity() > 1)
                this.binding.setSelectedMenuQuantity(this.binding.getSelectedMenuQuantity() - 1);
        } else {
            this.binding.setSelectedMenuQuantity(this.binding.getSelectedMenuQuantity() + 1);
        }
        getSelectedOptionsPrice();
    }

    /**
     * 단품 전용. 단, 수정하는 케이스는 여기가 아님.
     **/
    public void addCartSingleItem() {

        if (checkSelectOption()) {

            loadingDialog.show();

            Map<String, Object> orderMain = new HashMap<>();

            Order itemData = new Order();
            itemData.setData(mSelectedMenuItemData, 1);
            itemData.key = FirebaseDatabase.getInstance().getReference().push().getKey();
            itemData.orderedTableNo = binding.getSelectedTableNo();
            itemData.state = 1;
            orderMain.put(FBPathBuilder.getInstance().init()
                    .set(LoginUtil.getInstance(getContext()).getmBrandName())
                    .set("cart")
                    .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                    .set(binding.getSelectedTableNo())
                    .set(itemData.key)
                    .complete(), itemData);


            FirebaseDatabase.getInstance().getReference()
                    .updateChildren(orderMain, (databaseError, databaseReference) -> {
                        if (databaseError == null) {
                            LogUtil.d("장바구니 성공");
                            editCartKey = null;
                            editCartOrder = null;
                            Toast.makeText(getContext(), mSelectedMenuItemData.foodName + " 1개 추가되었습니다.", Toast.LENGTH_SHORT).show();
                            closeOption(null);
                            setCartListCount();
                        } else {
                            LogUtil.d("장바구니 실패");
                            Toast.makeText(getContext(), "잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();

                        }
                        loadingDialog.dismiss();
                    });

        } else {
            Toast.makeText(getContext(), "필수 항목을 선택해주세요", Toast.LENGTH_SHORT).show();
        }
//        printSelectedOptionsPrice();
    }


    /**
     * 셋트 품목을 넣을 경우
     **/
    public void addCart(View view) {

        if (checkSelectOption()) {

            loadingDialog.show();

            Map<String, Object> insertMap = new HashMap<>();
            Map<String, Object> orderMain = new HashMap<>();
            HashMap<String, Order> options = new HashMap<>();

            ///1. options 빌드
            ///옵션 선택 리스트 훑기
            LogUtil.d("선택 옵션 장바구니 담기 mSelectedOptionMap : " + mSelectedOptionMap.size());
            for (String key : this.mSelectedOptionMap.keySet()) {
                LogUtil.d("선택 옵션 장바구니 담기 key : " + key);
                for (String subKey : this.mSelectedOptionMap.get(key).keySet()) {
                    Order itemData = new Order();
                    String fbKey = FirebaseDatabase.getInstance().getReference().push().getKey();
                    itemData.setData(this.mSelectedOptionMap.get(key).get(subKey).mMenuSideOptionItemData, this.mSelectedOptionMap.get(key).get(subKey).quantity);
                    itemData.key = fbKey;
                    itemData.categoryName = key;
                    options.put(fbKey, itemData);
                }

            }
            //orderMain 빌드
            ///1. option이 존재하는 경우
            if (options != null && options.size() > 0) {
                Order itemData = new Order();
                int calcPrice = 0;
                itemData.setData(mSelectedMenuItemData, this.binding.getSelectedMenuQuantity());
                if (itemData.price == 0) {
                    for (String key : options.keySet()) {
                        calcPrice += (options.get(key).price * options.get(key).quantity);
                    }
                    itemData.price = calcPrice;
                }

                itemData.key = editCartKey != null ? editCartKey : FirebaseDatabase.getInstance().getReference().push().getKey();
                itemData.orderedTableNo = binding.getSelectedTableNo();
                itemData.state = 1;
                itemData.options = options;
                orderMain.put(FBPathBuilder.getInstance().init()
                        .set(LoginUtil.getInstance(getContext()).getmBrandName())
                        .set("cart")
                        .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                        .set(binding.getSelectedTableNo())


                        .set(editCartKey != null ? editCartKey : itemData.key)
                        .complete(), itemData);
            } else    ///option 이 없는 단품들
            {
                Order itemData = new Order();
                itemData.setData(mSelectedMenuItemData, this.binding.getSelectedMenuQuantity());
                itemData.key = editCartKey != null ? editCartKey : FirebaseDatabase.getInstance().getReference().push().getKey();
                itemData.orderedTableNo = binding.getSelectedTableNo();
                itemData.state = 1;
                orderMain.put(FBPathBuilder.getInstance().init()
                        .set(LoginUtil.getInstance(getContext()).getmBrandName())
                        .set("cart")
                        .set(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                        .set(binding.getSelectedTableNo())
                        .set(editCartKey != null ? editCartKey : itemData.key)
                        .complete(), itemData);
            }


            FirebaseDatabase.getInstance().getReference()
                    .updateChildren(orderMain, (databaseError, databaseReference) -> {
                        if (databaseError == null) {
                            LogUtil.d("장바구니 성공");
                            this.editCartKey = null;
                            this.editCartOrder = null;
                            Toast.makeText(getContext(), "장바구니에 담았습니다", Toast.LENGTH_SHORT).show();
                            closeOption(null);
                            setCartListCount();
                        } else {
                            LogUtil.d("장바구니 실패");
                            Toast.makeText(getContext(), "잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();

                        }
                        loadingDialog.dismiss();
                    });

        } else {
            Toast.makeText(getContext(), "필수 항목을 선택해주세요", Toast.LENGTH_SHORT).show();
        }
//        printSelectedOptionsPrice();
    }

    HashMap<String, Boolean> mSelectedMenuItemOptionIsNecessary = new HashMap<>();
    HashSet<String> mSelectedMenuItemOptionIsNecessaryCheck = new HashSet<>();

    public boolean isShowOption() {
        return (this.binding.layoutOption.getVisibility() == View.VISIBLE);
    }


    /**
     * 옵션 선택현황조사
     **/
    private boolean checkSelectOption() {
        mSelectedMenuItemOptionIsNecessary.clear();
        mSelectedMenuItemOptionIsNecessaryCheck.clear();


        for (MenuSideGroupItemData groupItemData : mSelectedMenuItemData.sideGroup) {
            mSelectedMenuItemOptionIsNecessary.put(groupItemData.groupName, (groupItemData.isNecessary == 1));
        }


        ///전체 리스트 훑기
        for (String key : this.mSelectedOptionMap.keySet()) {
            LogUtil.d("선택 옵션 장바구니 담기 key : " + key);
            for (String subKey : this.mSelectedOptionMap.get(key).keySet()) {
                if ((this.mSelectedOptionMap.get(key).get(subKey)).mSelected)
                    mSelectedMenuItemOptionIsNecessaryCheck.add(this.mSelectedOptionMap.get(key).get(subKey).groupName);
            }

        }
//        for (int i = 0; i < mMenuOptionListAdapter.getItemCount(); i++) {
//            if (mMenuOptionListAdapter.getDataList().get(i) instanceof MenuOptionSub) {
//                if (mSelectedMenuItemOptionIsNecessary.containsKey(((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).groupName)) {
//
//                    if (((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).mSelected)
//                        mSelectedMenuItemOptionIsNecessaryCheck.add(((MenuOptionSub) mMenuOptionListAdapter.getDataList().get(i)).groupName);
//                }
//            }
//        }

        ///필수 항목과 대조하기
        for (String key : mSelectedMenuItemOptionIsNecessary.keySet()) {
            if (mSelectedMenuItemOptionIsNecessary.get(key)) ///조사할 항목이 필수이냐?
            {
                LogUtil.d("옵션 조사 항목 : [" + key + "]는 " + mSelectedMenuItemOptionIsNecessaryCheck.contains(key));
                if (!mSelectedMenuItemOptionIsNecessaryCheck.contains(key))  //체크된 옵션항목중에 필수항목인 녀석이 들어있는가? 없다면 문제가 있으니 false 리턴해라
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void goBack(View view) {
        getActivity().onBackPressed();
    }


    public void editCartItem(Order order) {
        this.editCartKey = order.key;
        this.editCartOrder = order;
    }

    private void editCartItemViewSetting() {

        if (this.editCartOrder != null) {
            LogUtil.d("카트 수정 editCartItem : " + this.editCartOrder.foodName);
            LogUtil.d("카트 수정 editCartItem1 : " + this.editCartOrder.categoryCode);
            if (TabletSetting.getInstance().getmAllMenusHashMap().get(this.editCartOrder.categoryCode) != null) {
                LogUtil.d("카트 수정 editCartItem2 : " + this.editCartOrder.categoryCode);

                for (Object obj : mMenuListAdapter.getDataList()) {
                    LogUtil.d("카트 수정 editCartItem obj: " + obj);

                    if (obj instanceof MenuCategory) {
                        for (MenuCategorySub menuCategorySub : ((MenuCategory) obj).mSubList) {
                            LogUtil.d("카트 수정 editCartItem menuCategorySub: " + menuCategorySub);
                            if (menuCategorySub.mMenuItem.CMDTCD.equals(this.editCartOrder.CMDTCD)) {
                                LogUtil.d("카트 수정 editCartItem CMDTCD: " + this.editCartOrder.CMDTCD);

                                clearSelectedOption();
                                mSelectedMenuItemData = menuCategorySub.mMenuItem;
                                initOptionList(menuCategorySub.mMenuItem);
                                binding.setSelectedMenuQuantity(this.editCartOrder.quantity);
                                binding.setSelectedMenuIsSet(menuCategorySub.mMenuItem.isSet);
                                binding.layoutOption.setVisibility(View.VISIBLE);   //옵션화면을 연다.
                                binding.recyclerViewOption.scrollToPosition(0);


                                ///옵션리스트에서 일치하는거 가져와야지 뭐
                                for (Object option : mMenuOptionListAdapter.getDataList()) {
                                    if (option instanceof MenuOption) {
                                        for (MenuOptionSub menuOptionSub : ((MenuOption) option).mSubList) {
                                            ///cart의 옵션을 루프 돌려야 함.
                                            if (this.editCartOrder.options != null) {
                                                for (String optionKey : this.editCartOrder.options.keySet()) {
                                                    if (menuOptionSub.mMenuSideOptionItemData.CMDTCD.equals(this.editCartOrder.options.get(optionKey).CMDTCD)
//                                                            &&menuOptionSub.groupName.equals(this.editCartOrder.options.get(optionKey).categoryName)

                                                            ) {

                                                        //해당 옵션이 존재하므로 넣어줘야 함
                                                        MenuOptionSub tmp = menuOptionSub;
                                                        tmp.quantity = this.editCartOrder.options.get(optionKey).quantity;
                                                        tmp.mSelected = true;

                                                        LogUtil.d("카트 수정 editCartItem 옵션 ------------------------------------");
                                                        LogUtil.d("카트 수정 editCartItem 옵션 : " + tmp.groupName);
                                                        LogUtil.d("카트 수정 editCartItem 옵션 : " + tmp.mMenuSideOptionItemData.CMDTCD);
                                                        LogUtil.d("카트 수정 editCartItem 옵션 : " + tmp.mMenuSideOptionItemData.foodName);
                                                        LogUtil.d("카트 수정 editCartItem 옵션 : " + tmp.mMenuSideOptionItemData.quantity);

                                                        if (this.mSelectedOptionMap != null) {
                                                            Boolean isOk = true;
                                                            for (String mapKey : this.mSelectedOptionMap.keySet()) {
                                                                if (this.mSelectedOptionMap.get(mapKey).containsKey(tmp.mMenuSideOptionItemData.CMDTCD)) {
                                                                    ///이미 포함됨
                                                                    isOk = false;
                                                                } else {
                                                                }

                                                            }
                                                            if (isOk)
                                                                this.addSelectedOption(tmp.groupName, tmp);   ///옵션 추가
                                                        } else {
                                                            this.addSelectedOption(tmp.groupName, tmp);
                                                        }
//
//                                                        if(this.mSelectedOptionMap.get(tmp.groupName) != null && this.mSelectedOptionMap.values().contains().containsKey(tmp.mMenuSideOptionItemData.CMDTCD))
//                                                        {
//                                                            ///이미 포함됨
//                                                        }
//                                                        else
//                                                        {
//                                                            this.addSelectedOption(tmp.groupName, tmp);   ///옵션 추가
//                                                        }
                                                    }
                                                }
                                                binding.optionSelectedOptions.setText(Html.fromHtml("선택: " + getSelectedOptionList())); //선택한 메뉴 옵션들 나열 지정
                                                this.getSelectedOptionsPrice();

                                            }
                                        }
                                    }
                                }

                                break;
                            }
                        }
                    }
                }
            }

        }
    }

}
