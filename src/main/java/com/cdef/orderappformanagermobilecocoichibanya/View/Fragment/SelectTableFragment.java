package com.cdef.orderappformanagermobilecocoichibanya.View.Fragment;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cdef.commonmodule.dataModel.firebaseDataSet.TableMember;
import com.cdef.commonmodule.Utils.TabletSetting;
import com.cdef.orderappformanagermobilecocoichibanya.OrderAppApplication;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.orderappformanagermobilecocoichibanya.Utils.NavigationUtils;
import com.cdef.orderappformanagermobilecocoichibanya.View.Adapter.TableListAdapter;
import com.cdef.orderappformanagermobilecocoichibanya.View.CustomView.NoAnimationItemAnimator;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.FragmentSelectTableBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class SelectTableFragment extends BaseFragment {

    FragmentSelectTableBinding binding;
    TableListAdapter adapter;

    public static int SPANCOUNT = 4;
    public PackageInfo pi = null;


    ValueEventListener versionChecker = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            LogUtil.d("변동이 있구나 : " + dataSnapshot);
            if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                binding.textUpdateNotice.setVisibility(View.GONE);
            } else {
                Long updateVersionCode = dataSnapshot.getValue(Long.class);
                if (updateVersionCode == null) {
                    binding.textUpdateNotice.setVisibility(View.GONE);
                } else {

                    //버전코드 등록
                    if (pi != null) {
                        int versionCode = pi.versionCode;
                        if (versionCode != 0 && versionCode < updateVersionCode) {
                            binding.textUpdateNotice.setVisibility(View.VISIBLE);
                        } else {
                            binding.textUpdateNotice.setVisibility(View.GONE);
                        }
                    }
                }
            }

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (pi == null)
                pi = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        FirebaseDatabase.getInstance().getReference()
                .child(LoginUtil.getInstance(null).getmBrandName())
                .child("apk")
                .child("versionCode")
                .addValueEventListener(this.versionChecker);

        if (LoginUtil.getInstance(getContext()) != null && LoginUtil.getInstance(getContext()).getmBrandName() != null && LoginUtil.getInstance(getContext()).branchInfo.branchId != null) {
            FirebaseDatabase.getInstance().getReference()
                    .child(LoginUtil.getInstance(getContext()).getmBrandName())
                    .child("useQueue")
                    .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                                ///기존방식 쓰던애들
                                OrderAppApplication.setUseQueue(getContext(), null);

                            } else {
                                OrderAppApplication.setUseQueue(getContext(), "queue");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }

    }

    @Override
    public void onPause() {
        super.onPause();


        FirebaseDatabase.getInstance().getReference()
                .child(LoginUtil.getInstance(null).getmBrandName())
                .child("apk")
                .child("versionCode")
                .removeEventListener(this.versionChecker);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_select_table, container, false);


        ///로그인정보가 초기화 되어있으면 로그인 화면으로 보내라
        FirebaseDatabase.getInstance().getReference()
                .child(LoginUtil.getInstance(getContext()).getmBrandName())
                .child("tables")
                .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
                .orderByKey()
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot item : dataSnapshot.getChildren()) {
                                LogUtil.d("테이블스 키:" + item.getKey());
                                LogUtil.d("테이블스 :" + item);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        if (adapter == null) {
            this.adapter = new TableListAdapter(getContext());
//            this.adapter.setLongClickListener((v, selectedIndex, data) -> {
//                Toast.makeText(getContext(), TabletSetting.getInstance().mTableMapInfo.get(selectedIndex).tNo + "번 테이블 선택", Toast.LENGTH_SHORT).show();
//                MessageDialogBuilder.getInstance(getContext())
//                        .setContent(TabletSetting.getInstance().mTableMapInfo.get(selectedIndex).tNo + "번 테이블의\n주문내역/인원/계산서를\n초기화하시겠습니까?\n(포스는 직접 취소해주세요.)")
//                        .setCancelButton()
//                        .setOkButton("초기화", view -> {
//
//                            ///삭제대상
//                            ///1. cart
//                            ///2. order
//                            if (TabletSetting.getInstance().mTableMapInfo.get(selectedIndex).tNo != null) {
//                                FirebaseDatabase.getInstance().getReference()
//                                        .child(LoginUtil.getInstance(getContext()).getmBrandName())
//                                        .child("cart")
//                                        .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
//                                        .child(TabletSetting.getInstance().mTableMapInfo.get(selectedIndex).tNo)
//                                        .setValue(null);
//
//                                FirebaseDatabase.getInstance().getReference()
//                                        .child(LoginUtil.getInstance(getContext()).getmBrandName())
//                                        .child("tables")
//                                        .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
//                                        .child(TabletSetting.getInstance().mTableMapInfo.get(selectedIndex).tNo)
//                                        .child("clear")
//                                        .setValue(2, (databaseError, databaseReference) -> {
//                                            if (databaseError != null) {
//                                                Toast.makeText(getContext(), "초기화에 실패했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
//                                                return;
//                                            }
//
//                                            Toast.makeText(getContext(), "초기화 완료", Toast.LENGTH_SHORT).show();
//                                            MessageDialogBuilder.getInstance(getContext()).dismiss();
//                                        });
//                            }
//                        }).complete();
//
//
//                return true;
//            });
            this.adapter.setListener((v, index, data) -> {
                TableListAdapter.mLastSelectedIndex = TableListAdapter.mSelectedIndex;
                TableListAdapter.mSelectedIndex = index;

                long member = 0;


                try {
                    member = (TabletSetting.getInstance().getMapInfo().get(TabletSetting.getInstance().mTableMapInfo.get(index).tNo).men) + TabletSetting.getInstance().getMapInfo().get(TabletSetting.getInstance().mTableMapInfo.get(index).tNo).women;
                    LogUtil.d("안눌리는 경우1 : " + member);
                    if (member > 0) {
                        Toast.makeText(getContext(), "선택한 테이블 번호(selected) : " + TabletSetting.getInstance().mTableMapInfo.get(index).tNo, Toast.LENGTH_SHORT).show();

                        Bundle bundle = new Bundle();
                        bundle.putString("tableNo", TabletSetting.getInstance().mTableMapInfo.get(index).tNo);
                        bundle.putLong("member", member);
                        NavigationUtils.openDetailActivity(getContext(), SelectMenuFragment.class.getSimpleName(), bundle);
                    } else {
                        Toast.makeText(getContext(), "선택한 테이블 번호 : " + TabletSetting.getInstance().mTableMapInfo.get(index).tNo, Toast.LENGTH_SHORT).show();
                        //빈테이블 처리
                        Bundle bundle = new Bundle();
                        bundle.putString("tableNo", TabletSetting.getInstance().mTableMapInfo.get(index).tNo);
                        NavigationUtils.openDetailActivity(getContext(), InitPersonFragment.class.getSimpleName(), bundle);
                    }
                    LogUtil.d("안눌리는 경우2 : " + member);

                } catch (Exception e) //table에 대한 정보가 갱신 안된경우에 null exception이 발생할 수 있다.
                {
                    TableMember tmp = new TableMember();
                    tmp.men = 0;
                    tmp.women = 0;
                    TabletSetting.getInstance().getMapInfo().put(TabletSetting.getInstance().mTableMapInfo.get(index).tNo, tmp);

                    try {
                        member = (TabletSetting.getInstance().getMapInfo().get(TabletSetting.getInstance().mTableMapInfo.get(index).tNo).men) + TabletSetting.getInstance().getMapInfo().get(TabletSetting.getInstance().mTableMapInfo.get(index).tNo).women;
                        LogUtil.d("안눌리는 경우1 : " + member);
                        if (member > 0) {
                            Toast.makeText(getContext(), "선택한 테이블 번호(selected) : " + TabletSetting.getInstance().mTableMapInfo.get(index).tNo, Toast.LENGTH_SHORT).show();

                            Bundle bundle = new Bundle();
                            bundle.putString("tableNo", TabletSetting.getInstance().mTableMapInfo.get(index).tNo);
                            bundle.putLong("member", member);
                            NavigationUtils.openDetailActivity(getContext(), SelectMenuFragment.class.getSimpleName(), bundle);
                        } else {
                            Toast.makeText(getContext(), "선택한 테이블 번호 : " + TabletSetting.getInstance().mTableMapInfo.get(index).tNo, Toast.LENGTH_SHORT).show();
                            //빈테이블 처리
                            Bundle bundle = new Bundle();
                            bundle.putString("tableNo", TabletSetting.getInstance().mTableMapInfo.get(index).tNo);
                            NavigationUtils.openDetailActivity(getContext(), InitPersonFragment.class.getSimpleName(), bundle);
                        }
                        LogUtil.d("안눌리는 경우2 : " + member);

                    } catch (Exception e2) //table에 대한 정보가 갱신 안된경우에 null exception이 발생할 수 있다.
                    {
                    }

                }


            });
        }

        this.binding.recyclerView.setAdapter(this.adapter);
        this.adapter.setmListData(TabletSetting.getInstance().mTableMapInfo);
        this.adapter.notifyDataSetChanged();
        this.binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), SelectTableFragment.SPANCOUNT));
        this.binding.recyclerView.setItemAnimator(new NoAnimationItemAnimator());
        this.binding.imageLogo.setOnLongClickListener(view -> {


            FirebaseDatabase.getInstance().getReference()
                    .child(LoginUtil.getInstance(null).getmBrandName())
                    .child("apk")
                    .child("versionCode")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                                Toast.makeText(getContext(), "업데이트 정보가 없습니다.", Toast.LENGTH_SHORT).show();

                            } else {
                                Long updateVersionCode = dataSnapshot.getValue(Long.class);
                                if (updateVersionCode == null) {
                                    Toast.makeText(getContext(), "업데이트 정보가 없습니다.", Toast.LENGTH_SHORT).show();
                                } else {

                                    //버전코드 등록
                                    if (pi != null) {
                                        int versionCode = pi.versionCode;
                                        if (versionCode != 0 && versionCode < updateVersionCode) {
                                            ///가지러 가자
                                            FirebaseDatabase.getInstance().getReference()
                                                    .child(LoginUtil.getInstance(null).getmBrandName())
                                                    .child("apk")
                                                    .child("appURL")
                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                                                                Toast.makeText(getContext(), "업데이트 버전이 없습니다.", Toast.LENGTH_SHORT).show();
                                                            } else {
                                                                try {
                                                                    String downloadUrl = dataSnapshot.getValue(String.class);
                                                                    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(downloadUrl));
                                                                    startActivity(myIntent);
                                                                    Toast.makeText(getContext(), "다운로드가 완료되면 설치하시면 됩니다.", Toast.LENGTH_SHORT).show();
                                                                } catch (Exception e) {

                                                                }
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });
                                        } else {
                                            Toast.makeText(getContext(), "현재버전이 최신버전입니다.", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {


                                        FirebaseDatabase.getInstance().getReference()
                                                .child(LoginUtil.getInstance(null).getmBrandName())
                                                .child("apk")
                                                .child("appURL")
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                                                            Toast.makeText(getContext(), "업데이트 버전이 없습니다.", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            try {
                                                                String downloadUrl = dataSnapshot.getValue(String.class);
                                                                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(downloadUrl));
                                                                startActivity(myIntent);
                                                                Toast.makeText(getContext(), "다운로드가 완료되면 설치하시면 됩니다.", Toast.LENGTH_SHORT).show();
                                                            } catch (Exception e) {

                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });

                                    }
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


            return true;
        });
        this.binding.setBranchName(LoginUtil.getInstance(getContext()).branchInfo.branchName);

//        if (TableListAdapter.mSelectedIndex != 0) {
//            this.binding.recyclerView.getLayoutManager().scrollToPosition(TableListAdapter.mSelectedIndex);
//        }


        PackageInfo pi = null;
        //버전코드 등록
        try {
            pi = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (pi != null) {
            String versionCode = String.valueOf(pi.versionCode);
            String versionName = pi.versionName;
            this.binding.setVersionCode(versionName + "(" + versionCode + ")");
        }

        return binding.getRoot();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
