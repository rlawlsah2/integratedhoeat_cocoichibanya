package com.cdef.orderappformanagermobilecocoichibanya.View.Expandable;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable.MenuCategory;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.zaihuishou.expandablerecycleradapter.model.ExpandableListItem;
import com.zaihuishou.expandablerecycleradapter.viewholder.AbstractExpandableAdapterItem;

/**
 * Created by kimjinmo on 2018. 4. 23..
 */

public class MenuCategoryItem extends AbstractExpandableAdapterItem {

    MenuCategory menuCategory;
    TextView title, titleSub;
    ImageView iconState;


    MenuCategoryItem.OnClickItemListener listener;

    public MenuCategoryItem(MenuCategoryItem.OnClickItemListener listener) {
        this.listener = listener;
    }

    public interface OnClickItemListener {
        void click(View view, ExpandableListItem item);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.adapter_menu_parent_list;
    }


    @Override
    public void onBindViews(final View root) {
        title = root.findViewById(R.id.textTitle);
        titleSub = root.findViewById(R.id.textTitleSub);
        iconState = root.findViewById(R.id.iconState);
        /**
         * control item expand and unexpand
         */
        root.setOnClickListener(view -> {
//            doExpandOrUnexpand();
            if (listener != null) {
                listener.click(view, getExpandableListItem());
            }
//            Toast.makeText(root.getContext(), "click company：" + menuCategory., Toast.LENGTH_SHORT).show();
        });

    }

    public void doExpandOrUnexpand() {
        super.doExpandOrUnexpand();
        this.iconState.setEnabled(!getExpandableListItem().isExpanded());   ///enable과 expanded는 서로 반대라서 !
    }

    public void collapseView() {
        super.collapseView();
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
    }

    @Override
    public void onSetViews() {
    }

    @Override
    public void onUpdateViews(Object model, int position) {
        super.onUpdateViews(model, position);
        onSetViews();
        onExpansionToggled(getExpandableListItem().isExpanded());

        menuCategory = (MenuCategory) model;
        this.title.setText(menuCategory.mCategoryItem.categoryName);
    }


}