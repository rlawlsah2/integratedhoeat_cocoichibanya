package com.cdef.orderappformanagermobilecocoichibanya.View.Expandable;

import android.view.View;
import android.widget.TextView;

import com.cdef.commonmodule.Utils.Utils;
import com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable.MenuCategorySub;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.zaihuishou.expandablerecycleradapter.model.ExpandableListItem;
import com.zaihuishou.expandablerecycleradapter.viewholder.AbstractExpandableAdapterItem;

import java.util.List;

/**
 * Created by kimjinmo on 2018. 4. 23..
 */

public class MenuCategorySubItem extends AbstractExpandableAdapterItem {

    MenuCategorySub menuCategorySub;
    OnClickItemListener listener;

    TextView title, price;

    public MenuCategorySubItem(OnClickItemListener listener) {
        this.listener = listener;
    }

    public interface OnClickItemListener {
        void click(View view, MenuCategorySub item);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.adapter_menu_child_list;
    }

    @Override
    public void onBindViews(final View root) {

        this.title = root.findViewById(R.id.title);
        this.price = root.findViewById(R.id.price);
        /**
         * control item expand and unexpand
         */
        root.setOnClickListener(view -> {
            doExpandOrUnexpand();
            if (listener != null)
                listener.click(view, menuCategorySub);
//                Toast.makeText(root.getContext(), "click company：" + menuCategorySub.title, Toast.LENGTH_SHORT).show();
        });
    }

    public void collapse() {
        super.collapseView();
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
//        float start, target;
//        if (expanded) {
//            start = 0f;
//            target = 90f;
//        } else {
//            start = 90f;
//            target = 0f;
//        }
//        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mArrow, View.ROTATION, start, target);
//        objectAnimator.setDuration(300);
//        objectAnimator.start();
    }

    @Override
    public void onSetViews() {
//        mArrow.setImageResource(0);
//        mArrow.setImageResource(R.mipmap.arrow_down);
    }

    @Override
    public void onUpdateViews(Object model, int position) {
        super.onUpdateViews(model, position);
        onSetViews();
        menuCategorySub = (MenuCategorySub) model;
        ExpandableListItem parentListItem = (ExpandableListItem) model;
        List<?> childItemList = parentListItem.getChildItemList();
        this.title.setText(menuCategorySub.mMenuItem.foodName);
        this.price.setText(Utils.setComma(menuCategorySub.mMenuItem.price) + "원");
//        if (childItemList != null && !childItemList.isEmpty()) {
//            mArrow.setVisibility(View.VISIBLE);
//            mExpand.setText(parentListItem.isExpanded() ? "unexpand" : "expand");
//        } else mExpand.setText("expand");
    }

}