package com.cdef.orderappformanagermobilecocoichibanya.View.Expandable;

import android.databinding.DataBindingUtil;
import android.view.View;

import com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable.MenuOption;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.AdapterMenuOptionParentListBinding;
import com.zaihuishou.expandablerecycleradapter.model.ExpandableListItem;
import com.zaihuishou.expandablerecycleradapter.viewholder.AbstractExpandableAdapterItem;

/**
 * Created by kimjinmo on 2018. 4. 23..
 */

public class MenuOptionItem extends AbstractExpandableAdapterItem {

    AdapterMenuOptionParentListBinding binding;

    public MenuOption menuOption;


    MenuOptionItem.OnClickItemListener listener;

    public MenuOptionItem(MenuOptionItem.OnClickItemListener listener) {
        this.listener = listener;
    }

    public interface OnClickItemListener {
        void click(ExpandableListItem view);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.adapter_menu_option_parent_list;
    }


    @Override
    public void onBindViews(final View root) {
        this.binding = DataBindingUtil.bind(root);
//        title = root.findViewById(R.id.textTitle);
//        imageIsNecessary = root.findViewById(R.id.imageIsNecessary);
//        root.setClickable(false);
        /**
         * control item expand and unexpand
         */
        root.setOnClickListener(view -> {
            LogUtil.d("옵션 부모 접기 상태 : " + getExpandableListItem().isExpanded());
            if (this.listener != null)
                this.listener.click(getExpandableListItem());
//            doExpandOrUnexpand();
        });

    }

    public void doExpandOrUnexpand() {
        super.doExpandOrUnexpand();

    }

    public void collapseView() {
        super.collapseView();
    }

    @Override
    public void onExpansionToggled(boolean expanded) {

    }

    @Override
    public void onSetViews() {
    }

    @Override
    public void onUpdateViews(Object model, int position) {
        super.onUpdateViews(model, position);
        onSetViews();
        onExpansionToggled(getExpandableListItem().isExpanded());
        menuOption = (MenuOption) model;
        try {
            this.binding.textTitle.setText(menuOption.mMenuSideGroupItemData.groupName.replace(":", ""));
        } catch (Exception e) {
        }
        if (menuOption.mMenuSideGroupItemData.isNecessary == 1) {
            this.binding.imageIsNecessary.setVisibility(View.VISIBLE);
        } else {
            this.binding.imageIsNecessary.setVisibility(View.GONE);
        }
    }


}