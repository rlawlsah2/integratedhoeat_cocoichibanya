package com.cdef.orderappformanagermobilecocoichibanya.View.Expandable;

import android.databinding.DataBindingUtil;
import android.view.View;

import com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable.MenuOptionSub;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.commonmodule.Utils.Utils;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.AdapterMenuOptionChildListBinding;
import com.zaihuishou.expandablerecycleradapter.viewholder.AbstractExpandableAdapterItem;

/**
 * Created by kimjinmo on 2018. 4. 23..
 */

public class MenuOptionSubItem extends AbstractExpandableAdapterItem {

    AdapterMenuOptionChildListBinding binding;
    MenuOptionSub menuOptionSub;
    OnClickItemListener listener;
    OnChangeQuantiryListener listener1;

    public MenuOptionSub getMenuOptionSub(int position)
    {
        return menuOptionSub;
//        return (MenuOptionSub) getExpandableListItem().getChildItemList().get(position);
    }

    public MenuOptionSubItem(OnClickItemListener listener, OnChangeQuantiryListener listener1) {
        this.listener = listener;
        this.listener1 = listener1;
    }

    public interface OnClickItemListener {
        void click(View view, MenuOptionSub item, MenuOptionSubItem menuOptionSubItem);
    }


    public interface OnChangeQuantiryListener {
        void click(View view, MenuOptionSub item, MenuOptionSubItem menuOptionSubItem, Boolean isPlus);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.adapter_menu_option_child_list;
    }

    @Override
    public void onBindViews(final View root) {
        this.binding = DataBindingUtil.bind(root);
        this.binding.setIsSelected(false);
        this.binding.setAdapter(this);
        this.binding.setListener(this.listener);
        this.binding.setListener1(this.listener1);
        this.binding.imageCheckBox.setSelected(false);
        this.binding.layoutChangeQuantity.setEnabled(false);

        /**
         * control item expand and unexpand
         */
        this.binding.layoutSubMain.setOnClickListener(view -> {
//            doExpandOrUnexpand();
//            view.setSelected(!view.isSelected());
//            menuOptionSub.mSelected = !menuOptionSub.mSelected;
//            view.setSelected(menuOptionSub.mSelected);
            if (listener != null)
                listener.click(view, menuOptionSub, this);
//                Toast.makeText(root.getContext(), "click company：" + menuCategorySub.title, Toast.LENGTH_SHORT).show();
        });

    }

    public void collapse() {
        super.collapseView();
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
//        float start, target;
//        if (expanded) {
//            start = 0f;
//            target = 90f;
//        } else {
//            start = 90f;
//            target = 0f;
//        }
//        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mArrow, View.ROTATION, start, target);
//        objectAnimator.setDuration(300);
//        objectAnimator.start();
    }

    @Override
    public void onSetViews() {
//        mArrow.setImageResource(0);
//        mArrow.setImageResource(R.mipmap.arrow_down);
    }

    @Override
    public void onUpdateViews(Object model, int position) {
        super.onUpdateViews(model, position);
        onSetViews();
        menuOptionSub = (MenuOptionSub) model;
        this.binding.setIsSelected(menuOptionSub.mSelected);
//        this.binding.setIsSelected(menuOptionSub.mMenuSideOptionItemData.defaultItem == 1);
        this.binding.setMenu(menuOptionSub);
        this.binding.title.setText(Utils.removeFirstSpace(menuOptionSub.mMenuSideOptionItemData.foodName));
        if (menuOptionSub.mMenuSideOptionItemData.price > 0) {
            this.binding.price.setText("+ " + Utils.setComma(menuOptionSub.mMenuSideOptionItemData.price) + "원");
        } else {

            try {
                this.binding.price.setText((Utils.setComma(menuOptionSub.mMenuSideOptionItemData.price) + "원").replaceAll("-", "- "));
            } catch (Exception e) {
                this.binding.price.setText(Utils.setComma(menuOptionSub.mMenuSideOptionItemData.price) + "원");
            }
        }
        this.binding.imageCheckBox.setSelected(menuOptionSub.mSelected);
        this.binding.layoutChangeQuantity.setEnabled(menuOptionSub.mSelected);

    }

    public void changeQuantity(View view) {

    }

}