package com.cdef.orderappformanagermobilecocoichibanya.View.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.firebaseDataSet.TableInfo;
import com.cdef.commonmodule.dataModel.TableMapInfoData;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.commonmodule.Utils.TabletSetting;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.AdapterTableListBinding;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class TableListAdapter extends RecyclerView.Adapter<TableListAdapter.ViewHolder> {


    public static int height = 0;
    public static int mSelectedIndex = 0;
    public static int mLastSelectedIndex = 0;

    HashMap<String, Integer> tableNoPosition = new HashMap<>();
    HashMap<String, String> orderIdPosition = new HashMap<>();
    ArrayList<TableMapInfoData> mListData = new ArrayList<>();
    RequestManager glide;
    RequestOptions myOptions = null;
    int totalTableCount = 0;
    int[] sizeList = {11, 12, 13, 14, 15, 16, 17, 18, 19};


    public TableListAdapter.OnItemClickListener listener;
    public TableListAdapter.OnItemLongClickListener longClickListener;

    public void setListener(TableListAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public void setLongClickListener(TableListAdapter.OnItemLongClickListener listener) {
        this.longClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View v, int selectedIndex, TableMapInfoData data);
    }


    public interface OnItemLongClickListener {
        boolean onItemLongClick(View v, int selectedIndex, TableMapInfoData data);
    }


    public void setmListData(ArrayList<TableMapInfoData> data) {

        this.mListData.clear();
        this.mListData.addAll(data);

        LogUtil.d("테이블 맵 크기 : " + data.size());
    }

    public TableListAdapter(Context context) {
        ///파이어베이스 달자.
        FirebaseDatabase.getInstance().getReference()
                .child(LoginUtil.getInstance(context).getmBrandName())
                .child("tables")
                .child(LoginUtil.getInstance(context).branchInfo.branchId)
                .orderByKey()
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot tables) {
                        if (tables != null && tables.getChildrenCount() > 0) {
                            totalTableCount = (int) tables.getChildrenCount();


                            FirebaseDatabase.getInstance().getReference()
                                    .child(LoginUtil.getInstance(context).getmBrandName())
                                    .child("tables")
                                    .child(LoginUtil.getInstance(context).branchInfo.branchId)
                                    .orderByKey()
                                    .addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            LogUtil.d("TableListAdapter 의 파이어베이스 onChildAdded : " + dataSnapshot);
                                            try {
                                                if (dataSnapshot != null && dataSnapshot.getValue(TableInfo.class) != null) {
                                                    totalTableCount--;
                                                    TabletSetting.getInstance().setMapInfo(dataSnapshot.getKey(), dataSnapshot.getValue(TableInfo.class).members);
                                                    orderIdPosition.put(dataSnapshot.getKey(), dataSnapshot.getValue(TableInfo.class).IFSA_ORDER_ID);
                                                    if (totalTableCount <= 0) {
                                                        notifyDataSetChanged();
                                                    }
                                                }
                                            } catch (Exception e) {

                                            }
                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                            LogUtil.d("TableListAdapter 의 파이어베이스 onChildChanged : " + dataSnapshot);
                                            try {

                                                if (dataSnapshot != null && dataSnapshot.getValue(TableInfo.class) != null) {
                                                    TabletSetting.getInstance().setMapInfo(dataSnapshot.getKey(), dataSnapshot.getValue(TableInfo.class).members);
                                                    orderIdPosition.put(dataSnapshot.getKey(), dataSnapshot.getValue(TableInfo.class).IFSA_ORDER_ID);
                                                    try {
                                                        notifyItemChanged(tableNoPosition.get(dataSnapshot.getKey()));
                                                    } catch (Exception e) {
                                                    }
                                                }
                                            } catch (Exception e) {

                                            }
                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                                            try{
                                            LogUtil.d("TableListAdapter 의 파이어베이스 onChildRemoved : " + dataSnapshot);
                                            if (dataSnapshot != null && dataSnapshot.getValue(TableInfo.class) != null) {
                                                TabletSetting.getInstance().setMapInfo(dataSnapshot.getKey(), dataSnapshot.getValue(TableInfo.class).members);
                                                orderIdPosition.put(dataSnapshot.getKey(), dataSnapshot.getValue(TableInfo.class).IFSA_ORDER_ID);
                                                try {
                                                    notifyItemChanged(tableNoPosition.get(dataSnapshot.getKey()));
                                                } catch (Exception e) {
                                                }
                                            }}
                                            catch (Exception e)
                                            {

                                            }

                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                        } else {
                            Toast.makeText(context, "일시적으로 테이블 정보를 가져올 수 없습니다.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_table_list, parent, false));
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        TableMapInfoData data = mListData.get(position);
        this.tableNoPosition.put(data.tNo, position);

        holder.binding.setTableType(data.tNo == null ? null : (data.tNo.startsWith("D") ? "D" : (data.tNo.startsWith("A") ? "A" : (data.tNo.startsWith("B") ? "B" : (data.tNo.startsWith("F") ? "F" : (data.tNo.equals("ZDUMMY") ? "ZDUMMY" : null))))));
        holder.binding.setTable(data);
        holder.binding.setListener(listener);
        holder.binding.setCurrentPosition(position);
        holder.binding.setOrderId(orderIdPosition.get(data.tNo));
//        holder.binding.layoutMain.setOnLongClickListener(view -> {
//            if (this.longClickListener != null) {
//                return this.longClickListener.onItemLongClick(view, position, data);
//            }
//            return true;
//        });

        holder.binding.layoutMain.setSelected((TabletSetting.getInstance().getMapInfo() != null && TabletSetting.getInstance().getMapInfo().get(data.tNo) != null && (TabletSetting.getInstance().getMapInfo().get(data.tNo).men > 0 || TabletSetting.getInstance().getMapInfo().get(data.tNo).women > 0)));
        holder.binding.title.setSelected((TabletSetting.getInstance().getMapInfo() != null && TabletSetting.getInstance().getMapInfo().get(data.tNo) != null && (TabletSetting.getInstance().getMapInfo().get(data.tNo).men > 0 || TabletSetting.getInstance().getMapInfo().get(data.tNo).women > 0)));
        if ((TabletSetting.getInstance().getMapInfo() != null && TabletSetting.getInstance().getMapInfo().get(data.tNo) != null &&
                (TabletSetting.getInstance().getMapInfo().get(data.tNo).men > 0 || TabletSetting.getInstance().getMapInfo().get(data.tNo).women > 0))) {
            holder.binding.setPersonCount((int) (TabletSetting.getInstance().getMapInfo().get(data.tNo).men + TabletSetting.getInstance().getMapInfo().get(data.tNo).women));
        } else {
            holder.binding.setPersonCount(0);
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterTableListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.layoutMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    binding.layoutMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    if (TableListAdapter.height == 0) {
                        ViewGroup.LayoutParams lp = binding.layoutMain.getLayoutParams();
                        lp.height = binding.layoutMain.getMeasuredWidth();
                        binding.layoutMain.setLayoutParams(lp);
                        TableListAdapter.height = lp.height;
                    } else {
                        ViewGroup.LayoutParams lp = binding.layoutMain.getLayoutParams();
                        lp.height = TableListAdapter.height;
                        binding.layoutMain.setLayoutParams(lp);
                    }
                }
            });

        }
    }


}
