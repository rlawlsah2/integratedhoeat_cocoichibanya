package com.cdef.orderappformanagermobilecocoichibanya.View.Adapter;

import android.databinding.DataBindingUtil;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.Utils.TabletSetting;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.AdapterCategorySubListBinding;

import java.util.ArrayList;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class CategorySubAdapter extends RecyclerView.Adapter<CategorySubAdapter.ViewHolder> {

    ArrayList<MenuItemData> mListData = new ArrayList<>();
    RequestManager glide;
    RequestOptions myOptions = null;

    int[] sizeList = {11,12,13,14,15,16,17,18,19};


    public CategorySubAdapter.OnItemClickListener listener;

    public void setListener(CategorySubAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View v, MenuItemData menu, int index);
    }


    public void setmListData(ArrayList<MenuItemData> data) {

        this.mListData.clear();
        this.mListData.addAll(data);
    }

    public CategorySubAdapter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_category_sub_list, parent, false));
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        MenuItemData data = mListData.get(position);
        holder.binding.setMenu(data);
        holder.binding.setListener(listener);
        holder.binding.setIndex(position);
        holder.binding.setSelected(TabletSetting.getInstance().mCartList.containsKey(data.CMDTCD));
        holder.binding.background.setSelected(TabletSetting.getInstance().mCartList.containsKey(data.CMDTCD));
        TextViewCompat.setAutoSizeTextTypeWithDefaults(holder.binding.title, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(holder.binding.title,
                sizeList ,TypedValue.COMPLEX_UNIT_SP);
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterCategorySubListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }


}
