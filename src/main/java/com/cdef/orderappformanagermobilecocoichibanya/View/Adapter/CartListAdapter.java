package com.cdef.orderappformanagermobilecocoichibanya.View.Adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.LayoutRes;
import android.text.Html;
import android.view.View;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.orderappformanagermobilecocoichibanya.View.ViewHolder.CartListViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.ObservableSnapshotArray;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by kimjinmo on 2016. 11. 16..
 * <p>
 * #최적화 작업완료
 */

public class
CartListAdapter extends FirebaseRecyclerAdapter<Order, CartListViewHolder> {

    public static CartListAdapter.OnItemClickListener listener;

    public void setListener(CartListAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public CartListAdapter(ObservableSnapshotArray<Order> dataSnapshots, @LayoutRes int modelLayout, Class<CartListViewHolder> viewHolderClass, LifecycleOwner owner) {
        super(dataSnapshots, modelLayout, viewHolderClass, owner);
    }

    public CartListAdapter(ObservableSnapshotArray<Order> dataSnapshots, @LayoutRes int modelLayout, Class<CartListViewHolder> viewHolderClass) {
        super(dataSnapshots, modelLayout, viewHolderClass);
    }

    public CartListAdapter(SnapshotParser<Order> parser, @LayoutRes int modelLayout, Class<CartListViewHolder> viewHolderClass, Query query) {
        super(parser, modelLayout, viewHolderClass, query);
    }

    public CartListAdapter(SnapshotParser<Order> parser, @LayoutRes int modelLayout, Class<CartListViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
        super(parser, modelLayout, viewHolderClass, query, owner);
    }

    public CartListAdapter(Class<Order> modelClass, @LayoutRes int modelLayout, Class<CartListViewHolder> viewHolderClass, Query query) {
        super(modelClass, modelLayout, viewHolderClass, query);
    }

    public CartListAdapter(Class<Order> modelClass, @LayoutRes int modelLayout, Class<CartListViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
        super(modelClass, modelLayout, viewHolderClass, query, owner);
    }

    public ObservableSnapshotArray<Order> getData() {
        return this.mSnapshots;
    }

    public int getTotalPrice() {
        int calcPrice = 0;
        for (DataSnapshot item : getData()) {
            calcPrice += (item.getValue(Order.class).price * item.getValue(Order.class).quantity);
        }

        return (calcPrice);
    }

    @Override
    protected void populateViewHolder(CartListViewHolder holder, Order dataSet, int position) {

        LogUtil.d("리스트 구성중 : " + position);

        holder.binding.setItem(dataSet);
        holder.binding.setListener(listener);
        holder.binding.textOptions.setText("");
        if (dataSet.options != null && dataSet.options.size() > 0) {
//            StringBuilder sb = new StringBuilder();
//            for (String key : dataSet.options.keySet()) {
//                sb.append((dataSet.options.get(key).foodName));
//                sb.append(", ");
//            }

            try {
//                int lastIndex = sb.toString().lastIndexOf(", ");

                holder.binding.textOptions.setText(Html.fromHtml("선택: " + getSelectedOptionList(dataSet.options))); //선택한 메뉴 옵션들 나열 지정

            } catch (Exception e) {


                holder.binding.textOptions.setText("선택: " + getSelectedOptionList(dataSet.options)); //선택한 메뉴 옵션들 나열 지정

            }

        }


    }


    private String getSelectedOptionList(HashMap<String, Order> list) {
        StringBuilder sb = new StringBuilder();

        ///밥 맵기는 뒤로
        ArrayList<Order> etc = new ArrayList<>();

        for (String key : list.keySet()) {

            if ((list.get(key) != null && list.get(key).categoryName != null) && (list.get(key).categoryName.trim().equals("밥") || list.get(key).categoryName.equals("맵기"))) {
                etc.add(list.get(key));
            } else {
                sb.append("<b><u><font color='#000000'>");
                sb.append(list.get(key).foodName);
                sb.append("(");
                sb.append(list.get(key).quantity);
                sb.append("ea)");
                sb.append("</font></u></b>");
                sb.append(", ");
            }
        }

        //밥 맵기는 뒤로 뺀다
        if (etc != null && etc.size() > 0) {
            for (Order item : etc) {
                sb.append(item.foodName);
                sb.append("(");
                sb.append((item).quantity);
                sb.append("ea)");
                sb.append(", ");
            }
        }


        try {
            int lastIndex = sb.toString().lastIndexOf(", ");
            return sb.toString().substring(0, lastIndex);
        } catch (Exception e) {
            return sb.toString();
        }
    }


    @Override
    public long getItemId(int position) {
//        return super.getItemId(position);
        return getData().get(position).getValue().hashCode();
    }

    public void onDestroy() {

    }

    public interface OnItemClickListener {
        void selectedItem(View view, Order order);

        void changeQuantity(View view, Order order, Boolean isPlus);

        void deleteItem(View view, Order order);
    }


}
