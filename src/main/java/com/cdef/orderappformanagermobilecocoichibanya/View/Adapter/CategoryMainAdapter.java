package com.cdef.orderappformanagermobilecocoichibanya.View.Adapter;

import android.databinding.DataBindingUtil;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.OrderCategoryData;
import com.cdef.orderappformanagermobilecocoichibanya.R;
import com.cdef.orderappformanagermobilecocoichibanya.databinding.AdapterCategoryMainListBinding;

import java.util.ArrayList;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class CategoryMainAdapter extends RecyclerView.Adapter<CategoryMainAdapter.ViewHolder> {

    ArrayList<OrderCategoryData.CategoryItem> mListData = new ArrayList<>();
    RequestManager glide;
    RequestOptions myOptions = null;

    int[] sizeList = {11,12,13,14,15,16,17,18,19};


    public CategoryMainAdapter.OnItemClickListener listener;

    public void setListener(CategoryMainAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View v, OrderCategoryData.CategoryItem itemData);
    }


    public void setmListData(ArrayList<OrderCategoryData.CategoryItem> data) {

        this.mListData.clear();
        this.mListData.addAll(data);
    }

    public CategoryMainAdapter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_category_main_list, parent, false));
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        OrderCategoryData.CategoryItem data = mListData.get(position);
        holder.binding.setCategory(data);
        holder.binding.setListener(listener);
        TextViewCompat.setAutoSizeTextTypeWithDefaults(holder.binding.title, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(holder.binding.title,
                sizeList ,TypedValue.COMPLEX_UNIT_SP);
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterCategoryMainListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }


}
