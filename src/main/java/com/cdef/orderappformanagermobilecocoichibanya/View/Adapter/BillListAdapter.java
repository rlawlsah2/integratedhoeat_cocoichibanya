package com.cdef.orderappformanagermobilecocoichibanya.View.Adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.LayoutRes;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.orderappformanagermobilecocoichibanya.View.ViewHolder.BillListViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.ObservableSnapshotArray;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;


/**
 * Created by kimjinmo on 2016. 11. 16..
 * <p>
 * #최적화 작업완료
 */

public class BillListAdapter extends FirebaseRecyclerAdapter<Order, BillListViewHolder> {

    public BillListAdapter(ObservableSnapshotArray<Order> dataSnapshots, @LayoutRes int modelLayout, Class<BillListViewHolder> viewHolderClass, LifecycleOwner owner) {
        super(dataSnapshots, modelLayout, viewHolderClass, owner);
    }

    public BillListAdapter(ObservableSnapshotArray<Order> dataSnapshots, @LayoutRes int modelLayout, Class<BillListViewHolder> viewHolderClass) {
        super(dataSnapshots, modelLayout, viewHolderClass);
    }

    public BillListAdapter(SnapshotParser<Order> parser, @LayoutRes int modelLayout, Class<BillListViewHolder> viewHolderClass, Query query) {
        super(parser, modelLayout, viewHolderClass, query);
    }

    public BillListAdapter(SnapshotParser<Order> parser, @LayoutRes int modelLayout, Class<BillListViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
        super(parser, modelLayout, viewHolderClass, query, owner);
    }

    public BillListAdapter(Class<Order> modelClass, @LayoutRes int modelLayout, Class<BillListViewHolder> viewHolderClass, Query query) {
        super(modelClass, modelLayout, viewHolderClass, query);
    }

    public BillListAdapter(Class<Order> modelClass, @LayoutRes int modelLayout, Class<BillListViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
        super(modelClass, modelLayout, viewHolderClass, query, owner);
    }

    public ObservableSnapshotArray<Order> getData() {
        return this.mSnapshots;
    }

    public int getTotalPrice() {
        int calcPrice = 0;
        for (DataSnapshot item : getData()) {
            calcPrice += (item.getValue(Order.class).price * item.getValue(Order.class).quantity);
        }

        return (calcPrice);
    }

    @Override
    protected void populateViewHolder(BillListViewHolder holder, Order dataSet, int position) {

        LogUtil.d("리스트 구성중 : " + position);

        holder.binding.setItem(dataSet);
        holder.binding.setOptions("");

        if (dataSet.options != null && dataSet.options.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String key : dataSet.options.keySet()) {
                sb.append((dataSet.options.get(key).foodName));
                sb.append(", ");
            }

            try {
                int lastIndex = sb.toString().lastIndexOf(", ");
                holder.binding.setOptions("선택: " + sb.toString().substring(0, lastIndex)); //선택한 메뉴 옵션들 나열 지정

            } catch (Exception e) {
                holder.binding.setOptions("선택: " + sb.toString()); //선택한 메뉴 옵션들 나열 지정

            }

        }


    }

}
