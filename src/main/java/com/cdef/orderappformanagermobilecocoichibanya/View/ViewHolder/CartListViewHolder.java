package com.cdef.orderappformanagermobilecocoichibanya.View.ViewHolder;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cdef.orderappformanagermobilecocoichibanya.databinding.AdapterCartListBinding;

/**
 * Created by kimjinmo on 2018. 3. 13..
 */

public class CartListViewHolder extends RecyclerView.ViewHolder {


    public AdapterCartListBinding binding;

    public CartListViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}
