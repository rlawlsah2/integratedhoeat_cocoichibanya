package com.cdef.orderappformanagermobilecocoichibanya.View.ViewHolder;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cdef.orderappformanagermobilecocoichibanya.databinding.AdapterBillListBinding;

/**
 * Created by kimjinmo on 2018. 3. 13..
 */

public class BillListViewHolder extends RecyclerView.ViewHolder {


    public AdapterBillListBinding binding;

    public BillListViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}
