package com.cdef.orderappformanagermobilecocoichibanya.Networking;

import android.content.Context;

import com.cdef.commonmodule.Networking.NetworkUtil;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.orderappformanagermobilecocoichibanya.OrderAppApplication;

/**
 * Created by kimjinmo on 2018. 7. 5..
 *
 *
 *
 */

public class CocoichibanyaNetworkUtil extends NetworkUtil {
    protected static CocoichibanyaNetworkUtil instance = null;

    public static CocoichibanyaNetworkUtil getInstance(Context context) {
        if (CocoichibanyaNetworkUtil.instance == null)
            instance = new CocoichibanyaNetworkUtil(context);
        return instance;
    }



    private CocoichibanyaNetworkUtil(Context context) {
        super(context);

        LogUtil.d("홈 네트워크 유틸 생성자");
    }

    @Override
    public void inject(Context context) {
        super.inject(context);
        LogUtil.d("홈 네트워크 유틸 inject");

        OrderAppApplication
                .getNetworkComponent(context)
                .inject(this);

    }
}
