package com.cdef.orderappformanagermobilecocoichibanya.Networking.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */

public class OrderResponse {


    @SerializedName("status")
    @Expose
    public String status;


    @SerializedName("errCode")
    @Expose
    public String errCode;

}
