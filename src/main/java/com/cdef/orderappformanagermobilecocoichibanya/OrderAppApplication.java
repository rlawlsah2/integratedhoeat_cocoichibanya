package com.cdef.orderappformanagermobilecocoichibanya;

import android.app.Application;
import android.content.Context;

import com.cdef.commonmodule.Networking.NetworkModule;
import com.cdef.commonmodule.Utils.PreferenceUtils;
import com.cdef.orderappformanagermobilecocoichibanya.Networking.DaggerNetworkComponent;
import com.cdef.orderappformanagermobilecocoichibanya.Networking.NetworkComponent;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by kimjinmo on 2016. 11. 12..
 * 애플리케이션 전반적인 관리를 하는 클래스. 생명주기가 따로 존재함 참고바람
 */

public class OrderAppApplication extends Application {


    private static String useQueue = null;
    public static String getUseQueue(Context context)
    {
        if(OrderAppApplication.useQueue == null)
        {
            OrderAppApplication.useQueue = PreferenceUtils.getStringValuePref(context, "pref_name_useQueue" , "pref_key_useQueue");
        }
        return OrderAppApplication.useQueue;
    }
    public static void setUseQueue(Context context , String str)
    {
        OrderAppApplication.useQueue = str;
        PreferenceUtils.addStringValuePref(context, "pref_name_useQueue" , "pref_key_useQueue", str);
    }

    public static Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(false);


    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public NetworkComponent networkComponent = DaggerNetworkComponent.builder()
            .networkModule(new NetworkModule())
            .build();
    public static NetworkComponent getNetworkComponent(Context context)
    {
        return ((OrderAppApplication)context.getApplicationContext()).networkComponent;
    }

}
