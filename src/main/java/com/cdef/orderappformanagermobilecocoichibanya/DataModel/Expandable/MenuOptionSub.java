package com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable;

import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.zaihuishou.expandablerecycleradapter.model.ExpandableListItem;

import java.util.List;

/**
 * Created by kimjinmo on 2018. 4. 23..
 */

public class MenuOptionSub implements ExpandableListItem {

    public boolean mExpanded = false;
    public boolean mIsAbleToMultiCheck = false;
    public boolean mSelected = false;
    public MenuSideOptionItemData mMenuSideOptionItemData;
    public String groupName;
    public int maxGroupItem;
    public int quantity = 0;

    @Override
    public List<?> getChildItemList() {
        return null;
    }

    @Override
    public boolean isExpanded() {
        return mExpanded;
    }

    @Override
    public void setExpanded(boolean isExpanded) {
        mExpanded = isExpanded;
    }

}