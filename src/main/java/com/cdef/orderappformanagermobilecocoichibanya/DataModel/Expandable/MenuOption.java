package com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable;

import com.cdef.commonmodule.dataModel.MenuSideGroupItemData;
import com.zaihuishou.expandablerecycleradapter.model.ExpandableListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kimjinmo on 2018. 4. 23..
 */

public class MenuOption implements ExpandableListItem {

    public boolean mExpanded = false;
    public List<MenuOptionSub> mSubList = new ArrayList<>();

    public MenuSideGroupItemData mMenuSideGroupItemData;

    @Override
    public List<?> getChildItemList() {
        return mSubList;
    }

    @Override
    public boolean isExpanded() {
        return mExpanded;
    }

    @Override
    public void setExpanded(boolean isExpanded) {
        mExpanded = isExpanded;
    }

}