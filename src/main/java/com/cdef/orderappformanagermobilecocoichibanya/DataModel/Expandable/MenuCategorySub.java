package com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable;

import com.cdef.commonmodule.dataModel.MenuItemData;
import com.zaihuishou.expandablerecycleradapter.model.ExpandableListItem;

import java.util.List;

/**
 * Created by kimjinmo on 2018. 4. 23..
 */

public class MenuCategorySub implements ExpandableListItem {

    public boolean mExpanded = false;
    public MenuItemData mMenuItem;

    @Override
    public List<?> getChildItemList() {
        return null;
    }

    @Override
    public boolean isExpanded() {
        return mExpanded;
    }

    @Override
    public void setExpanded(boolean isExpanded) {
        mExpanded = isExpanded;
    }

}