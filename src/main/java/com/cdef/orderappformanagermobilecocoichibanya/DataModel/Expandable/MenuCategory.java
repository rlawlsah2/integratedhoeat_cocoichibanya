package com.cdef.orderappformanagermobilecocoichibanya.DataModel.Expandable;

import com.cdef.commonmodule.dataModel.OrderCategoryData;
import com.zaihuishou.expandablerecycleradapter.model.ExpandableListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kimjinmo on 2018. 4. 23..
 */

public class MenuCategory implements ExpandableListItem {

    public boolean mExpanded = false;
    public OrderCategoryData.CategoryItem mCategoryItem;
    public List<MenuCategorySub> mSubList = new ArrayList<>();

    @Override
    public List<?> getChildItemList() {
        return mSubList;
    }

    @Override
    public boolean isExpanded() {
        return mExpanded;
    }

    @Override
    public void setExpanded(boolean isExpanded) {
        mExpanded = isExpanded;
    }
 
}