package com.cdef.orderappformanagermobilecocoichibanya.Utils;

import com.cdef.commonmodule.BuildConfig;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.commonmodule.Utils.Utils;
import com.cdef.orderappformanagermobilecocoichibanya.OrderAppApplication;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by kimjinmo on 2017. 11. 7..
 */

public class MSSQLUtils {


    String ip = "cdefinition.ddns.net";

    private static MSSQLUtils instance;
    private Connection conn;

    private static String lastConnection = null;
    private StringBuilder orderLog = null;

    private PreparedStatement preparedStatement_select_STBL_CD, preparedStatement_update_IF_SALE_ACCNT, preparedStatement_insert_IF_SALE_ACCNT, preparedStatement_select_IFSC_REMARK, preparedStatement_insert_IF_SALE_CMDT, preparedStatement_insert_IF_SALE_CMDT_SET, preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT, preparedStatement_select_IFSC_NO, preparedStatement_select_SA_CALC_MK;

    private ResultSet resultset_select_STBL_CD,
            resultset_update_IF_SALE_ACCNT,
            resultset_insert_IF_SALE_ACCNT,
            resultset_select_IFSC_REMARK,
            resultset_insert_IF_SALE_CMDT,
            resultset_insert_IF_SALE_CMDT_SET,
            resultset_select_IFSA_NO_AND_IFSA_SDA_DT,
            resultset_select_IFSC_NO,
            resultset_select_SA_CALC_MK;


    private static final String query_select_STBL_CD = "SELECT STBL_CD from STR_TABLE WHERE STBL_NM = ?";
    private static final String query_update_IF_SALE_ACCNT =
            "UPDATE IF_SALE_ACCNT SET "
                    + "IFSA_RCV_MK = CASE WHEN IFSA_RCV_MK = 'N' THEN 'N' ELSE 'A' END, IFSA_AMT_TTL = IFSA_AMT_TTL + ?, CHG_USERID = ?, IFSA_CHG_DT = Convert(varchar(10), Getdate(),112) + Replace(Convert(varchar(8), Getdate(),108),':',''), "
                    + "IFSA_UP_DT = Convert(varchar(10), Getdate(),112) + Replace(Convert(varchar(8), Getdate(),108),':',''), STBL_CD = ? "
                    + "WHERE IFSA_TP = 'TBL' and IFSA_ORDER_ID = ?";

    private static final String query_insert_IF_SALE_ACCNT =
            "INSERT INTO IF_SALE_ACCNT "
                    + "(IFSA_RCV_MK, IFSA_TP, IFSA_SDA_DT, IFSA_NO, IFSA_UP_DT, IFSA_UP_MK, IFSA_DEL_MK, IFSA_AMT_TTL, IFSA_CALC_MK, IFSA_ORD_TP, USERID, IFSA_RGST_DT, CHG_USERID, IFSA_CHG_DT, IFSA_ORDER_ID, STBL_CD, IFSA_GUEST_M,IFSA_GUEST_F) "
                    + " SELECT ? IFSA_RCV_MK, ? IFSA_TP, Convert(varchar(10), Getdate(),112) IFSA_SDA_DT"
                    + ", ISNULL(MAX(IFSA_NO), 0)+1 IFSA_NO, Convert(varchar(10), Getdate(),112) + Replace(Convert(varchar(8), Getdate(),108),':','') IFSA_UP_DT"
                    + ", ? IFSA_UP_MK, ? IFSA_DEL_MK, ? IFSA_AMT_TTL, ? IFSA_CALC_MK, ? IFSA_ORD_TP"
                    + ", ? USERID, Convert(varchar(10), Getdate(),112) + Replace(Convert(varchar(8), Getdate(),108),':','') IFSA_RGST_DT"
                    + ", ? CHG_USERID, Convert(varchar(10), Getdate(),112) + Replace(Convert(varchar(8), Getdate(),108),':','') IFSA_CHG_DT"
                    + ", ? IFSA_ORDER_ID, ? STBL_CD  "
                    + ", ? IFSA_GUEST_M, ? IFSA_GUEST_F "  /*   여기에 내국인 외국인 값 넣어주면 됨   */
                    + "FROM IF_SALE_ACCNT WHERE IFSA_SDA_DT = Convert(varchar(10), Getdate(),112) ";


    private static final String query_select_IFSC_REMARK =
            "select ISNULL(MAX(CASE WHEN ISNUMERIC(IFSC_REMARK) = 0 THEN 0 ELSE CONVERT(INT, IFSC_REMARK) END), 0) +1"
                    + " from IF_SALE_ACCNT A"
                    + " LEFT JOIN IF_SALE_CMDT B"
                    + " ON A.IFSA_NO = B.IFSA_NO and A.IFSA_SDA_DT = B.IFSA_SDA_DT"
                    + " and A.IFSA_TP = B.IFSA_TP WHERE A.IFSA_ORDER_ID = ?";

    private static final String query_insert_IF_SALE_CMDT =
            "INSERT INTO IF_SALE_CMDT"
                    + " (IFSC_REMARK ,IFSA_TP, IFSA_SDA_DT, IFSA_NO, IFSC_NO, IFSC_CMDT_CD, IFSC_DEL_MK, IFSC_QTY, IFSC_UNIT_COST, IFSC_AMT_TTL, USERID, IFSC_RGST_DT, CHG_USERID, IFSC_CHG_DT, IFSC_CMDT_NM, IFSC_SC_TP) "
                    + " select top 1 ?, A.IFSA_TP, A.IFSA_SDA_DT, A.IFSA_NO, ISNULL(IFSC_NO, 0) + 1, ?, ?, ?, ?, ?"
                    + " , ?,  Convert(varchar(10), Getdate(),112) + Replace(Convert(varchar(8), Getdate(),108),':',''), ?,  Convert(varchar(10), Getdate(),112) + Replace(Convert(varchar(8), Getdate(),108),':',''), ? ,?"
                    + " from IF_SALE_ACCNT A"
                    + " LEFT JOIN IF_SALE_CMDT B"
                    + " ON A.IFSA_NO = B.IFSA_NO"
                    + " and A.IFSA_SDA_DT = B.IFSA_SDA_DT"
                    + " and A.IFSA_TP = B.IFSA_TP"
                    + " WHERE A.IFSA_ORDER_ID = ?"
                    + " order by IFSC_NO desc";

    private static final String query_select_SA_CALC_MK =
            "SELECT B.SA_CALC_MK "
                    + "FROM IF_SALE_ACCNT A, "
                    + "SALE_ACCNT B "
                    + "WHERE A.STR_NO = B.STR_NO "
                    + "AND A.SDA_DT = B.SDA_DT "
                    + "AND A.SA_NO = B.SA_NO "
                    + "AND A.IFSA_ORDER_ID = ? ";

    private static final String query_insert_IF_SALE_CMDT_SET =
            "INSERT INTO IF_SALE_CMDT_SET"
                    + " (IFSA_TP, IFSA_SDA_DT, IFSA_NO, IFSC_NO, IFCMDTS_CD, IFCMDT_CD, IFSCS_QTY, IFSCS_UNIT_COST, USERID, IFSCS_RGST_DT) "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, Getdate())";


    private static final String query_select_IFSA_NO_AND_IFSA_SDA_DT =
            "SELECT IFSA_NO, IFSA_SDA_DT " +
                    "FROM IF_SALE_ACCNT " +
                    "WHERE IFSA_ORDER_ID = ?";
    private static final String query_select_IFSC_NO =
            "SELECT TOP 1 IFSC_NO " +
                    "FROM IF_SALE_CMDT " +
                    "WHERE IFSA_NO = ? " +
                    "AND IFSA_SDA_DT = ? " +
                    "ORDER BY IFSC_NO DESC";


    private MSSQLUtils() {
        if (this.conn == null) {

            this.conn = CONN();
        }
        if (orderLog == null)
            this.orderLog = new StringBuilder();
    }

    public synchronized static MSSQLUtils getInstance() {
        if (instance == null) {
            instance = new MSSQLUtils();
        }
        return instance;
    }

    public String orderType = "H";//  H:홀(기본값) / D:배달 / P:포장


    private synchronized Connection CONN() {
        conn = null;
        String ConnURL = null;
        try {

            ///pos ip를 찾을 수 없는경
            if (LoginUtil.getInstance(OrderAppApplication.mContext).getsPosIp() == null) {
                new Exception("no posip");
            }


            Class.forName(BuildConfig.MSSQL_CLASS);
            ConnURL = "jdbc:jtds:sqlserver://" + LoginUtil.getInstance(OrderAppApplication.mContext).getsPosIp() + ";"
                    + "databaseName=" + BuildConfig.MSSQL_DB + ";user=" + BuildConfig.MSSQL_USERNAME + ";password="
                    + BuildConfig.MSSQL_PASSWORD + ";loginTimeout=5;socketTimeout=5";
            LogUtil.d("mssql connection url은 : " + ConnURL);

            MSSQLUtils.lastConnection = LoginUtil.getInstance(OrderAppApplication.mContext).getsPosIp().replaceAll(".", "-") + "@@" + BuildConfig.MSSQL_DB + "@@" + BuildConfig.MSSQL_USERNAME;
            conn = DriverManager.getConnection(ConnURL);

            /***
             * preparedStatement 객체 초기화
             * */
            ///1. SELECT STBL_CD
//            this.preparedStatement_select_STBL_CD = conn.prepareStatement(query_select_STBL_CD);
//            ///2. UPDATE IF_SALE_ACCNT
//            this.preparedStatement_update_IF_SALE_ACCNT = conn.prepareStatement(query_update_IF_SALE_ACCNT);
//            ///3. INSERT IF_SALE_ACCNT
//            this.preparedStatement_insert_IF_SALE_ACCNT = conn.prepareStatement(query_insert_IF_SALE_ACCNT);
//            ///4. select IFSC_REMARK
//            this.preparedStatement_select_IFSC_REMARK = conn.prepareStatement(query_select_IFSC_REMARK);
//            ///5. insert IF_SALE_CMDT
//            this.preparedStatement_insert_IF_SALE_CMDT = conn.prepareStatement(query_insert_IF_SALE_CMDT);
//            ///6. select SA_CALC_MK
//            this.preparedStatement_select_SA_CALC_MK = conn.prepareStatement(query_select_SA_CALC_MK);


            LogUtil.d("mssql connection ok");
        } catch (SQLException se) {
            LogUtil.d("mssql connection fail1  : " + se);
            conn = null;
        } catch (ClassNotFoundException e) {
            LogUtil.d("mssql connection fail2  : " + e);
            conn = null;

        } catch (Exception e) {
            LogUtil.d("mssql connection fail3  : " + e);
            conn = null;
        }
        return conn;
    }

    public Boolean checkConnection() {
        return (this.CONN() != null);
    }

    /***
     * DB STBL_CD를 가져옴
     * **/
    public String getUserID(String tableNo) {
        String userID = null;

        LogUtil.d("sql getUserID : " + userID);
        LogUtil.d("sql getUserID conn: " + conn);
        try {
            if (conn == null) {
                ///애초에 conn이 null이면 문제가 있다는 소리임.
//                userID = "[error] failed to connect : " + MSSQLUtils.lastConnection;
//                return userID;
                this.CONN();
            }
            if (conn.isClosed()) //닫힌거면 초기화 해버리고 새로 갱신해
            {
                this.conn = null;
                this.CONN();
            }
            this.preparedStatement_select_STBL_CD = this.conn.prepareStatement(query_select_STBL_CD);
            //preparedStatement set query data
//            this.preparedStatement_select_STBL_CD.clearParameters();
            this.preparedStatement_select_STBL_CD.setString(1, tableNo);
            this.resultset_select_STBL_CD = this.preparedStatement_select_STBL_CD.executeQuery();
            while (this.resultset_select_STBL_CD.next()) {
                userID = this.resultset_select_STBL_CD.getString(1);
                LogUtil.d("sql getUserID1 : " + userID);
            }
        } catch (Exception e) {
            LogUtil.d("error getUserID : " + e);
            userID = null;
        } finally {
            release_select_STBL_CD();
        }


        ///혹시 한번 실패한 경우 한번만 더 실행할것.
        if (userID == null) {
            try {
                if (conn == null) {
                    ///애초에 conn이 null이면 문제가 있다는 소리임.
//                    userID = "[error] failed to connect : " + MSSQLUtils.lastConnection;
//                    return userID;
                    this.CONN();
                }
                if (conn.isClosed()) //닫힌거면 초기화 해버리고 새로 갱신해
                {
                    this.conn = null;
                    this.CONN();
                }
                this.preparedStatement_select_STBL_CD = this.conn.prepareStatement(query_select_STBL_CD);

                //개선 후
                this.preparedStatement_select_STBL_CD.setString(1, tableNo);
                this.resultset_select_STBL_CD = this.preparedStatement_select_STBL_CD.executeQuery();


                while (this.resultset_select_STBL_CD.next()) {
                    userID = this.resultset_select_STBL_CD.getString(1);
                    LogUtil.d("sql getUserID2 : " + userID);
                }

                if (this.preparedStatement_select_STBL_CD != null)
                    this.preparedStatement_select_STBL_CD.clearParameters();

            } catch (Exception e) {
                userID = null;
                LogUtil.d("error getUserID : " + e);
            } finally {
                release_select_STBL_CD();
            }
        }

        LogUtil.d("sql getUserID3 : " + userID);
        return userID;
    }

    public int insertOrder_(int price, String tableNo, String tableType, boolean isFirstOrder, HashMap<String, Order> order, String guest_m, String guest_f) {
        int result = 1;
        this.orderLog.setLength(0);
        this.orderLog.append("orderstart__");

        this.orderType = tableType;

        try {
            if (this.conn == null)
                CONN();

            orderLog.append("dbConnection: ");
            if (this.conn != null)
                orderLog.append(conn.isClosed());
            else
                orderLog.append("null");
            orderLog.append("__________");
            if (conn.isClosed()) //닫힌거면 초기화 해버리고 새로 갱신해
            {
                this.conn = null;
                this.CONN();
            }
            if (this.conn == null) {
                result = -3;
            }
        } catch (SQLException e) {
            result = -4;
        }

        ///conn ok
        if (result == 1) {
            ///orderID가 존재하는지 체크

//            if(LoginUtil.getInstance(OrderAppApplication.mContext) != null && LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID() != null)
            if (!isFirstOrder) {
                String checker = CheckOrder(LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID());
                if (checker == null || !checker.equals("Y")) {
                    orderLog.append("insertType1__________");

                    try {

                        ///2. preparedstatement setting
                        this.preparedStatement_update_IF_SALE_ACCNT = this.conn.prepareStatement(query_update_IF_SALE_ACCNT);
//                        this.preparedStatement_update_IF_SALE_ACCNT.setString(1, String.valueOf(price));
//                        this.preparedStatement_update_IF_SALE_ACCNT.setString(2, tableNo);
//                        this.preparedStatement_update_IF_SALE_ACCNT.setString(3, tableNo);
//                        this.preparedStatement_update_IF_SALE_ACCNT.setString(4, LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID());

                        orderLog.append("query: ");
                        orderLog.append(
                                this.setPreparedStatement(this.preparedStatement_update_IF_SALE_ACCNT,
                                        new String[]{
                                                String.valueOf(price)
                                                , tableNo
                                                , tableNo,
                                                LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID()
                                        })
                        );
                        orderLog.append("__________");


                        ///0 옵션 처리를 위해 IFSA_NO를 먼저 가져온다
                        this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT = this.conn.prepareStatement(query_select_IFSA_NO_AND_IFSA_SDA_DT);
                        orderLog.append("query query_select_IFSA_NO_AND_IFSA_SDA_DT: ");
                        orderLog.append(
                                this.setPreparedStatement(this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT,
                                        new String[]{
                                                LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID()
                                        })
                        );
                        orderLog.append("__________");

                        ///1. 트랜젝션 on
                        this.conn.setAutoCommit(false); //자동 커밋을 막는다. 트랜잭션 시작

                        ///2. insertOrder
                        int resultSet1 = this.preparedStatement_update_IF_SALE_ACCNT.executeUpdate();
                        if (resultSet1 == 0) {
                            throw new Exception("poserror_UPDATE IF_SALE_ACCNT SET");
                        }

                        ///2.5 getIFSA_NO
                        this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT = this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT.executeQuery();
                        LogUtil.d("쿼리 실행 preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT : " + this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT.toString());

                        Integer ifsa_no = 0;
                        String ifsa_sda_dt = "";

                        while (this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.next()) {
                            ifsa_no = this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.getInt("IFSA_NO");
                            ifsa_sda_dt = this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.getString("IFSA_SDA_DT");
                        }
                        LogUtil.d("쿼리 결과 ifsa_no : " + ifsa_no);
                        LogUtil.d("쿼리 결과 ifsa_sda_dt : " + ifsa_sda_dt);

                        ///3. insertMenu
                        this.InsertMenuRoot(ifsa_no, ifsa_sda_dt, order, LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID());

                        ///4. 트랜젝션 off
                        this.conn.commit();
                        this.conn.setAutoCommit(true);
//                    LoginUtil.getInstance(OrderAppApplication.mContext).setmOrderIFSC_REMARK(IFSC_REMARTK + 1);
                        result = 1;

                    } catch (Exception e) {
                        orderLog.append("error: ").append(e.getMessage());
                        orderLog.append("__________");
                        orderLog.append("error_statckTrace: ").append(Utils.makeStackTrace(e));
                        orderLog.append("__________");

                        try {
                            conn.rollback();
                        } catch (SQLException e1) {

                            orderLog.append("error: (rollback fail) ").append(e1.getMessage());
                            orderLog.append("__________");
                            orderLog.append("error_statckTrace: (rollback fail)").append(Utils.makeStackTrace(e1));
                            orderLog.append("__________");

                            result = -2;

                        }
                        result = -2;
                    } finally {
                        release_update_IF_SALE_ACCNT();
                        release_select_IFSC_REMARK();
                        release_insert_IF_SALE_CMDT();
                    }
                } else {
                    orderLog.append("insertType2__________");

                    String orderId = LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID();//initOrderId();
                    try {

                        ///2. preparedstatement setting
                        this.preparedStatement_insert_IF_SALE_ACCNT = this.conn.prepareStatement(query_insert_IF_SALE_ACCNT);
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(1, "N");
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(2, "TBL");
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(3, "Y");
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(4, "Y");
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(5, String.valueOf(price));
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(6, "N");
//
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(7, "M");
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(8, getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()));
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(9, getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()));
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(10, orderId);
//                    this.preparedStatement_insert_IF_SALE_ACCNT.setString(11, getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()));

                        orderLog.append("query: ");
                        orderLog.append(
                                this.setPreparedStatement(this.preparedStatement_insert_IF_SALE_ACCNT,
                                        new String[]{
                                                "N"
                                                , "TBL"
                                                , "Y"
                                                , "Y"
                                                , String.valueOf(price)
                                                , "N"
                                                , orderType
                                                , getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo())
                                                , getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo())
                                                , orderId
                                                , getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo())
                                                , guest_m
                                                , guest_f
                                        })
                        );
                        orderLog.append("__________");


                        ///0 옵션 처리를 위해 IFSA_NO를 먼저 가져온다
                        this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT = this.conn.prepareStatement(query_select_IFSA_NO_AND_IFSA_SDA_DT);
                        orderLog.append("query preparedStatement_select_IFSC_NO: ");
                        orderLog.append(
                                this.setPreparedStatement(this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT,
                                        new String[]{
                                                LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID()
                                        })
                        );
                        orderLog.append("__________");

                        ///1. 트랜젝션 on
                        this.conn.setAutoCommit(false); //자동 커밋을 막는다. 트랜잭션 시작

                        ///2. insertOrder
                        this.resultset_insert_IF_SALE_ACCNT = this.preparedStatement_insert_IF_SALE_ACCNT.executeQuery();
                        LogUtil.d("쿼리 실행 preparedStatement_insert_IF_SALE_ACCNT : " + (this.preparedStatement_insert_IF_SALE_ACCNT).toString());
                        ///2.5 getIFSA_NO
                        this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT = this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT.executeQuery();
                        LogUtil.d("쿼리 실행 preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT : " + this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT.toString());

                        Integer ifsa_no = 0;
                        String ifsa_sda_dt = "";

                        while (this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.next()) {
                            ifsa_no = this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.getInt("IFSA_NO");
                            ifsa_sda_dt = this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.getString("IFSA_SDA_DT");
                        }
                        LogUtil.d("쿼리 결과 ifsa_no : " + ifsa_no);
                        LogUtil.d("쿼리 결과 ifsa_sda_dt : " + ifsa_sda_dt);

                        ///3. insertMenu
                        this.InsertMenuRoot(ifsa_no, ifsa_sda_dt, order, orderId);

                        ///4. 트랜젝션 off
                        this.conn.commit();
                        this.conn.setAutoCommit(true);
                        LoginUtil.getInstance(OrderAppApplication.mContext).setmOrderID(orderId);
                        result = 1;

                    } catch (Exception e) {
                        LogUtil.d("insertOrder_ Error : " + e.getMessage());
                        orderLog.append("error: ").append(e.getMessage());
                        orderLog.append("__________");

                        orderLog.append("error_statckTrace: ").append(Utils.makeStackTrace(e));
                        orderLog.append("__________");


                        try {
                            conn.rollback();
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                            LogUtil.d("InsertMenuRoot rollback도 실패 : " + e1.getMessage());

                            orderLog.append("error: (rollback fail) ").append(e1.getMessage());
                            orderLog.append("__________");

                            orderLog.append("error_statckTrace: (rollback fail)").append(Utils.makeStackTrace(e1));
                            orderLog.append("__________");

                            result = -2;

                        }
                        result = -2;
                    } finally {
                        release_insert_IF_SALE_ACCNT();
                        release_select_IFSC_REMARK();
                        release_insert_IF_SALE_CMDT();
                    }
                }
            } else {
                orderLog.append("insertType3__________");


                String orderId = LoginUtil.getInstance(OrderAppApplication.mContext).getmOrderID();//initOrderId();
                try {

                    ///2. preparedstatement setting
                    this.preparedStatement_insert_IF_SALE_ACCNT = this.conn.prepareStatement(query_insert_IF_SALE_ACCNT);
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(1, "N");
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(2, "TBL");
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(3, "Y");
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(4, "Y");
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(5, String.valueOf(price));
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(6, "N");
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(7, "M");
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(8, getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()));
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(9, getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()));
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(10, orderId);
//                this.preparedStatement_insert_IF_SALE_ACCNT.setString(11, getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()));
                    orderLog.append("query: ");
                    orderLog.append(
                            this.setPreparedStatement(this.preparedStatement_insert_IF_SALE_ACCNT,
                                    new String[]{
                                            "N"
                                            , "TBL"
                                            , "Y"
                                            , "Y"
                                            , String.valueOf(price)
                                            , "N"
                                            , orderType
                                            , getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo())
                                            , getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo())
                                            , orderId
                                            , getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo())
                                            , guest_m
                                            , guest_f
                                    })
                    );
                    orderLog.append("__________");
                    ///0 옵션 처리를 위해 IFSA_NO를 먼저 가져온다
                    this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT = this.conn.prepareStatement(query_select_IFSA_NO_AND_IFSA_SDA_DT);
                    orderLog.append("query preparedStatement_select_IFSC_NO: ");
                    orderLog.append(
                            this.setPreparedStatement(this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT,
                                    new String[]{
                                            orderId
                                    })
                    );
                    orderLog.append("__________");

                    ///1. 트랜젝션 on
                    this.conn.setAutoCommit(false); //자동 커밋을 막는다. 트랜잭션 시작

                    ///2. insertOrder
                    this.preparedStatement_insert_IF_SALE_ACCNT.executeUpdate();

                    ///2.5 getIFSA_NO
                    this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT = this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT.executeQuery();
                    LogUtil.d("쿼리 실행 preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT : " + this.preparedStatement_select_IFSA_NO_AND_IFSA_SDA_DT.toString());

                    Integer ifsa_no = 0;
                    String ifsa_sda_dt = "";

                    while (this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.next()) {
                        ifsa_no = this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.getInt("IFSA_NO");
                        ifsa_sda_dt = this.resultset_select_IFSA_NO_AND_IFSA_SDA_DT.getString("IFSA_SDA_DT");
                    }
                    LogUtil.d("쿼리 결과 ifsa_no : " + ifsa_no);
                    LogUtil.d("쿼리 결과 ifsa_sda_dt : " + ifsa_sda_dt);


                    ///3. insertMenu
                    this.InsertMenuRoot(ifsa_no, ifsa_sda_dt, order, orderId);

                    ///4. 트랜젝션 off
                    this.conn.commit();
                    LogUtil.d("트랜젝션 성공2 : " + result);
                    this.conn.setAutoCommit(true);
                    LogUtil.d("트랜젝션 성공3 : " + result);
                    LoginUtil.getInstance(OrderAppApplication.mContext).setmOrderID(orderId);
                    result = 1;
                    LogUtil.d("트랜젝션 성공1 : " + result);

                } catch (Exception e) {
                    LogUtil.d("insertOrder_ Error : " + e.getMessage());
                    orderLog.append("error: ").append(e.getMessage());
                    orderLog.append("__________");

                    orderLog.append("error_statckTrace: ").append(Utils.makeStackTrace(e));
                    orderLog.append("__________");


                    try {
                        conn.rollback();
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                        LogUtil.d("InsertMenuRoot rollback도 실패 : " + e1.getMessage());

                        orderLog.append("error: (rollback fail) ").append(e1.getMessage());
                        orderLog.append("__________");
                        orderLog.append("error_statckTrace: (rollback fail)").append(Utils.makeStackTrace(e1));
                        orderLog.append("__________");

                        result = -2;

                    }
                    result = -2;
                } finally {
                    release_insert_IF_SALE_ACCNT();
                    release_select_IFSC_REMARK();
                    release_insert_IF_SALE_CMDT();
                }
            }
        }


        if (result == 1) {
            //성공
            this.orderLog.insert(0, "_SUCCESS___________");
        } else {
            //실페ㅐ
            this.orderLog.insert(0, "_FAIL___________");
        }

        ///커넥션을 닫아버리자.
        try {
            if (this.conn != null && !this.conn.isClosed()) {
                this.conn.close();
                this.orderLog.append("_________close ok");
            }
        } catch (SQLException e) {
            this.orderLog.append("_________close fail");
        }


//        this.conn.isValid()
        FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(null).getmBrandName())
                .child("queryLog")
                .child(LoginUtil.getInstance(OrderAppApplication.mContext).branchInfo.branchId)
                .child(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo())
                .child(Utils.getDate("yyyy-MM-dd HH:mm:ss"))
                .setValue(Utils.removeInvalidChars(orderLog.toString()));

        LogUtil.d("주문관련 로그찍기 : " + Utils.removeInvalidChars(orderLog.toString()));

        return result;
    }

    /***
     * 메뉴 집어 넣기
     * **/
    private void InsertMenuRoot(int ifsa_no, String ifsa_sda_dt, HashMap<String, Order> order, String orderId) throws SQLException {
        LogUtil.d("InsertMenuRoot $$$$");
        this.orderLog.append("InsertMenu__________");
        int result = 0;

        ///preparedStatement update
        this.preparedStatement_select_IFSC_REMARK = this.conn.prepareStatement(query_select_IFSC_REMARK);

        this.orderLog.append("query: ")
                .append(this.setPreparedStatement(this.preparedStatement_select_IFSC_REMARK, new String[]{
                        orderId
                }))
                .append("__________");

        this.resultset_select_IFSC_REMARK = this.preparedStatement_select_IFSC_REMARK.executeQuery();
        LogUtil.d("쿼리 실행 preparedStatement_select_IFSC_REMARK : " + this.preparedStatement_select_IFSC_REMARK.toString());

        Integer resultString = 0;

        while (this.resultset_select_IFSC_REMARK.next()) {
            resultString = this.resultset_select_IFSC_REMARK.getInt(1);
        }

        String orderQuantity = null;
        String orderPrice = null;
        String orderName = null;
        String orderCMDTCD = null;


        /////sub inserMenu에서 사용할 statement를 여기서 초기화 하자.
        this.preparedStatement_insert_IF_SALE_CMDT = this.conn.prepareStatement(query_insert_IF_SALE_CMDT);
        //밑에 두개는 옵션 넣을때 쓸거
        this.preparedStatement_select_IFSC_NO = this.conn.prepareStatement(query_select_IFSC_NO);
        this.preparedStatement_insert_IF_SALE_CMDT_SET = this.conn.prepareStatement(query_insert_IF_SALE_CMDT_SET);


        for (String key : order.keySet()) {
            orderQuantity = String.valueOf(order.get(key).quantity);
            orderPrice = String.valueOf(order.get(key).price);
            orderName = order.get(key).foodName;
            orderCMDTCD = order.get(key).CMDTCD;
            LogUtil.d("InsertMenuRoot $$$$ orderQuantity : " + orderQuantity);
            LogUtil.d("InsertMenuRoot $$$$ orderPrice: " + orderPrice);
            LogUtil.d("InsertMenuRoot $$$$ orderName: " + orderName);
            LogUtil.d("InsertMenuRoot $$$$ orderCMDTCD: " + orderCMDTCD);

            if (orderQuantity != null && orderPrice != null && orderName != null && orderCMDTCD != null) {
                this.insertMenu(order.get(key), ifsa_sda_dt, ifsa_no, orderId, orderName, orderPrice, orderQuantity, orderCMDTCD, String.valueOf(resultString));
            }
        }
    }

    private void insertMenu(Order menu, String ifsa_sda_dt, int ifsa_no, String orderId, String name, String price, String quantity, String CMDTCD, String IFSC_REMARK) throws SQLException {
        LogUtil.d("insertMenu @@@@@");
        this.orderLog.append("InsertMenuRoot detail__________");


        int totalPrice = 0;

        try {
            int iPrice = Integer.parseInt(price);
            int iQuntity = Integer.parseInt(quantity);
            totalPrice = iPrice * iQuntity;

        } catch (Exception e) {

        }

        this.orderLog.append("query: ")
                .append(this.setPreparedStatement(this.preparedStatement_insert_IF_SALE_CMDT, new String[]{
                        IFSC_REMARK,
                        CMDTCD,
                        "L",
                        quantity,
                        price,
                        String.valueOf(totalPrice),
                        getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()),
                        getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()),
                        name,
                        orderType,
                        orderId
                }))
                .append("__________");
//        this.preparedStatement_insert_IF_SALE_CMDT.addBatch();
        this.preparedStatement_insert_IF_SALE_CMDT.executeUpdate();


        ///옵션 처리를 위해 IFSC_NO를 가져오자
        this.orderLog.append("query preparedStatement_select_IFSC_NO : ")
                .append(this.setPreparedStatement(this.preparedStatement_select_IFSC_NO, new String[]{
                        ifsa_no + "",
                        ifsa_sda_dt
                }))
                .append("__________");

        this.resultset_select_IFSC_NO = this.preparedStatement_select_IFSC_NO.executeQuery();
        LogUtil.d("쿼리 실행 preparedStatement_select_IFSC_NO : " + this.preparedStatement_select_IFSC_NO.toString());

        int ifsc_no = 0;

        while (this.resultset_select_IFSC_NO.next()) {
            ifsc_no = this.resultset_select_IFSC_NO.getInt("IFSC_NO");
        }
        /////sub inserMenuOptions에서 사용할 statement를 여기서 초기화 하자.


        if (menu.options != null && menu.options.size() > 0) {
            for (String optionKey : menu.options.keySet()) {
                this.insertMenuOptions(ifsa_sda_dt, ifsa_no, ifsc_no, menu.CMDTCD, menu.options.get(optionKey));
            }
        }


        this.preparedStatement_insert_IF_SALE_CMDT.clearParameters();
        this.preparedStatement_select_IFSC_NO.clearParameters();
        LogUtil.d("쿼리 실행 preparedStatement_insert_IF_SALE_CMDT : " + this.preparedStatement_insert_IF_SALE_CMDT.toString());
    }

    private void insertMenuOptions(String ifsa_sda_dt, int ifsa_no, int ifsc_no, String menuCMDTCD, Order option) throws SQLException {
        LogUtil.d("insertMenuOptions @@@@@");
        this.orderLog.append("insertMenuOptions detail__________");



        this.orderLog.append("query: ")
                .append(this.setPreparedStatement(this.preparedStatement_insert_IF_SALE_CMDT_SET, new String[]{
                        "TBL",
                        ifsa_sda_dt,
                        ifsa_no + "",
                        ifsc_no + "",
                        option.CMDTCD,  ///옵션의 CMDTCD
                        menuCMDTCD,
                        option.quantity + "",
                        option.price + "",
                        LoginUtil.getInstance(null).branchInfo.branchId
                }))
                .append("__________");
//        this.preparedStatement_insert_IF_SALE_CMDT.addBatch();
        this.preparedStatement_insert_IF_SALE_CMDT_SET.executeUpdate();
        this.preparedStatement_insert_IF_SALE_CMDT_SET.clearParameters();
        LogUtil.d("쿼리 실행 preparedStatement_insert_IF_SALE_CMDT_SET : " + this.preparedStatement_insert_IF_SALE_CMDT_SET.toString());
    }


    /***
     * Log 찍기
     * **/
    private String setPreparedStatement(PreparedStatement preparedStatement, String[] input) throws SQLException {

        String log = "";

        for (int i = 1; i <= input.length; i++) {
            preparedStatement.setString(i, input[i - 1]);
            log += input[i - 1];
            log += ", ";
        }
        return log;
    }


//    public String initOrderId()
//    {
//        String orderId = "M" + LoginUtil.getInstance(OrderAppApplication.mContext).getsBranchUid() + "_" + getUserID(LoginUtil.getInstance(OrderAppApplication.mContext).getmSelectedTableNo()) + "_" + Utils.getDate("MMddHHmmss");
//        return orderId;
//    }

    ///현재 주문 코드를 가지고
///YN 체크, 계산 여부.
//    GetData.IFSA_ORDER_ID?
    public String CheckOrder(String orderID) {

        String result = null;

        try {

//            preparedStatement_select_SA_CALC_MK.setString(1, orderID);
            LogUtil.d("CheckOrder conn.isClose : " + conn.isClosed());
            if (conn.isClosed()) //닫힌거면 초기화 해버리고 새로 갱신해
            {
                this.conn = null;
                this.CONN();
            }
            this.preparedStatement_select_SA_CALC_MK = this.conn.prepareStatement(query_select_SA_CALC_MK);

            LogUtil.d("쿼리 실행 preparedStatement_select_SA_CALC_MK : " + this.setPreparedStatement(this.preparedStatement_select_SA_CALC_MK, new String[]{
                    orderID
            }));

            this.resultset_select_SA_CALC_MK = preparedStatement_select_SA_CALC_MK.executeQuery();

            while (this.resultset_select_SA_CALC_MK.next()) {
                result = this.resultset_select_SA_CALC_MK.getString(1);
            }


        } catch (Exception e) {
            LogUtil.d("CheckOrder Error : " + e.getMessage());
            result = null;
        } finally {
            release_select_SA_CALC_MK();
        }
        LogUtil.d("CheckOrder 결과 : " + result);
        return result;
    }


    /***
     * 릴르즈 필요한 모든 객체를 객체별로 해제할 수 있는 메소드를 작성한다.
     * preparedStatement와 ResultSet을 세트로 한다.
     * **/
    public void release_all() {
        release_insert_IF_SALE_ACCNT();
        release_insert_IF_SALE_CMDT();
        release_select_IFSC_REMARK();
        release_select_SA_CALC_MK();
        release_select_STBL_CD();
        release_update_IF_SALE_ACCNT();
//        if(MSSQLUtils.getInstance() != null)
//            clear_instance();
    }

    private void clear_instance() {
        MSSQLUtils.instance = null;
    }

    private void release_insert_IF_SALE_ACCNT() {
        if (this.preparedStatement_insert_IF_SALE_ACCNT != null) try {
            this.preparedStatement_insert_IF_SALE_ACCNT.close();
        } catch (SQLException ex) {
        }
        if (this.resultset_insert_IF_SALE_ACCNT != null) try {
            this.resultset_insert_IF_SALE_ACCNT.close();
        } catch (SQLException ex) {
        }
    }

    private void release_insert_IF_SALE_CMDT() {
        if (this.preparedStatement_insert_IF_SALE_CMDT != null) try {
            this.preparedStatement_insert_IF_SALE_CMDT.close();
        } catch (SQLException ex) {
        }
        if (this.resultset_insert_IF_SALE_CMDT != null) try {
            this.resultset_insert_IF_SALE_CMDT.close();
        } catch (SQLException ex) {
        }
    }

    private void release_select_STBL_CD() {
        if (this.preparedStatement_select_STBL_CD != null) try {
            this.preparedStatement_select_STBL_CD.close();
        } catch (SQLException ex) {
        }
        if (this.resultset_select_STBL_CD != null) try {
            this.resultset_select_STBL_CD.close();
        } catch (SQLException ex) {
        }
    }

    private void release_select_SA_CALC_MK() {
        if (this.preparedStatement_select_SA_CALC_MK != null) try {
            this.preparedStatement_select_SA_CALC_MK.close();
        } catch (SQLException ex) {
        }
        if (this.resultset_select_SA_CALC_MK != null) try {
            this.resultset_select_SA_CALC_MK.close();
        } catch (SQLException ex) {
        }
    }

    private void release_select_IFSC_REMARK() {
        if (this.preparedStatement_select_IFSC_REMARK != null) try {
            this.preparedStatement_select_IFSC_REMARK.close();
        } catch (SQLException ex) {
        }
        if (this.resultset_select_IFSC_REMARK != null) try {
            this.resultset_select_IFSC_REMARK.close();
        } catch (SQLException ex) {
        }
    }

    private void release_update_IF_SALE_ACCNT() {
        if (this.preparedStatement_update_IF_SALE_ACCNT != null) try {
            this.preparedStatement_update_IF_SALE_ACCNT.close();
        } catch (SQLException ex) {
        }
        if (this.resultset_update_IF_SALE_ACCNT != null) try {
            this.resultset_update_IF_SALE_ACCNT.close();
        } catch (SQLException ex) {
        }
    }


    public void log(String point) {
        LogUtil.d("---------------------------[" + point + "]-------------------------------");
        LogUtil.d("---------------------------preparedStatement-------------------------------");
        LogUtil.d("this.preparedStatement_insert_IF_SALE_ACCNT : " + this.preparedStatement_insert_IF_SALE_ACCNT);
        LogUtil.d("this.preparedStatement_select_IFSC_REMARK : " + this.preparedStatement_select_IFSC_REMARK);
        LogUtil.d("this.preparedStatement_update_IF_SALE_ACCNT : " + this.preparedStatement_update_IF_SALE_ACCNT);
        LogUtil.d("this.preparedStatement_select_STBL_CD : " + this.preparedStatement_select_STBL_CD);
        LogUtil.d("this.preparedStatement_insert_IF_SALE_CMDT : " + this.preparedStatement_insert_IF_SALE_CMDT);
        LogUtil.d("this.preparedStatement_select_SA_CALC_MK : " + this.preparedStatement_select_SA_CALC_MK);

        LogUtil.d("---------------------------resultset-------------------------------");
        LogUtil.d("this.resultset_insert_IF_SALE_ACCNT : " + this.resultset_insert_IF_SALE_ACCNT);
        LogUtil.d("this.resultset_select_IFSC_REMARK : " + this.resultset_select_IFSC_REMARK);
        LogUtil.d("this.resultset_update_IF_SALE_ACCNT : " + this.resultset_update_IF_SALE_ACCNT);
        LogUtil.d("this.resultset_select_STBL_CD : " + this.resultset_select_STBL_CD);
        LogUtil.d("this.resultset_insert_IF_SALE_CMDT : " + this.resultset_insert_IF_SALE_CMDT);
        LogUtil.d("this.resultset_select_SA_CALC_MK : " + this.resultset_select_SA_CALC_MK);
    }


}
