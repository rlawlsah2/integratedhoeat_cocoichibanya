package com.cdef.orderappformanagermobilecocoichibanya.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.orderappformanagermobilecocoichibanya.View.Activity.DetailActivity;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.BillFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.CartListFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.InitPersonFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.LoginFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.SelectMenuFragment;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.SelectTableFragment;

/**
 * Created by kimjinmo on 2016. 11. 16..
 */

public class NavigationUtils {

    public static final String BACKSTACKID = "fragments";


    public static void removeAllFragment(FragmentManager fragmentManager) {
        fragmentManager.popBackStack();
    }


    /***
     * fragment를 새로열때(단, 돌아올 때 새로 로딩 안됨) + 애니메이션 처리 없이
     * **/
    public static void detailOpenFragment(FragmentManager fragmentManager, int layoutFragment, String openFragment) {

        Fragment currentFragment = fragmentManager.findFragmentById(layoutFragment);
        Fragment checkAlreadyIncludedFragment = fragmentManager.findFragmentByTag(openFragment);

        LogUtil.d("NavigationUtils detailOpenFragment : " + openFragment);
        LogUtil.d("NavigationUtils detailOpenFragment currentFragment : " + currentFragment);
        LogUtil.d("NavigationUtils detailOpenFragment checkAlreadyIncludedFragment : " + checkAlreadyIncludedFragment);

        Fragment newFragment = null;

        //열려고하는 fragment가 존재하지않으면 생성해야지
        if (checkAlreadyIncludedFragment == null) {
            if (openFragment.equals(LoginFragment.class.getSimpleName())) {
                newFragment = new LoginFragment();
            } else if (openFragment.equals(SelectTableFragment.class.getSimpleName())) {
                newFragment = new SelectTableFragment();
            }else if (openFragment.equals(SelectMenuFragment.class.getSimpleName())) {
                newFragment = new SelectMenuFragment();
            }
        }

        if (newFragment == null)
            return;

        if (currentFragment == null) //열린 화면이 하나도 없는 경우
        {
            fragmentManager.beginTransaction()
                    .add(layoutFragment, newFragment, openFragment)
                    .addToBackStack(NavigationUtils.BACKSTACKID)
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .commit();
        } else {
            if (checkAlreadyIncludedFragment == null)    //열린 화면은 존재하는데, 열려고 하는 화면이 manager에 없을때
            {
                removeAllFragment(fragmentManager);
                fragmentManager.beginTransaction()
                        .hide(currentFragment)
                        .add(layoutFragment, newFragment, openFragment)
                        .addToBackStack(NavigationUtils.BACKSTACKID)
                        .setTransition(FragmentTransaction.TRANSIT_NONE)
                        .commit();
            } else {
                if (currentFragment != checkAlreadyIncludedFragment)     //열린 화면이 존재하고, 열려고 하는 화면이 열린것과는 다른경우. 즉 back단에 있을때
                {
                    removeAllFragment(fragmentManager);
                    fragmentManager.beginTransaction()
                            .hide(currentFragment)
                            .add(layoutFragment, newFragment, openFragment)
                            .addToBackStack(NavigationUtils.BACKSTACKID)
                            .setTransition(FragmentTransaction.TRANSIT_NONE)
                            .commit();
                } else    //열린화면이 딱 한개인경우에 해당.
                {

                }
            }
        }
    }

    public static void templateOpenFragment(FragmentManager fragmentManager, int layoutFragment, String openFragment, Bundle bundle) {

        Fragment currentFragment = fragmentManager.findFragmentById(layoutFragment);
        Fragment checkAlreadyIncludedFragment = fragmentManager.findFragmentByTag(openFragment);

        LogUtil.d("NavigationUtils detailOpenFragment : " + openFragment);
        LogUtil.d("NavigationUtils detailOpenFragment currentFragment : " + currentFragment);
        LogUtil.d("NavigationUtils detailOpenFragment checkAlreadyIncludedFragment : " + checkAlreadyIncludedFragment);

        Fragment newFragment = null;
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        //열려고하는 fragment가 존재하지않으면 생성해야지
        if (checkAlreadyIncludedFragment == null) {


            if (openFragment.equals(LoginFragment.class.getSimpleName())) {
                newFragment = new LoginFragment();
            } else if (openFragment.equals(SelectTableFragment.class.getSimpleName())) {
                newFragment = new SelectTableFragment();
            } else if (openFragment.equals(CartListFragment.class.getSimpleName())) {
                newFragment = new CartListFragment();
//            } else if (openFragment.equals(HistoryFragment.class.getSimpleName())) {
//                newFragment = new HistoryFragment();
            } else if (openFragment.equals(InitPersonFragment.class.getSimpleName())) {
                newFragment = new InitPersonFragment();
            } else if (openFragment.equals(SelectMenuFragment.class.getSimpleName())) {
                newFragment = new SelectMenuFragment();
            } else if (openFragment.equals(BillFragment.class.getSimpleName())) {
                newFragment = new BillFragment();
            }

            if (bundle != null) {
                newFragment.setArguments(bundle);
            }
        } else {
            if (bundle != null) {
                checkAlreadyIncludedFragment.setArguments(bundle);
            }
        }

        if (checkAlreadyIncludedFragment == null && newFragment == null) {
            return;
        }

        if (currentFragment == null) //열린 화면이 하나도 없는 경우
        {
            transaction
                    .replace(layoutFragment, newFragment, openFragment)
//                    .add(layoutFragment, newFragment, openFragment)
                    .addToBackStack(NavigationUtils.BACKSTACKID)
                    .setTransition(FragmentTransaction.TRANSIT_NONE).commit();
        } else {

            if (checkAlreadyIncludedFragment == null) {
                transaction
//                        .hide(currentFragment)
                        .replace(layoutFragment, newFragment, openFragment)
//                        .add(layoutFragment, newFragment, openFragment)
                        .addToBackStack(NavigationUtils.BACKSTACKID)
                        .setTransition(FragmentTransaction.TRANSIT_NONE).commit();
            } else {
                transaction
                        .replace(layoutFragment, checkAlreadyIncludedFragment)
//                        .show(checkAlreadyIncludedFragment)
//                        .addToBackStack(NavigationUtils.BACKSTACKID)
                        .setTransition(FragmentTransaction.TRANSIT_NONE).commit();
            }


        }
    }

    public static void openDetailActivity(Context context, String openFragment, Bundle bundle) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtras(bundle);
        intent.putExtra("fragment", openFragment);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(0, 0);
    }
}
