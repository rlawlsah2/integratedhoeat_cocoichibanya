package com.cdef.orderappformanagermobilecocoichibanya.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.cdef.commonmodule.dataModel.LoginData;
import com.cdef.commonmodule.Utils.TabletSetting;
import com.cdef.orderappformanagermobilecocoichibanya.BuildConfig;
import com.cdef.orderappformanagermobilecocoichibanya.Networking.CocoichibanyaNetworkUtil;
import com.cdef.commonmodule.Repository.LoginRepository;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.orderappformanagermobilecocoichibanya.View.Fragment.SelectTableFragment;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class LoginViewModel extends BaseViewModel {


    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public MutableLiveData<LoginData> loginInfo = new MutableLiveData<LoginData>() {
    };
    public MutableLiveData<INITSETTING> initSetting = new MutableLiveData<INITSETTING>() {
    };
    private LoginRepository loginRepository;

    private String mStaffID = null;
    private String mStaffPW = null;

    public enum INITSETTING {
        NONE, LOADING, ERROR, COMPLETE
    }

    public LoginViewModel() {

    }

    public void initRepository(Context context) {
        this.loginRepository = new LoginRepository(CocoichibanyaNetworkUtil.getInstance(context).getService());
        this.initSetting.setValue(INITSETTING.NONE);

    }

    public void getMainCategoryList() {
        this.initSetting.setValue(INITSETTING.LOADING);
        this.loginRepository.initSettingForCOCO(SelectTableFragment.SPANCOUNT)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            LogUtil.d("로그인 스트림 subscribe success: " + response);

                        },
                        error -> {
                            LogUtil.d("로그인 스트림 subscribe error: " + error);
                            this.initSetting.setValue(INITSETTING.ERROR);

                        },
                        () -> {
                            TabletSetting.getInstance().setOrderMainCategoryList(loginRepository.mainCategories);

                            ///카테고리 셋팅
                            for (String key : loginRepository.subCategories.keySet()) {
                                TabletSetting.getInstance().setOrderSubCategoryList(key, loginRepository.subCategories.get(key));
                            }

                            ///메뉴 셋팅
                            TabletSetting.getInstance().setmAllMenusHashMap(loginRepository.allMenus);
                            LogUtil.d("loginviewmodel allmenulist : " + loginRepository.mAllMenuList.size());
                            TabletSetting.getInstance().addMenuList(loginRepository.mAllMenuList);


                            this.initSetting.setValue(INITSETTING.COMPLETE);

                        }
                );

    }

    public void getLoginInfo(String branchId, String pw) {
        this.state.setValue(STATE.LOADING);
        loginRepository.loginForManager(branchId, pw)
                .subscribe(
                        response -> {
                            LogUtil.d("로그인 modelView response : " + response.getValue());

                            mStaffID = branchId;
                            mStaffPW = pw;
                            loginInfo.setValue(response.getValue());
                            this.state.setValue(STATE.COMPLETE);

                        },
                        error -> {
                            LogUtil.d("로그인 modelView error : " + error);
                            loginInfo.setValue(null);
                            this.state.setValue(STATE.ERROR);

                        },
                        () -> {

                            LogUtil.d("로그인 modelView complete : ");

                        }
                );
    }

    public void saveDefaultLoginInfo(Context context) {
        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mStaffID);
        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mStaffPW);
//        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mBranchTableNo);

        if (this.mStaffID != null && this.mStaffPW != null) {
//            this.loginInfo.getValue().branchInfo.storeBranchUid = mStaffID;
            LoginUtil.getInstance(context).setLoginInfo(context, this.loginInfo.getValue().branchInfo.fbBrandName, this.mStaffID, this.mStaffPW, this.loginInfo.getValue().branchInfo.branchId, this.loginInfo.getValue().branchInfo.storeBranchUid, this.loginInfo.getValue().accessToken, this.loginInfo.getValue().branchInfo.posIp, this.loginInfo.getValue().branchInfo);
            this.getMainCategoryList();
        }

    }
}
