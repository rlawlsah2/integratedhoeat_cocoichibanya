package com.cdef.orderappformanagermobilecocoichibanya.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.cdef.orderappformanagermobilecocoichibanya.BuildConfig;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.Repository.OrderRepository;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.commonmodule.Utils.TabletSetting;
import com.cdef.commonmodule.Utils.Utils;
import com.cdef.orderappformanagermobilecocoichibanya.Networking.CocoichibanyaNetworkUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class OrderViewModel extends ViewModel {


    public MutableLiveData<ArrayList<MenuItemData>> menuList = new MutableLiveData<>();

    private OrderRepository orderRepository;

    public MutableLiveData<ADDCARTPROFRESS> addCartProgress = new MutableLiveData<>();

    public enum ADDCARTPROFRESS {
        NONE, LOADING, ERROR, COMPLETE
    }

    public OrderViewModel() {

    }

    public Observable goOrder(String url, String orderJson)
    {
        return this.orderRepository.order(url, orderJson)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public void initRepository(Context context) {
        this.orderRepository = new OrderRepository(CocoichibanyaNetworkUtil.getInstance(context).getService());
        this.addCartProgress.setValue(ADDCARTPROFRESS.NONE);
    }

    public void removeCart(String branchId, String tableNo, MenuItemData menuItemData) {

        this.addCartProgress.setValue(ADDCARTPROFRESS.LOADING);
        FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(null).getmBrandName())
                .child("tables")
                .child(branchId)
                .child(tableNo)
                .child("cart")
                .child("food_" + menuItemData.foodUid)
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        mutableData.setValue(null);
                        return Transaction.success(mutableData);

                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                        if (databaseError != null) {
                            addCartProgress.setValue(ADDCARTPROFRESS.ERROR);
                        } else {
                            addCartProgress.setValue(ADDCARTPROFRESS.COMPLETE);
                        }
                    }
                });
    }

    public void addCart(String branchId, String tableNo, MenuItemData menuItemData) {
        this.addCartProgress.setValue(ADDCARTPROFRESS.LOADING);
        FirebaseDatabase.getInstance().getReference()
                .child(LoginUtil.getInstance(null).getmBrandName())
                .child("tables")
                .child(branchId)
                .child(tableNo)
                .child("cart")
                .child("food_" + menuItemData.foodUid)
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        Order order = mutableData.getValue(Order.class);
                        if (order != null) {
                            int iQuantity = order.quantity;
                            iQuantity++;
                            order.quantity = iQuantity;
                            order.time = Utils.getDate("yy-MM-dd HH:mm:ss");

                            mutableData.setValue(order);

                        } else {
                            Order newOrder = new Order();
                            newOrder.foodUid = menuItemData.foodUid;
                            newOrder.foodName = menuItemData.foodName;
                            newOrder.CMDTCD = menuItemData.CMDTCD;
                            String count = "1";
                            int iCount = 0;
                            try {
                                iCount = Integer.parseInt(count);
                            } catch (NumberFormatException e) {
                                iCount = 1;
                            }
                            newOrder.isKitchen = menuItemData.isKitchen;
                            newOrder.orderedTableNo = tableNo;
                            newOrder.quantity = iCount;
                            newOrder.price = menuItemData.price;
                            newOrder.time = Utils.getDate("yy-MM-dd HH:mm:ss");
                            newOrder.categoryCode = menuItemData.tag;
                            newOrder.key = "food_" + menuItemData.foodUid;
                            if (TabletSetting.getInstance().getmOrderSubCategoryAllHashMap().get(menuItemData.tag) != null)
                                newOrder.categoryName = TabletSetting.getInstance().getmOrderSubCategoryAllHashMap().get(menuItemData.tag).categoryName;
                            mutableData.setValue(newOrder);
                        }
                        return Transaction.success(mutableData);

                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
//                        mCartListAdapter.notifyDataSetChanged();
//                        recyclerViewCart.scrollToPosition(0);
                        if (databaseError != null) {
                            addCartProgress.setValue(ADDCARTPROFRESS.ERROR);
                        } else {
                            addCartProgress.setValue(ADDCARTPROFRESS.COMPLETE);
                        }
                    }
                });
    }

    public void getMenuList(String categoryId, String storeBranchUid) {
        this.orderRepository.getMenuList(categoryId, storeBranchUid)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            LogUtil.d("주문 modelView response : " + response.getValue());
                            this.menuList.setValue(null);
                            this.menuList.setValue(response.getValue());


                        },
                        error -> {
                            LogUtil.d("주문 modelView error : " + error);
                            this.menuList.setValue(null);

                        },
                        () -> {

                            LogUtil.d("주문 modelView complete : ");

                        });
    }

}
