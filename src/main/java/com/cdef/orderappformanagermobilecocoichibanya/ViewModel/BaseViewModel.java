package com.cdef.orderappformanagermobilecocoichibanya.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class BaseViewModel extends ViewModel {

    public MutableLiveData<STATE> state = new MutableLiveData<>();


    public enum STATE {
        NONE, LOADING, ERROR, COMPLETE
    }

}
