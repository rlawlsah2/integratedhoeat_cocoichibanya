package com.cdef.orderappformanagermobilecocoichibanya.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.androidhuman.rxfirebase2.database.RxFirebaseDatabase;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.dataModel.firebaseDataSet.TableMember;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class CartViewModel extends ViewModel {


    public MutableLiveData<HashMap<String, Order>> mCartList = new MutableLiveData<>();
    public MutableLiveData<LoginViewModel.INITSETTING> initSetting = new MutableLiveData<LoginViewModel.INITSETTING>() {
    };

    public MutableLiveData<String> mOrderId = new MutableLiveData<>();
    public MutableLiveData<TableMember> mTableMember = new MutableLiveData<>();

    public enum INITSETTING {
        NONE, LOADING, ERROR, COMPLETE
    }


    public ValueEventListener cartListener = new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            LogUtil.d("카트 가져오자 DataSnapshot: " + dataSnapshot);

            if (dataSnapshot != null) {
                HashMap<String, Order> tmpMap = new HashMap<>();
                Order order = null;
                for (DataSnapshot item : dataSnapshot.getChildren()) {
                    order = item.getValue(Order.class);
                    tmpMap.put(order.CMDTCD, order);
                }

                mCartList.setValue(tmpMap);

            } else {
                mCartList.setValue(null);
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            mCartList.setValue(null);
        }
    };


    public CartViewModel() {
        this.initSetting.setValue(LoginViewModel.INITSETTING.NONE);

    }


    public void initCartListener(String branchId, String tableNo) {

        LogUtil.d("카트 가져오자 branchId: " + branchId);
        LogUtil.d("카트 가져오자 tableNo: " + tableNo);
        FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(null).getmBrandName())
                .child("tables")
                .child(branchId)
                .child(tableNo)
                .child("cart")
                .addValueEventListener(this.cartListener);
    }

    public void removeCartListener(String branchId, String tableNo) {
        FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(null).getmBrandName())
                .child("tables")
                .child(branchId)
                .child(tableNo)
                .child("cart")
                .removeEventListener(this.cartListener);
    }


    public void changeQuantity(String branchId, String tableNo, Order order, int newQuantity) {
        this.initSetting.setValue(LoginViewModel.INITSETTING.LOADING);
        RxFirebaseDatabase.setValue(
                FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(null).getmBrandName())
                        .child("tables")
                        .child(branchId)
                        .child(tableNo)
                        .child("cart")
                        .child(order.key)
                        .child("quantity"), newQuantity)
                .subscribe(() -> {
                    this.initSetting.setValue(LoginViewModel.INITSETTING.COMPLETE);
                }, error -> {
                    this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);

                });
    }


    public void removeCartItem(String branchId, String tableNo, Order order) {
        this.initSetting.setValue(LoginViewModel.INITSETTING.LOADING);
        RxFirebaseDatabase.removeValue(
                FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(null).getmBrandName())
                        .child("tables")
                        .child(branchId)
                        .child(tableNo)
                        .child("cart")
                        .child(order.key))
                .subscribe(() -> {
                    this.initSetting.setValue(LoginViewModel.INITSETTING.COMPLETE);
                }, error -> {
                    this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);

                });
    }

    public void removeCartALLItem(String branchId, String tableNo) {
        this.initSetting.setValue(LoginViewModel.INITSETTING.LOADING);
        RxFirebaseDatabase.removeValue(
                FirebaseDatabase.getInstance().getReference().child(LoginUtil.getInstance(null).getmBrandName())
                        .child("tables")
                        .child(branchId)
                        .child(tableNo)
                        .child("cart"))
                .subscribe(() -> {
                    this.initSetting.setValue(LoginViewModel.INITSETTING.COMPLETE);
                }, error -> {
                    this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);

                });
    }

    public void getOrderId(String branchId, String tableNo) {
        this.initSetting.setValue(LoginViewModel.INITSETTING.LOADING);
        RxFirebaseDatabase.data(
                FirebaseDatabase.getInstance().getReference()
                        .child(LoginUtil.getInstance(null).getmBrandName())
                        .child("tables")
                        .child(branchId)
                        .child(tableNo)
                        .child("IFSA_ORDER_ID"))
                .subscribe((snapshot) -> {

                    try {
                        this.initSetting.setValue(LoginViewModel.INITSETTING.COMPLETE);
                        this.mOrderId.setValue(snapshot.getValue(String.class));
                    } catch (Exception e) {
                        this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);
                        this.mOrderId.setValue(null);

                    }


                }, error -> {
                    this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);
                    this.mOrderId.setValue(null);
                });
    }


    public void getTableMembers(String branchId, String tableNo) {
        this.initSetting.setValue(LoginViewModel.INITSETTING.LOADING);
        RxFirebaseDatabase.data(
                FirebaseDatabase.getInstance().getReference()
                        .child(LoginUtil.getInstance(null).getmBrandName())
                        .child("tables")
                        .child(branchId)
                        .child(tableNo)
                        .child("members"))
                .subscribe((snapshot) -> {

                    try {
                        this.initSetting.setValue(LoginViewModel.INITSETTING.COMPLETE);
                        this.mTableMember.setValue(snapshot.getValue(TableMember.class));
                    } catch (Exception e) {
                        this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);
                        this.mTableMember.setValue(null);

                    }


                }, error -> {
                    this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);
                    this.mTableMember.setValue(null);
                });
    }


}
